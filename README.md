# NormalMap Debugger

## Description
1. NormalMap Debugger is a FREE, OpenSource program for debug NomalMap direction in any pixel of a NomalMap texture.
2. With NormalMap Debugger you can view and change NomalMap directions, generate and normalize NormalMap, restore and clear B-Channel.
3. Also Height Map generation supported: CPU, GPU and CPU + GPU methods, GPU part based on AwesomeBump (License: GNU GPLv3, https://github.com/kmkolasinski/AwesomeBump).

## Download
### [Precompiled version for Windows (x86/x64)](Precompiled_Windows.zip)

## Link
* https://gitlab.com/Alex_Green/NormalMap_Debugger_OpenGL
* https://gitlab.com/Alex_Green/NormalMap_Debugger (old, CPU based only)

## License:
**GNU GPLv3**

## Supported formats:
* .NET Formats: BMP, JPG, PNG, GIF, TIFF.
* TGA (TGASharpLib, https://gitlab.com/Alex_Green/TGASharpLib or https://github.com/ALEXGREENALEX/TGASharpLib, License: MIT)
* DDS (DDSReaderSharp, https://gitlab.com/Alex_Green/DDSReaderSharp or https://github.com/ALEXGREENALEX/DDSReaderSharp, License: MIT)
* PSD (System.Drawing.PSD, https://github.com/bizzehdee/System.Drawing.PSD, License: BSD)

## Author
Zelenskyi Alexandr (Зеленський Олександр) - alex.green.zaa.93@gmail.com