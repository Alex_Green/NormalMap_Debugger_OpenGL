﻿#version 330
layout(location = 0) in vec3 v_Position;
layout(location = 1) in vec3 v_Normal;
layout(location = 2) in vec2 v_UV;
layout(location = 3) in vec3 v_Tan;

uniform mat4 MVP;
uniform mat4 ModelMatrix;
uniform mat3 NormalMatrix;

out mat3 f_TBN;
out vec3 f_Position;
out vec2 f_UV;

void main()
{
	vec3 N = normalize(NormalMatrix * v_Normal);
	vec3 T = normalize(NormalMatrix * v_Tan);
	//vec3 T = normalize(vec3(ModelMatrix * vec4(v_Tan, 0.0)));
	vec3 B = cross(N, T);
	f_TBN = mat3(T, B, N);
	
	f_UV = v_UV;
	f_Position = vec3(ModelMatrix * vec4(v_Position, 1.0));
	gl_Position = MVP * vec4(v_Position, 1.0);
}