﻿#version 330
uniform sampler2D TextureUnit0; // NormalMap

in vec2 f_UV;

uniform bool InvertX = false;
uniform bool InvertY = false;
uniform int EditingMode = 0;
uniform float ClearBValue = 1.0;

layout(location = 0) out vec4 FragColor;

///////////////////////// Start #include("GenerateNormalMap.glsl") ///////////////////////
#define MaxNormalMapsCount 20
uniform int NormalMapsCount = 1;
uniform int Rgb2GrayMode = 0;
uniform bool NMapNormalize = true;

uniform int Kernels[MaxNormalMapsCount];
uniform float Scales[MaxNormalMapsCount];

float NM_GetGrayTexture(sampler2D Texture, vec2 UV, int Rgb2GrayMode)
{
	vec4 RGBA = texture(Texture, UV);
	switch (Rgb2GrayMode)
	{
		default:
		case 0: // Desaturate
			return dot(RGBA.rgb, vec3(0.299, 0.587, 0.114));
		
		case 1: // Average
			return dot(RGBA.rgb, vec3(1.0) / vec3(3.0));
		
		case 2: // Minimum
			return min(RGBA.r, min(RGBA.g, RGBA.b));
		
		case 3: // Maximum
			return max(RGBA.r, max(RGBA.g, RGBA.b));
		
		case 4: // Red
			return RGBA.r;
		
		case 5: // Green
			return RGBA.g;
		
		case 6: // Blue
			return RGBA.b;
		
		case 7: // Alpha
			return RGBA.a;
	}
}

int NM_CreateSobelKernelValue(int Side, int HalfSide, int i, int j) //SobelGradient X, Y, (swap 'i' and 'j')
{
	int k = (i < HalfSide ? HalfSide + i : Side + HalfSide - i - 1);
	if (j < HalfSide)
		return j - k;
	else if (j > HalfSide)
		return k + j - Side + 1;
	return 0;
}

vec3 NM_NormalMapGen(sampler2D Texture, vec2 UV, int Kernel, float Scale, bool InvertX, bool InvertY)
{
	vec2 TexSize = vec2(1.0) / textureSize(TextureUnit0, 0);
	vec3 NMap = vec3(0.0);
	int HalfK = Kernel / 2;
	
	for(int i = -HalfK; i <= HalfK; i++)
	{
		int IplusHalfK = i + HalfK;
		for(int j = -HalfK; j <= HalfK; j++)
		{
			vec2 K_UV = UV + TexSize * vec2(i, j);
			int JplusHalfK = j + HalfK;
			float GrayTex = NM_GetGrayTexture(Texture, K_UV, Rgb2GrayMode);
			NMap.x += NM_CreateSobelKernelValue(Kernel, HalfK, JplusHalfK, IplusHalfK) * GrayTex;
			NMap.y += NM_CreateSobelKernelValue(Kernel, HalfK, IplusHalfK, JplusHalfK) * GrayTex;
		}
	}
	
	NMap.xy *= vec2((InvertX ? Scale : -Scale), (InvertY ? Scale : -Scale)) / sqrt(2.0);
	NMap.z = sqrt(clamp(1.0 - NMap.x * NMap.x - NMap.y * NMap.y, 0.0, 1.0));
	return NMap * 0.5 + 0.5;
}

vec3 NM_Normalize(vec3 N)
{
	return normalize(N * 2.0 - 1.0) * 0.5 + 0.5;
}

vec3 NM_LinearLightBlend(vec3 Img1, vec3 Img2)
{
	return clamp(Img1 * 2.0 + Img2 - 1.0, 0.0, 1.0);
}

vec3 GenerateNormalMap(sampler2D Texture, vec2 UV, bool InvertX, bool InvertY)
{
	vec3 NMap_LowFreq = NM_NormalMapGen(Texture, UV, Kernels[NormalMapsCount - 1], Scales[NormalMapsCount - 1], InvertX, InvertY);

	if (NMapNormalize)
		NMap_LowFreq = NM_Normalize(NMap_LowFreq);
	
	for (int i = NormalMapsCount - 2; i >= 0; i--)
	{
		vec3 NMap_HiFreq = NM_NormalMapGen(Texture, UV, Kernels[i], Scales[i], InvertX, InvertY);

		if (NMapNormalize)
			NMap_HiFreq = NM_Normalize(NMap_HiFreq);
		
		NMap_LowFreq = NM_Normalize(NM_LinearLightBlend(NMap_HiFreq, NMap_LowFreq));
	}
	
	return NMap_LowFreq;
}
///////////////////////// End #include("GenerateNormalMap.glsl") /////////////////////////

///////////////////////// Start #include("GenerateHeightMap.glsl") ///////////////////////
uniform int HMap_Step = 1;
uniform float HMap_MinValue = 0.0;
uniform float HMap_MaxValue = 1.0;

float GenerateHeightMap(sampler2D Texture, vec2 UV, int Step)
{
	vec2 TexSize = vec2(1.0) / textureSize(TextureUnit0, 0);
	float VectorCoef = Step * 0.125; // Step / 8f;
	vec2 UV_StepX = vec2(Step * TexSize.x, 0.0);
	vec2 UV_StepY = vec2(0.0, Step * TexSize.y);
	
	float Height_L = texture(Texture, UV + UV_StepX).b;
	float Height_R = texture(Texture, UV - UV_StepX).b;
	float Height_D = texture(Texture, UV + UV_StepY).b;
	float Height_U = texture(Texture, UV - UV_StepY).b;
	
	float HeightAvg = (Height_L + Height_R + Height_D + Height_U) * 0.25;
	
	float Normal_L = texture(Texture, UV + UV_StepX).r;
	float Normal_R = texture(Texture, UV - UV_StepX).r;
	float Normal_D = texture(Texture, UV + UV_StepY).g;
	float Normal_U = texture(Texture, UV - UV_StepY).g;
	
	float DeltaX = Normal_L - Normal_R;
	float DeltaY = Normal_D - Normal_U;
	
	return HeightAvg + (DeltaX + DeltaY) * VectorCoef;
}
///////////////////////// End #include("GenerateHeightMap.glsl") /////////////////////////

void main()
{
	switch(EditingMode)
	{
		default:
		case 0: // NOP
			FragColor = texture(TextureUnit0, f_UV);
			break;
		case 1: // Invert R
			FragColor.gba = texture(TextureUnit0, f_UV).gba;
			FragColor.r = 1.0 - texture(TextureUnit0, f_UV).r;
			break;
		
		case 2: // Invert G
			FragColor.rba = texture(TextureUnit0, f_UV).rba;
			FragColor.g = 1.0 - texture(TextureUnit0, f_UV).g;
			break;
		
		case 3: // Invert B
			FragColor.rga = texture(TextureUnit0, f_UV).rga;
			FragColor.b = 1.0 - texture(TextureUnit0, f_UV).b;
			break;
		
		case 4: // Invert A
			FragColor.rgb = texture(TextureUnit0, f_UV).rgb;
			FragColor.a = 1.0 - texture(TextureUnit0, f_UV).a;
			break;
		
		case 5: // Invert RGB
			FragColor.rgb = vec3(1.0) - texture(TextureUnit0, f_UV).rgb;
			FragColor.a = texture(TextureUnit0, f_UV).a;
			break;
			
		case 6: // Normalize NormalMap
			FragColor.rgb = normalize(texture(TextureUnit0, f_UV).rgb * 2.0 - 1.0) * 0.5 + 0.5;
			FragColor.a = texture(TextureUnit0, f_UV).a;
			break;
		
		case 7: // Restore B
			vec3 RestB_N = texture(TextureUnit0, f_UV).rgb * 2.0 - 1.0;
			RestB_N.z = sqrt(clamp(1.0 - RestB_N.x * RestB_N.x - RestB_N.y * RestB_N.y, 0.0, 1.0));
			FragColor = vec4(normalize(RestB_N)* 0.5 + 0.5, texture(TextureUnit0, f_UV).a);
			break;
		
		case 8: // Clear B
			FragColor.rga = texture(TextureUnit0, f_UV).rga;
			FragColor.b = ClearBValue;
			break;
		
		case 9: // Flip R <-> G
			FragColor = texture(TextureUnit0, f_UV).grba;
			break;
		
		case 10: // Flip G <-> B
			FragColor = texture(TextureUnit0, f_UV).rbga;
			break;
		
		case 11: // Flip B <-> A
			FragColor = texture(TextureUnit0, f_UV).rgab;
			break;
		
		case 12: // Rotate -90°
			vec2 Rot270_UV = vec2(f_UV.y, 1.0 - f_UV.x);
			FragColor.r = (InvertY ? texture(TextureUnit0, Rot270_UV).g : 1.0 - texture(TextureUnit0, Rot270_UV).g);
			FragColor.g = (InvertY ? 1.0 - texture(TextureUnit0, Rot270_UV).r : texture(TextureUnit0, Rot270_UV).r);
			FragColor.ba = texture(TextureUnit0, Rot270_UV).ba;
			break;
			
		case 13: // Rotate +90°
			vec2 Rot90_UV = vec2(1.0 - f_UV.y, f_UV.x);
			FragColor.r = (InvertY ? 1.0 - texture(TextureUnit0, Rot90_UV).g : texture(TextureUnit0, Rot90_UV).g);
			FragColor.g = (InvertY ? texture(TextureUnit0, Rot90_UV).r : 1.0 - texture(TextureUnit0, Rot90_UV).r);
			FragColor.ba = texture(TextureUnit0, Rot90_UV).ba;
			break;
		
		case 14: // Rotate 180°
			vec2 Rot180_UV = vec2(1.0) - f_UV;
			FragColor.ba = texture(TextureUnit0, Rot180_UV).ba;
			FragColor.rg = vec2(1.0) - texture(TextureUnit0, Rot180_UV).rg;
			break;
		
		case 15: // Flip Horizontally
			vec2 FlipHor_UV = vec2(1.0 - f_UV.x, f_UV.y);
			FragColor.gba = texture(TextureUnit0, FlipHor_UV).gba;
			FragColor.r = 1.0 - texture(TextureUnit0, FlipHor_UV).r;
			break;
		
		case 16: // Flip Vertically
			vec2 FlipVer_UV = vec2(f_UV.x, 1.0 - f_UV.y);
			FragColor.rba = texture(TextureUnit0, FlipVer_UV).rba;
			FragColor.g = 1.0 - texture(TextureUnit0, FlipVer_UV).g;
			break;
		
		case 17: // Generate NormalMap
			FragColor.rgb = GenerateNormalMap(TextureUnit0, f_UV, InvertX, InvertY);
			FragColor.a = texture(TextureUnit0, f_UV).a;
			break;
		
		case 18: // Generate HeightMap
			FragColor.b = GenerateHeightMap(TextureUnit0, f_UV, HMap_Step);
			FragColor.rga = texture(TextureUnit0, f_UV).rga;
			break;
		
		case 19: // Normalize HeightMap
			float HeightValue = (texture(TextureUnit0, f_UV).b - HMap_MinValue) / (HMap_MaxValue - HMap_MinValue);
			FragColor.rgb = vec3(clamp(HeightValue, 0.0, 1.0));
			FragColor.a = texture(TextureUnit0, f_UV).a;
			break;
	}
}