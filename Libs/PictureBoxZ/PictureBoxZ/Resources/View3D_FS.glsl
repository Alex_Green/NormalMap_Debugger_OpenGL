﻿#version 330
uniform sampler2D TextureUnit0; // Diffuse/NormalMap

uniform mat4 MVP;
uniform mat4 ModelView;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat3 NormalMatrix;
uniform vec3 CameraPos;

in mat3 f_TBN;
in vec3 f_Position;
in vec2 f_UV;

uniform vec4 Material_Kd;		// Diffuse reflectivity, RGBA
uniform vec3 Material_Ks;		// Specular reflectivity, RGB
uniform float Material_S;		// Specular shininess factor

uniform vec3 Light_Dir;		// Light Direction
uniform vec3 Light_Ld;		// Diffuse intensity
uniform vec3 Light_Ls;		// Specular intensity

uniform bool UseLight = true;
uniform bool ShowTexture = true;
uniform bool TextureAsNMap = true;
uniform bool InvertX = false;
uniform bool InvertY = false;

layout(location = 0) out vec4 FragColor;

void main()
{
	vec3 Normal = normalize(f_TBN[2]);
	if (TextureAsNMap)
	{
		Normal = texture(TextureUnit0, f_UV).rgb * 2.0 - 1.0;
		Normal.x = InvertX ? -Normal.x : Normal.x;
		Normal.y = InvertY ? -Normal.y : Normal.y;
		Normal = normalize(f_TBN * Normal);
	}
	
	vec4 Kd = Material_Kd;
	if (ShowTexture)
		Kd = texture(TextureUnit0, f_UV);
	
	if (UseLight)
	{
		vec3 LDiffuse = vec3(0.0);
		vec3 LSpecular = vec3(0.0);
		vec3 S = normalize(Light_Dir); // Light direction
		float sDotN = max(dot(S, Normal), 0.0);
		
		if (sDotN > 0.0)
		{
			LDiffuse += sDotN * Light_Ld; // Light Diffuse
			
			vec3 V = normalize(CameraPos - f_Position); // View Vector
			vec3 H = normalize(V + S); // HalfWay vector
			LSpecular += pow(max(dot(H, Normal), 0.0), Material_S) * Light_Ls; // Light Specular
		}
		
		FragColor.rgb = LDiffuse * Kd.rgb + LSpecular * Material_Ks;
	}
	else
		FragColor.rgb = Kd.rgb;
	
	FragColor.a = sqrt(dot(FragColor.rgb, vec3(0.299, 0.587, 0.114))); // Luma for FXAA
}