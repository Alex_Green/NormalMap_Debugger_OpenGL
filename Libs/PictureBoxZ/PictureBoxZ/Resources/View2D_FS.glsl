﻿#version 330
uniform sampler2D TextureUnit0;

in vec2 f_UV;

uniform bool UseAlpha = true;
uniform int DrawMode = 0;
uniform float BadPixThreshold = 0.02;
uniform vec3 BadPixColorBad = vec3(1.0, 0.0, 0.0);
uniform vec3 BadPixColorGood = vec3(0.0, 0.0, 0.0);

layout(location = 0) out vec4 FragColor;

void main()
{
	FragColor.a = (UseAlpha ? texture(TextureUnit0, f_UV).a : 1.0);
	
	switch(DrawMode)
	{
		default:
		case 0: // RGB
			FragColor.rgb = texture(TextureUnit0, f_UV).rgb;
			break;
		case 1: // Red
			FragColor.rgb = texture(TextureUnit0, f_UV).rrr;
			break;
		case 2: // Green
			FragColor.rgb = texture(TextureUnit0, f_UV).ggg;
			break;
		case 3: // Blue
			FragColor.rgb = texture(TextureUnit0, f_UV).bbb;
			break;
		case 4: // Alpha
			FragColor = vec4(texture(TextureUnit0, f_UV).aaa, 1.0);
			break;
		case 5: // BadPixels
			vec3 N = texture(TextureUnit0, f_UV).rgb * 2.0 - 1.0;
			float V = step(BadPixThreshold, abs(sqrt(N.x * N.x + N.y * N.y + N.z * N.z) - 1.0));
			FragColor.rgb = mix(BadPixColorGood, BadPixColorBad, V);
			break;
	}
}