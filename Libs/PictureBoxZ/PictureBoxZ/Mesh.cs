﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace PictureBoxZ
{
    public static class Meshes
    {
        public static List<Mesh> MeshesList = new List<Mesh>(); // All Meshes

        public static void LoadMeshes()
        {
            try
            {
                string[] MeshFiles = new string[]
                {
                    Properties.Resources.Box_obj,
                    Properties.Resources.ChamferBox_obj,
                    Properties.Resources.Cylinder_obj,
                    Properties.Resources.Sphere_obj
                };

                for (int i = 0; i < MeshFiles.Length; i++)
                {
                    Mesh M = new Mesh(MeshFiles[i]);
                    M.Load();
                    MeshesList.Add(M);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Models.LoadMeshes() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

    class Face
    {
        public int[] V = new int[3];
        public int[] VT = new int[3];
        public int[] VN = new int[3];
        public int MaterialID = -1;
        public UInt32 SmoothingGroup = 0;
    }

    public class Mesh
    {
        public string file = String.Empty;
        public List<MeshPart> Parts = new List<MeshPart>();

        public Vector3 Position = Vector3.Zero;
        public Vector3 Rotation = Vector3.Zero;
        public Vector3 Scale = Vector3.One;

        public Matrix4 ModelMatrix = Matrix4.Identity;
        public Matrix4 ModelViewMatrix = Matrix4.Identity;
        public Matrix4 ModelViewProjectionMatrix = Matrix4.Identity;

        public Mesh()
        {
        }

        public Mesh(string file)
        {
            this.file = file;
        }

        public void Load()
        {
            try
            {
                if (Parts.Count > 0)
                    Free();
                Parts.AddRange(MeshPart.LoadFromFile(file));
            }
            catch (Exception e)
            {
                if (File.Exists(file))
                    MessageBox.Show(String.Format("Error in Mesh | File:\"{0}\"", file) + e.Message, "Mesh.Load() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show("Error in Mesh!" + e.Message, "Mesh.Load() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Free()
        {
            for (int i = 0; i < Parts.Count; i++)
                Parts[i].Free();
            Parts.Clear();
        }

        public void CalculateMatrices(Camera Camera)
        {
            ModelMatrix = Matrix4.CreateScale(Scale) * Matrix4.CreateRotationX(Rotation.X) * Matrix4.CreateRotationY(Rotation.Y) * Matrix4.CreateRotationZ(Rotation.Z) * Matrix4.CreateTranslation(Position);
            ModelViewMatrix = ModelMatrix * Camera.GetViewMatrix();
            ModelViewProjectionMatrix = ModelViewMatrix * Camera.GetProjectionMatrix();
        }
    }

    public class MeshPart
    {
        public int VertexBufferID, NormalBufferID, UVBufferID, TangentBufferID;

        public Vector3[] Vertexes, Normals, Tangents;
        public Vector2[] UVs;

        public MeshPart()
        {
        }

        public void GenBuffers()
        {
            VertexBufferID = GL.GenBuffer();
            NormalBufferID = GL.GenBuffer();
            UVBufferID = GL.GenBuffer();
            TangentBufferID = GL.GenBuffer();
        }

        public void BindBuffers()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferID);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertexes.Length * Vector3.SizeInBytes), Vertexes, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, NormalBufferID);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Normals.Length * Vector3.SizeInBytes), Normals, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, UVBufferID);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(UVs.Length * Vector2.SizeInBytes), UVs, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, TangentBufferID);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Tangents.Length * Vector3.SizeInBytes), Tangents, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public void FreeBuffers()
        {
            GL.DeleteBuffer(VertexBufferID);
            GL.DeleteBuffer(NormalBufferID);
            GL.DeleteBuffer(UVBufferID);
            GL.DeleteBuffer(TangentBufferID);

            VertexBufferID = 0;
            NormalBufferID = 0;
            UVBufferID = 0;
            TangentBufferID = 0;
        }

        public void Free()
        {
            FreeBuffers();

            Vertexes = null;
            Normals = null;
            UVs = null;
            Tangents = null;
        }

        static void ComputeTangentBasis(Vector3[] Vertices, Vector3[] Normals, Vector2[] UVs, out Vector3[] Tangents)
        {
            Tangents = new Vector3[Vertices.Length];
            Vector3[] Tangents2 = new Vector3[Tangents.Length];

            try
            {
                // Compute the tangent vector
                for (int i = 0; i < Vertices.Length; i += 3)
                {
                    Vector3 p1 = Vertices[i];
                    Vector3 p2 = Vertices[i + 1];
                    Vector3 p3 = Vertices[i + 2];

                    Vector2 t1 = UVs[i];
                    Vector2 t2 = UVs[i + 1];
                    Vector2 t3 = UVs[i + 2];

                    Vector3 q1 = p2 - p1;
                    Vector3 q2 = p3 - p1;
                    Vector2 s1 = t2 - t1;
                    Vector2 s2 = t3 - t1;

                    float r = 1.0f / (s1.X * s2.Y - s2.X * s1.Y);

                    Tangents[i] = (s2.Y * q1 - s1.Y * q2) * r;
                    Tangents2[i] = (s1.X * q2 - s2.X * q1) * r;

                    Tangents[i + 1] = Tangents[i];
                    Tangents[i + 2] = Tangents[i];
                    Tangents2[i + 1] = Tangents2[i];
                    Tangents2[i + 2] = Tangents2[i];
                }

                for (int i = 0; i < Vertices.Length; i++)
                {
                    Vector3 n = Normals[i];
                    Vector3 t1 = Tangents[i];
                    Vector3 t2 = Tangents2[i];

                    // Gram-Schmidt orthogonalize.
                    Tangents[i] = Vector3.Normalize(t1 - (Vector3.Dot(n, t1) * n));

                    //if (Vector3.Dot(Vector3.Cross(n, t1), t2) < 0.0f)
                    //    Tangents[i] *= -1.0f; // Tangents[i].w *= -1.0f;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "MeshPart.ComputeTangentBasis() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static MeshPart[] LoadFromFile(string file)
        {
            MeshPart[] obj = null;
            try
            {
                if (File.Exists(file))
                    obj = LoadFromString(File.ReadAllText(file));
                else
                    obj = LoadFromString(file);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "MeshPart.LoadFromFile() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return obj;
        }

        public static MeshPart[] LoadFromString(string obj)
        {
            try
            {
                List<string> Lines = new List<string>(obj.Split('\n'));
                Dictionary<string, int> MP_Materials = new Dictionary<string, int>();
                UInt32 CurrentSmoothingGroup = 0;
                int CurrentMaterial = 0;

                #region Remove all comments, empty string; convert ToLower()
                for (int i = 0; i < Lines.Count; i++)
                {
                    string L = Lines[i].Trim(new char[] { ' ', '\r' });
                    if (L.Length == 0 || (L.Length > 0 && L.Substring(0, 1) == "#"))
                    {
                        Lines.RemoveAt(i);
                        i--;
                    }
                    else
                        Lines[i] = L.ToLower();
                }
                #endregion

                #region Triangulate
                List<String> LinesTriangulated = new List<string>();
                for (int i = 0; i < Lines.Count; i++)
                {
                    if (Lines[i].Substring(0, 1) == "f")
                    {
                        string[] FaceParts = Lines[i].Split(new char[] { ' ' });
                        if (FaceParts.Length > 4)
                            for (int j = 1; j < FaceParts.Length - 2; j++)
                                LinesTriangulated.Add(FaceParts[0] + " " + FaceParts[1] + " " + FaceParts[j + 1] + " " + FaceParts[j + 2]);
                        else
                            LinesTriangulated.Add(Lines[i]);
                    }
                    else
                        LinesTriangulated.Add(Lines[i]);
                }
                Lines = LinesTriangulated;
                LinesTriangulated = null;
                #endregion

                // Списки для хранения данных модели
                List<Vector3> Vertexes = new List<Vector3>();
                List<Vector3> Normals = new List<Vector3>();
                List<Vector2> UVs = new List<Vector2>();
                List<Face> Faces = new List<Face>();

                float MaxX = float.MinValue;
                float MaxY = float.MinValue;
                float MaxZ = float.MinValue;
                float MinX = float.MaxValue;
                float MinY = float.MaxValue;
                float MinZ = float.MaxValue;

                string[] lineparts;
                for (int i = 0; i < Lines.Count; i++)
                {
                    #region Parse Lines
                    lineparts = Lines[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lineparts.Length > 1)
                    {
                        switch (lineparts[0])
                        {
                            case "v":
                                #region Vertex
                                try
                                {
                                    if (lineparts.Length >= 4)
                                    {
                                        float x = float.Parse(lineparts[1]);
                                        float y = float.Parse(lineparts[2]);
                                        float z = float.Parse(lineparts[3]);
                                        MaxX = Math.Max(x, MaxX);
                                        MaxY = Math.Max(y, MaxY);
                                        MaxZ = Math.Max(z, MaxZ);
                                        MinX = Math.Min(x, MinX);
                                        MinY = Math.Min(y, MinY);
                                        MinZ = Math.Min(z, MinZ);
                                        Vertexes.Add(new Vector3(x, y, z));
                                    }
                                }
                                catch
                                {
                                    MessageBox.Show("Error parsing Vertex in line " + (i + 1).ToString() + ": " + Lines[i]);
                                }
                                #endregion
                                break;

                            case "vt":
                                #region TexCoord
                                try
                                {
                                    if (lineparts.Length >= 3)
                                    {
                                        float u = float.Parse(lineparts[1]);
                                        float v = float.Parse(lineparts[2]);
                                        UVs.Add(new Vector2(u, v));
                                    }
                                }
                                catch
                                {
                                    MessageBox.Show("Error parsing TextureCoords in line " + (i + 1).ToString() + ": " + Lines[i]);
                                }
                                #endregion
                                break;

                            case "vn":
                                #region Normal
                                try
                                {
                                    if (lineparts.Length >= 4)
                                    {
                                        float nx = float.Parse(lineparts[1]);
                                        float ny = float.Parse(lineparts[2]);
                                        float nz = float.Parse(lineparts[3]);
                                        Normals.Add(new Vector3(nx, ny, nz));
                                    }
                                }
                                catch
                                {
                                    MessageBox.Show("Error parsing Normals in line " + (i + 1).ToString() + ": " + Lines[i]);
                                }
                                #endregion
                                break;

                            case "f":
                                #region Face
                                try
                                {
                                    Face Face = new Face();

                                    for (int j = 0; j < lineparts.Length - 1; j++)
                                    {
                                        String[] FaceParams = lineparts[j + 1].Split('/'); // v, v/vt, v//vn, v/vt/vn
                                        int FaceV_Index, FaceVT_Index, FaceVN_Index;

                                        switch (FaceParams.Length)
                                        {
                                            case 1: // "v"
                                                FaceV_Index = int.Parse(FaceParams[0]);
                                                if (FaceV_Index > 0)
                                                    Face.V[j] = FaceV_Index - 1;
                                                else if (FaceV_Index < 0)
                                                    Face.V[j] = Vertexes.Count + FaceV_Index;
                                                break;

                                            case 2: // "v/vt"
                                                FaceV_Index = int.Parse(FaceParams[0]);
                                                if (FaceV_Index > 0)
                                                    Face.V[j] = FaceV_Index - 1;
                                                else if (FaceV_Index < 0)
                                                    Face.V[j] = Vertexes.Count + FaceV_Index;

                                                FaceVT_Index = int.Parse(FaceParams[1]);
                                                if (FaceVT_Index > 0)
                                                    Face.VT[j] = FaceVT_Index - 1;
                                                else if (FaceVT_Index < 0)
                                                    Face.VT[j] = UVs.Count + FaceVT_Index;
                                                break;

                                            case 3: // "v//vn"
                                                if (FaceParams[1].Trim() == String.Empty)
                                                {
                                                    FaceV_Index = int.Parse(FaceParams[0]);
                                                    if (FaceV_Index > 0)
                                                        Face.V[j] = FaceV_Index - 1;
                                                    else if (FaceV_Index < 0)
                                                        Face.V[j] = Vertexes.Count + FaceV_Index;

                                                    FaceVN_Index = int.Parse(FaceParams[2]);
                                                    if (FaceVN_Index > 0)
                                                        Face.VN[j] = FaceVN_Index - 1;
                                                    else if (FaceVN_Index < 0)
                                                        Face.VN[j] = Normals.Count + FaceVN_Index;
                                                }
                                                else // "v/vt/vn"
                                                {
                                                    FaceV_Index = int.Parse(FaceParams[0]);
                                                    if (FaceV_Index > 0)
                                                        Face.V[j] = FaceV_Index - 1;
                                                    else if (FaceV_Index < 0)
                                                        Face.V[j] = Vertexes.Count + FaceV_Index;

                                                    FaceVT_Index = int.Parse(FaceParams[1]);
                                                    if (FaceVT_Index > 0)
                                                        Face.VT[j] = FaceVT_Index - 1;
                                                    else if (FaceVT_Index < 0)
                                                        Face.VT[j] = UVs.Count + FaceVT_Index;

                                                    FaceVN_Index = int.Parse(FaceParams[2]);
                                                    if (FaceVN_Index > 0)
                                                        Face.VN[j] = FaceVN_Index - 1;
                                                    else if (FaceVN_Index < 0)
                                                        Face.VN[j] = Normals.Count + FaceVN_Index;
                                                }
                                                break;
                                        }
                                    }
                                    Face.SmoothingGroup = CurrentSmoothingGroup;
                                    Face.MaterialID = CurrentMaterial;
                                    Faces.Add(Face);
                                }
                                catch
                                {
                                    MessageBox.Show("Error parsing Face in line " + (i + 1).ToString() + ": " + Lines[i]);
                                }
                                #endregion
                                break;

                            case "s":
                                #region SmoothingGroup
                                if (lineparts[1] == "off")
                                    CurrentSmoothingGroup = 0;
                                else
                                    CurrentSmoothingGroup = Convert.ToUInt32(lineparts[1]);
                                #endregion
                                break;

                            case "usemtl":
                                #region Material
                                if (MP_Materials.ContainsKey(lineparts[1]))
                                    CurrentMaterial = MP_Materials[lineparts[1]];
                                else
                                {
                                    MP_Materials.Add(lineparts[1], MP_Materials.Count);
                                    CurrentMaterial = MP_Materials.Count - 1;
                                }
                                #endregion
                                break;
                        }
                    }
                    #endregion
                }

                // Нормализуем размер меша
                float MaxXYZ = Math.Max(MaxX, Math.Max(MaxY, Math.Max(MaxZ, Math.Max(-MinX, Math.Max(-MinY, -MinZ)))));
                for (int i = 0; i < Vertexes.Count; i++)
                {
                    Vertexes[i] -= new Vector3(MinX + MaxX, MinY + MaxY, MinZ + MaxZ) * 0.5f;
                    Vertexes[i] /= MaxXYZ;
                }

                // Если нет текстурных координат, то создаем одну.
                if (UVs.Count == 0)
                    UVs.Add(new Vector2());

                #region If (NormalsCount = 0) -> Calc Normals
                if (Normals.Count == 0)
                {
                    for (int i = 0; i < Faces.Count; i++)
                    {
                        Vector3 U = Vertexes[Faces[i].V[1]] - Vertexes[Faces[i].V[0]];
                        Vector3 V = Vertexes[Faces[i].V[2]] - Vertexes[Faces[i].V[0]];
                        Normals.Add(Vector3.Cross(U, V));
                        Faces[i].VN[0] = i;
                        Faces[i].VN[1] = i;
                        Faces[i].VN[2] = i;
                    }
                }
                #endregion

                // Sort Faces by MaterialID
                Faces.Sort(delegate (Face A, Face B)
                {
                    return A.MaterialID.CompareTo(B.MaterialID);
                });

                MeshPart[] MeshParts = new MeshPart[Faces[Faces.Count - 1].MaterialID + 1];
                for (int m = 0; m < MeshParts.Length; m++)
                {
                    MeshPart v = new MeshPart();
                    List<Face> vFaces = new List<Face>();

                    //Select faces with same MaterialID
                    for (int i = 0; i < Faces.Count; i++)
                        if (Faces[i].MaterialID == m)
                            vFaces.Add(Faces[i]);

                    v.Vertexes = new Vector3[vFaces.Count * 3];
                    v.UVs = new Vector2[vFaces.Count * 3];
                    v.Normals = new Vector3[vFaces.Count * 3];

                    for (int j = 0; j < 3; j++)
                    {
                        for (int i = 0; i < vFaces.Count; i++)
                        {
                            int ij3 = i * 3 + j;
                            v.Vertexes[ij3] = Vertexes[vFaces[i].V[j]];
                            v.UVs[ij3] = UVs[vFaces[i].VT[j]];
                            v.Normals[ij3] = Normals[vFaces[i].VN[j]];
                        }
                    }

                    ComputeTangentBasis(v.Vertexes, v.Normals, v.UVs, out v.Tangents);

                    v.GenBuffers();
                    v.BindBuffers();

                    MeshParts[m] = v;
                }

                //Cleanup
                Vertexes = null;
                UVs = null;
                Normals = null;
                Faces = null;
                MP_Materials = null;

                return MeshParts;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "MeshPart.LoadFromString() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }
}
