﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace PictureBoxZ
{
    public static class Engine // Helper functions class
    {
        public static int TextureID = 0;
        public static int TextureMapGen_OrigID = 0;
        public static TextureMagFilter TextureMagFilter_Default = TextureMagFilter.Nearest;
        public static TextureMinFilter TextureMinFilter_Default = TextureMinFilter.Nearest;
        public static TextureMagFilter TextureMagFilter_BadPixels = TextureMagFilter.Nearest;
        public static TextureMinFilter TextureMinFilter_BadPixels = TextureMinFilter.Nearest;
        public static ColorRGBA GLClearColor = new ColorRGBA(Color4.Gray);

        public static bool ShowTexture = false;
        public static bool UseTextureAsNormalMap = true;
        public static bool UseLight = true;
        public static bool UseFXAA = true;
        public static bool InvertX = false;
        public static bool InvertY = false;
        public static bool AnisotropicFiltering = true;
        public static float AnisotropicFilteringMaxValue = -1f;

        public static float ClearBlueChannelValue = 1f;
        public static RgbToGrayModes RgbToGrayMode = RgbToGrayModes.Desaturate;
        public static bool NormalMapNormalize = true;
        public static int[] NormalMapKernels = new int[] { 3 };
        public static float[] NormalMapScales = new float[] { 5f };

        public static int HeightMap_GenStepValue = 1;
        public static float HeightMap_MinValue = float.MaxValue;
        public static float HeightMap_MaxValue = float.MinValue;

        public static DrawModes2D DrawMode2D = DrawModes2D.RGB;
        public static bool Draw2D_UseAlpha = true;
        public static float BadPixel_Threshold = 0.02f;
        public static Vector3 BadPixel_ColorBad = Vector3.UnitX;
        public static Vector3 BadPixel_ColorGood = Vector3.Zero;

        public static Camera MainCamera = null;
        public static Material MainMaterial = null;
        public static Mesh MainMesh = null;

        #region TextureImageUnits
        /// <summary>
        /// MaxTextureImageUnits - This is the number of fragment shader texture image units.
        /// </summary>
        public static int MaxTextureImageUnits = 0; //FS
        public static int MaxVertexTextureImageUnits = 0; //VS
        public static int MaxGeometryTextureImageUnits = 0; //GS

        /// <summary>
        /// TextureImageUnits = Min(MaxTextureImageUnits, MaxVertexTextureImageUnits, MaxGeometryTextureImageUnits)
        /// </summary>
        public static int TextureImageUnits = 0;
        #endregion

        public static void Load()
        {
            try
            {
                MainCamera = new Camera();
                MainCamera.Position = new Vector3(0f, 0f, 2.8f);
                MainCamera.YawPitch = new Vector2(MathHelper.DegreesToRadians(180f), MathHelper.DegreesToRadians(0f));
                MainCamera.zNear = 0.5f;
                MainCamera.zFar = 10f;
                MainCamera.FOV = 75f;

                MainMaterial = new Material();
                MainMaterial.Shader = Shaders.GetShader("View3D");
                MainMesh = Meshes.MeshesList[0];
            }
            catch
            {
                MessageBox.Show("Map loading Error!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void Free()
        {
            MainCamera = null;
            MainMaterial = null;
            MainMesh = null;

            for (int i = 0; i < Meshes.MeshesList.Count; i++)
                Meshes.MeshesList[i].Free();
            Meshes.MeshesList.Clear();

            GL.BindTexture(TextureTarget.Texture2D, 0);
            if (TextureID != 0)
            {
                GL.DeleteTexture(TextureID);
                TextureID = 0;
            }

            GL.UseProgram(0);
            for (int i = 0; i < Shaders.ShadersList.Count; i++)
                Shaders.ShadersList[i].Free();
            Shaders.ShadersList.Clear();
        }

        public static void LoadConfigAndContent()
        {
            // GetTextureUnitsCount
            MaxTextureImageUnits = GL.GetInteger(GetPName.MaxTextureImageUnits); //FS TextureImageUnits Only
            MaxVertexTextureImageUnits = GL.GetInteger(GetPName.MaxVertexTextureImageUnits); //VS
            MaxGeometryTextureImageUnits = GL.GetInteger(GetPName.MaxGeometryTextureImageUnits); //GS
            TextureImageUnits = Math.Min(Math.Min(MaxTextureImageUnits, MaxVertexTextureImageUnits), MaxGeometryTextureImageUnits);

            if (IsExtensionSupported("GL_EXT_texture_filter_anisotropic"))
                AnisotropicFilteringMaxValue = GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt);

            try
            {
                Shaders.LoadShaders();
                Meshes.LoadMeshes();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Engine.LoadConfigAndContent() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static string[] GetSupportedExtensions()
        {
            string[] SupportedExtensions = new string[GL.GetInteger(GetPName.NumExtensions)];
            for (int i = 0; i < SupportedExtensions.Length; i++)
                SupportedExtensions[i] = GL.GetString(StringNameIndexed.Extensions, i);
            return SupportedExtensions;
        }

        public static bool IsExtensionSupported(string GL_EXT)
        {
            GL_EXT = GL_EXT.Trim().ToLowerInvariant();
            string[] SupportedExtensions = GetSupportedExtensions();

            for (int i = 0; i < SupportedExtensions.Length; i++)
                if (SupportedExtensions[i].Trim().ToLowerInvariant() == GL_EXT)
                    return true;

            return false;
        }

        /// <summary>
        /// Get Direction vector from Yaw and Pitch (in radians).
        /// </summary>
        /// <param name="Yaw">Yaw in radians.</param>
        /// <param name="Pitch">Pitch in radians.</param>
        /// <returns>Direction vector</returns>
        public static Vector3 FromYawPitch(float Yaw, float Pitch)
        {
            float CosPitch = (float)Math.Cos(Pitch); //Optimization
            Vector3 Result = new Vector3(CosPitch * (float)Math.Sin(Yaw), (float)Math.Sin(Pitch), CosPitch * (float)Math.Cos(Yaw));
            //Result.Normalize();
            return Result;
        }

        /// <summary>
        /// Get Yaw from Direction vector.
        /// </summary>
        /// <param name="DirectionVector">Direction vector.</param>
        /// <returns>Yaw, Pitch in radians/</returns>
        public static Vector2 ExtractYawPitch(Vector3 Direction)
        {
            var V = Direction.Normalized();
            float Pitch = (float)Math.Asin(V.Y);
            float Yaw = (float)Math.Atan2(V.X, V.Z);
            return new Vector2(Yaw, Pitch);
        }

        /// <summary>
        /// Fix path if it have different Directory Separator Chars ('/' and '\').
        /// </summary>
        /// <param name="path">Original path</param>
        /// <returns>Fixed path</returns>
        public static string FixPath(string path)
        {
            if (path == null)
                return null;

            return path.Replace('/', Path.DirectorySeparatorChar).
                Replace('\\', Path.DirectorySeparatorChar).Trim();
        }

        public static string CombinePaths(string path1, string path2)
        {
            if (path1 == null || path2 == null)
                return null;

            path1 = FixPath(path1).Trim().TrimEnd(new char[] { '\\', '/' });
            path2 = FixPath(path2).Trim().TrimStart(new char[] { '\\', '/' });
            return path1 + Path.DirectorySeparatorChar + path2;
        }
    }
}