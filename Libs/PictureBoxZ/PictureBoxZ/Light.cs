﻿using OpenTK;

namespace PictureBoxZ
{
    public static class Light
    {
        public static  Vector3 Diffuse = new Vector3(0.8f);
        public static Vector3 Specular = new Vector3(1f);
        static Vector3 direction = new Vector3(0f, 0f, MathHelper.PiOver2);

        public static Vector3 Direction
        {
            get { return direction; }
            set
            {
                direction = value;
                direction.Normalize();
            }
        }
    }
}
