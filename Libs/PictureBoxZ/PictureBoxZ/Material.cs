﻿using System;
using OpenTK;

namespace PictureBoxZ
{
    public class Material
    {
        public string Name = String.Empty;
        public Shader Shader;

        public Vector4 Kd = new Vector4(new Vector3(1f), 1f);
        public Vector3 Ks = new Vector3(0.3f);
        public float Shininess = 50.0f;
    }
}
