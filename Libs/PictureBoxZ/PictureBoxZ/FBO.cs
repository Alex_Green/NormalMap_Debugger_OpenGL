﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace PictureBoxZ
{
    public enum EditingModes
    {
        NoOperation = 0,
        InvertR,
        InvertG,
        InvertB,
        InvertA,
        InvertRGB,
        Normalize,
        RestoreB,
        ClearB,
        FlipRG,
        FlipGB,
        FlipBA,
        Rotate270,
        Rotate90,
        Rotate180,
        FlipHorizontally,
        FlipVertically,
        NormalMapGen,
        HeightMapGen,
        HeightMapNormalize
    }

    public enum RgbToGrayModes
    {
        Desaturate = 0,
        Average,
        Minimum,
        Maximum,
        Red,
        Green,
        Blue,
        Alpha
    }

    public static class FBO_Func
    {
        static public void CheckFBO(string FBO_Name)
        {
            FramebufferErrorCode FramebufferStatus = GL.CheckFramebufferStatus(FramebufferTarget.FramebufferExt);
            if (FramebufferStatus != FramebufferErrorCode.FramebufferCompleteExt)
                MessageBox.Show("\"" + FBO_Name + "\" error: " + FramebufferStatus.ToString(), "GL.CheckFramebufferStatus:", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    public static class FBO_2D
    {
        public static int Edit2D(EditingModes EditingMode, PictureBoxZ control, bool InvertX = false, bool InvertY = false)
        {
            Vector2[] FBO_UVs = new Vector2[] { new Vector2(-1f, -1f), new Vector2(-1f, 1f), new Vector2(1f, 1f), new Vector2(1f, -1f) };
            Shader shader = Shaders.GetShader("Editing2D");

            int Tex_Width, Tex_Height;
            GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
            GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out Tex_Width);
            GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out Tex_Height);

            if (EditingMode == EditingModes.Rotate270 || EditingMode == EditingModes.Rotate90)
            {
                int TempWH = Tex_Width;
                Tex_Width = Tex_Height;
                Tex_Height = TempWH;
                Size ImgSize = control.ImageSize;
                control.ImageSize = new Size(ImgSize.Height, ImgSize.Width);
            }

            GL.Viewport(0, 0, Tex_Width, Tex_Height);

            int texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, Tex_Width, Tex_Height, 0, PixelFormat.Rgba, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            int VBO = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(FBO_UVs.Length * Vector2.SizeInBytes), FBO_UVs, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            int FBO = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, texture, 0);
            FBO_Func.CheckFBO("FBO Editing2D");

            // Draw
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.Blend);
            GL.ClearColor(Engine.GLClearColor);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.UseProgram(shader.ProgramID);

            bool NormalMapMode = (EditingMode == EditingModes.NormalMapGen);
            GL.Uniform1(shader.GetUniform("InvertX"), Convert.ToInt32(NormalMapMode ? Engine.InvertX ^ InvertX : Engine.InvertX));
            GL.Uniform1(shader.GetUniform("InvertY"), Convert.ToInt32(NormalMapMode ? Engine.InvertY ^ InvertY : Engine.InvertY));
            GL.Uniform1(shader.GetUniform("EditingMode"), (int)EditingMode);
            GL.Uniform1(shader.GetUniform("ClearBValue"), Engine.ClearBlueChannelValue);

            if (NormalMapMode)
            {
                GL.Uniform1(shader.GetUniform("NormalMapsCount"), Math.Min(Engine.NormalMapKernels.Length, Engine.NormalMapScales.Length));
                GL.Uniform1(shader.GetUniform("Rgb2GrayMode"), (int)Engine.RgbToGrayMode);
                GL.Uniform1(shader.GetUniform("NMapNormalize"), Convert.ToInt32(Engine.NormalMapNormalize));
                GL.Uniform1(shader.GetUniform("Kernels[0]"), Engine.NormalMapKernels.Length, Engine.NormalMapKernels);
                GL.Uniform1(shader.GetUniform("Scales[0]"), Engine.NormalMapScales.Length, Engine.NormalMapScales);
            }
            else if (EditingMode == EditingModes.HeightMapGen)
            {
                GL.Uniform1(shader.GetUniform("HMap_Step"), Engine.HeightMap_GenStepValue);
            }
            else if (EditingMode == EditingModes.HeightMapNormalize)
            {
                GL.Uniform1(shader.GetUniform("HMap_MinValue"), Engine.HeightMap_MinValue);
                GL.Uniform1(shader.GetUniform("HMap_MaxValue"), Engine.HeightMap_MaxValue);
            }

            //Bind Textures
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
            if (Engine.AnisotropicFiltering && Engine.AnisotropicFilteringMaxValue > 1f)
                GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, 1f);

            shader.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.VertexAttribPointer(shader.GetAttribute("v_UV"), 2, VertexAttribPointerType.Float, false, 0, 0);
            GL.DrawArrays(PrimitiveType.Quads, 0, 4);

            shader.DisableVertexAttribArrays();

            // Free
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0); //Bind default FrameBuffer
            GL.DeleteFramebuffer(FBO);
            GL.DeleteBuffer(VBO);
            GL.Viewport(0, 0, control.ClientSize.Width, control.ClientSize.Height);
            return texture;
        }
    }

    public static class FBO_3D
    {
        public static Shader Shader_PP;
        public static int FBO_PP;
        public static int Texture_PP, Texture_Depth;

        static int ScreenWidth, ScreenHeight;
        static int VBO;
        static Vector2[] FBO_UVs = new Vector2[] { new Vector2(-1f, -1f), new Vector2(-1f, 1f), new Vector2(1f, 1f), new Vector2(1f, -1f) };

        public static void Init(int ScrWidth, int ScrHeight)
        {
            ScreenWidth = ScrWidth;
            ScreenHeight = ScrHeight;

            Shader_PP = Shaders.GetShader("PostProcess");

            Texture_PP = GL.GenTexture();
            Texture_Depth = GL.GenTexture();

            Rescale();

            // Gen PostProcess FrameBuffer
            FBO_PP = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBO_PP);
            GL.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, TextureTarget.Texture2D, Texture_Depth, 0);
            GL.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, Texture_PP, 0);
            FBO_Func.CheckFBO("FBO_PostProcess");

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0); //Bind default FrameBuffer

            VBO = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(FBO_UVs.Length * Vector2.SizeInBytes), FBO_UVs, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        static void Rescale()
        {
            // Depth
            GL.BindTexture(TextureTarget.Texture2D, Texture_Depth);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent32f, ScreenWidth, ScreenHeight, 0, PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            // Post Process
            GL.BindTexture(TextureTarget.Texture2D, Texture_PP);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba16f, ScreenWidth, ScreenHeight, 0, PixelFormat.Rgba, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public static void Draw_PostPocess()
        {
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); //Bind default FrameBuffer
            GL.Disable(EnableCap.DepthTest);
            GL.ClearColor(Engine.GLClearColor);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.UseProgram(Shader_PP.ProgramID);
            GL.Uniform1(Shader_PP.GetUniform("FXAAEnabled"), Convert.ToInt32(Engine.UseFXAA));

            //Bind Textures
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, Texture_PP);

            Shader_PP.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.VertexAttribPointer(Shader_PP.GetAttribute("v_UV"), 2, VertexAttribPointerType.Float, false, 0, 0);
            GL.DrawArrays(PrimitiveType.Quads, 0, 4);

            Shader_PP.DisableVertexAttribArrays();
        }

        public static void Free()
        {
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);

            Shader_PP = null;

            GL.DeleteTexture(Texture_PP);
            GL.DeleteTexture(Texture_Depth);
            GL.DeleteBuffer(VBO);
            GL.DeleteFramebuffer(FBO_PP);

            VBO = 0;
            FBO_PP = 0;
            Texture_PP = 0;
            Texture_Depth = 0;
        }
    }
}