﻿using System.Drawing;
using OpenTK;
using OpenTK.Graphics;

namespace PictureBoxZ
{
    public class ColorRGBA
    {
        public float R = 0f, G = 0f, B = 0f, A = 1f;

        public ColorRGBA()
        {
        }

        public ColorRGBA(byte Value)
        {
            R = Value / 255f;
            G = Value / 255f;
            B = Value / 255f;
        }

        public ColorRGBA(byte R, byte G, byte B)
        {
            this.R = R / 255f;
            this.G = G / 255f;
            this.B = B / 255f;
        }

        public ColorRGBA(byte R, byte G, byte B, byte A)
        {
            this.R = R / 255f;
            this.G = G / 255f;
            this.B = B / 255f;
            this.A = A / 255f;
        }

        public ColorRGBA(float Value)
        {
            R = Value;
            G = Value;
            B = Value;
        }

        public ColorRGBA(float R, float G, float B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }

        public ColorRGBA(float R, float G, float B, float A)
        {
            this.R = R;
            this.G = G;
            this.B = B;
            this.A = A;
        }

        public ColorRGBA(Color color)
        {
            R = color.R / 255f;
            G = color.G / 255f;
            B = color.B / 255f;
            A = color.A / 255f;
        }

        public ColorRGBA(Color4 color)
        {
            R = color.R;
            G = color.G;
            B = color.B;
            A = color.A;
        }

        public ColorRGBA(Vector4 vector)
        {
            R = vector.X;
            G = vector.Y;
            B = vector.Z;
            A = vector.W;
        }

        public ColorRGBA(Vector3 vector)
        {
            R = vector.X;
            G = vector.Y;
            B = vector.Z;
        }

        public static implicit operator Color(ColorRGBA color)
        {
            return Color.FromArgb(
                MathHelper.Clamp((int)(color.A * 255f), 0, 255),
                MathHelper.Clamp((int)(color.R * 255f), 0, 255),
                MathHelper.Clamp((int)(color.G * 255f), 0, 255),
                MathHelper.Clamp((int)(color.B * 255f), 0, 255));
        }

        public static implicit operator Color4(ColorRGBA color)
        {
            return new Color4(color.R, color.G, color.B, color.A);
        }

        public static implicit operator Vector3(ColorRGBA color)
        {
            return new Vector3(color.R, color.G, color.B);
        }

        public static implicit operator Vector4(ColorRGBA color)
        {
            return new Vector4(color.R, color.G, color.B, color.A);
        }

        public static implicit operator ColorRGBA(Color color)
        {
            return new ColorRGBA(color);
        }

        public static implicit operator ColorRGBA(Color4 color)
        {
            return new ColorRGBA(color);
        }

        public static implicit operator ColorRGBA(Vector3 vector)
        {
            return new ColorRGBA(vector);
        }

        public static implicit operator ColorRGBA(Vector4 vector)
        {
            return new ColorRGBA(vector);
        }
    }
}
