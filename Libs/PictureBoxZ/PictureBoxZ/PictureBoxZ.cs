﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace PictureBoxZ
{
    public partial class PictureBoxZ : GLControl
    {
        bool ControlLoaded = false;
        bool is3DView = false;
        Bitmap ImageBMP = null;
        public Size ImageSize = Size.Empty;
        bool Image_MipMaps = false;

        float Stretch_Zoom = 0f;
        int Zoom_Index = 0;
        List<float> Zoom_List = new List<float>();
        Point Mouse_LocationOld = new Point();
        PointF Zoom_CenterPoint = new PointF();

        public PictureBoxZ()
        {
            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture; //For fix parsing values like "0.5" and "0,5"
            MouseWheel += new MouseEventHandler(glControl_MouseWheel);
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                ControlLoaded = true;

                Engine.LoadConfigAndContent();
                FBO_3D.Init(ClientSize.Width, ClientSize.Height);
                Draw2D.Init();
                Engine.Load(); //Load Map

                if (ImageBMP != null)
                {
                    Image = ImageBMP;
                    ImageBMP = null;
                    MipMaps = Image_MipMaps;
                }

                glControl_Resize(sender, e);
            }
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            base.OnHandleDestroyed(e);

            if (DesignMode || !ControlLoaded)
                return;

            Engine.Free(); // Free all: Map -> Meshes, Shaders, Textures...
            FBO_3D.Free();
            Draw2D.Free();

            Shaders.ShadersList.Clear();
            Meshes.MeshesList.Clear();
        }

        private void glControl_MouseEnter(object sender, EventArgs e)
        {
            if (!Focused) Focus(); //Enable focus for MouseWheel
        }

        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Right)
            {
                Mouse_LocationOld = e.Location;
                Cursor = Cursors.SizeAll;
            }
            else if (e.Button == MouseButtons.XButton1)
                ZoomUpdate(false);
            else if (e.Button == MouseButtons.XButton2)
                for (int i = 0; i < ZoomList.Count; i++)
                    if (ZoomList[i] == 1.0f)
                    {
                        ZoomListIndex = i;
                        ZoomUpdate(true);
                    }
        }

        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Right)
            {
                Cursor = Cursors.SizeAll;

                if (is3DView)
                {
                    float Scale = 0.01f;
                    float X = (e.X - Mouse_LocationOld.X) * Scale;
                    float Y = (e.Y - Mouse_LocationOld.Y) * Scale;

                    if (e.Button == MouseButtons.Middle) // Rotate Mesh
                    {
                        Engine.MainMesh.Rotation.Y += MathHelper.DegreesToRadians(X);
                        Engine.MainMesh.Rotation.X += MathHelper.DegreesToRadians(Y);
                    }
                    else if (e.Button == MouseButtons.Right)
                    {
                        MoveLight(e.Location);
                    }
                    Invalidate();
                }
                else
                {
                    MoveImage(e.Location);
                }
            }
        }

        private void glControl_MouseWheel(object sender, MouseEventArgs e)
        {
            if (is3DView)
            {
                if (e.Delta < 0 && Engine.MainCamera.FOV < 179f)
                    Engine.MainCamera.FOV++;
                else if (e.Delta > 0 && Engine.MainCamera.FOV > 1f)
                    Engine.MainCamera.FOV--;

                Invalidate();
            }
            else
            {
                if (e.Delta > 0 && Zoom_Index < Zoom_List.Count - 1)
                    Zoom_Index++;
                else if (e.Delta < 0 && Zoom_Index > 0)
                    Zoom_Index--;

                MoveImage(Mouse_LocationOld);
            }
        }

        private void MoveImage(Point NewMouseCoords)
        {
            #region Zoom_CenterPoint
            Zoom_CenterPoint = new PointF(
                Zoom_CenterPoint.X - (NewMouseCoords.X - Mouse_LocationOld.X) / GetZoom,
                Zoom_CenterPoint.Y - (NewMouseCoords.Y - Mouse_LocationOld.Y) / GetZoom);
            Mouse_LocationOld = NewMouseCoords;

            float WidthZ = ClientSize.Width / GetZoom;
            float HeightZ = ClientSize.Height / GetZoom;
            float Half_WidthZ = WidthZ * 0.5f;
            float Half_HeightZ = HeightZ * 0.5f;

            if (ImageSize.Width > WidthZ)
            {
                if (Zoom_CenterPoint.X - Half_WidthZ < 0f)
                    Zoom_CenterPoint.X = Half_WidthZ;

                if (Zoom_CenterPoint.X + Half_WidthZ > ImageSize.Width)
                    Zoom_CenterPoint.X = ImageSize.Width - Half_WidthZ;
            }
            else
                Zoom_CenterPoint.X = ImageSize.Width * 0.5f;

            if (ImageSize.Height > HeightZ)
            {
                if (Zoom_CenterPoint.Y - Half_HeightZ < 0f)
                    Zoom_CenterPoint.Y = Half_HeightZ;

                if (Zoom_CenterPoint.Y + Half_HeightZ > ImageSize.Height)
                    Zoom_CenterPoint.Y = ImageSize.Height - Half_HeightZ;
            }
            else
                Zoom_CenterPoint.Y = ImageSize.Height * 0.5f;
            #endregion

            Invalidate();
        }

        private void MoveLight(Point NewMouseCoords)
        {
            float Yaw = NewMouseCoords.X / (float)ClientSize.Width * MathHelper.PiOver2 - MathHelper.PiOver4;
            float Pitch = NewMouseCoords.Y / (float)ClientSize.Height * MathHelper.PiOver2 - MathHelper.PiOver4;
            Yaw = Math.Max(Math.Min(MathHelper.PiOver4, Yaw), -MathHelper.PiOver4);
            Pitch = Math.Max(Math.Min(MathHelper.PiOver4, Pitch), -MathHelper.PiOver4);
            Light.Direction = Engine.FromYawPitch(Yaw, -Pitch);
            Invalidate();
        }

        public Color4 GetColorAtPoint(Point MouseLocation)
        {
            if (DesignMode || !ControlLoaded)
                return new Color4(-1f, -1f, -1f, -1f);

            GL.ReadBuffer(ReadBufferMode.FrontLeft);

            if (Is3DView)
            {
                float[] PixelColor = new float[3]; // RGB
                GL.ReadPixels(MouseLocation.X, ClientRectangle.Height - MouseLocation.Y, 1, 1, PixelFormat.Rgb, PixelType.Float, PixelColor);
                return new Color4(PixelColor[0], PixelColor[1], PixelColor[2], 1f);
            }
            else
            {
                float[] PixelColor = new float[4]; // RGBA
                GL.ReadPixels(MouseLocation.X, ClientRectangle.Height - MouseLocation.Y, 1, 1, PixelFormat.Rgba, PixelType.Float, PixelColor);
                return new Color4(PixelColor[0], PixelColor[1], PixelColor[2], PixelColor[3]);
            }
        }

        #region Zoom
        [Category("Zoom")]
        public PointF ZoomCenterPoint
        {
            get
            {
                return new PointF(Zoom_CenterPoint.X / ImageSize.Width, Zoom_CenterPoint.Y / ImageSize.Height);
            }
            set
            {
                Zoom_CenterPoint.X = value.X * ImageSize.Width;
                Zoom_CenterPoint.Y = value.Y * ImageSize.Height;
                MoveImage(Mouse_LocationOld);
            }
        }

        [Category("Zoom")]
        public List<float> ZoomList
        {
            get { return Zoom_List; }
            set
            {
                Zoom_List = value;

                if (Zoom_Index > Zoom_List.Count - 1)
                    Zoom_Index = Zoom_List.Count - 1;
            }
        }

        [Category("Zoom")]
        public int ZoomListIndex
        {
            get { return Zoom_Index; }
            set
            {
                if (value < 0)
                    Zoom_Index = 0;
                else if (value > Zoom_List.Count - 1)
                    Zoom_Index = Zoom_List.Count - 1;
                else
                    Zoom_Index = value;
            }
        }

        [Category("Zoom")]
        public float GetZoom
        {
            get
            {
                if (Zoom_Index < 0 || Zoom_Index >= Zoom_List.Count)
                    return 1f;
                return Zoom_List[Zoom_Index];
            }
        }

        [Category("Zoom")]
        public float GetStretchZoom
        {
            get { return Stretch_Zoom; }
        }

        [Category("Zoom")]
        public void ZoomUpdate(bool RestoreLastZoom = false)
        {
            // Save Old Zoom!
            float LastZoom = GetZoom;

            Zoom_List.Clear();
            Zoom_Index = 0;

            // StretchZoom
            float DeltaW = ClientSize.Width / (float)ImageSize.Width;
            float DeltaH = ClientSize.Height / (float)ImageSize.Height;
            Stretch_Zoom = (DeltaW > DeltaH ? DeltaH : DeltaW);
            Zoom_List.Add(Stretch_Zoom);

            float MaxZoom = 64f;
            float MinZoom = (float)Math.Pow(2.0, Math.Ceiling(Math.Log(64.0 / Math.Max(ImageSize.Width, ImageSize.Height), 2.0)));

            // 1, 2, 4, 8 ... MaxZoom
            for (float i = 1f; i <= MaxZoom; i *= 2f)
                Zoom_List.Add(i);

            // 0.5, 0.25, 0.125 ... MinZoom
            for (float i = 0.5f; i >= MinZoom; i *= 0.5f)
                Zoom_List.Add(i);

            Zoom_List.Sort();

            int Zoom_List_Count = Zoom_List.Count - 1;
            for (int i = 0; i < Zoom_List_Count; i++)
            {
                if (Zoom_List[i] == Zoom_List[i + 1])
                {
                    Zoom_List.RemoveAt(i);
                    Zoom_List_Count--;
                    i--;
                }
            }

            if (RestoreLastZoom) // Restore Last Zoom
            {
                int MinDeltaIndex = 0;
                float MinDelta = Zoom_List[MinDeltaIndex];
                for (int i = 1; i < Zoom_List.Count; i++)
                {
                    float Delta = Math.Abs(Zoom_List[i] - LastZoom);
                    if (Delta < MinDelta)
                    {
                        MinDeltaIndex = i;
                        MinDelta = Delta;
                    }
                }
                Zoom_Index = MinDeltaIndex;
            }
            else
                Zoom_Index = Zoom_List.IndexOf(Stretch_Zoom);

            MoveImage(Mouse_LocationOld);
        }
        #endregion

        #region Prefences
        [Category("OpenGL")]
        [DefaultValue(false)]
        public bool Is3DView
        {
            get { return is3DView; }
            set
            {
                is3DView = value;

                if (DesignMode || !ControlLoaded)
                    return;

                glControl_Resize(null, EventArgs.Empty);
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(false)]
        public bool ShowTexture
        {
            get { return Engine.ShowTexture; }
            set
            {
                Engine.ShowTexture = value;

                if (DesignMode || !ControlLoaded)
                    return;

                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(true)]
        public bool UseTextureAsNormalMap
        {
            get { return Engine.UseTextureAsNormalMap; }
            set
            {
                Engine.UseTextureAsNormalMap = value;

                if (DesignMode || !ControlLoaded)
                    return;

                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(true)]
        public bool UseLight
        {
            get { return Engine.UseLight; }
            set
            {
                Engine.UseLight = value;

                if (DesignMode || !ControlLoaded)
                    return;

                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(true)]
        public bool FXAA
        {
            get { return Engine.UseFXAA; }
            set
            {
                Engine.UseFXAA = value;

                if (DesignMode || !ControlLoaded)
                    return;

                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(true)]
        public bool AnisotropicFiltering
        {
            get { return Engine.AnisotropicFiltering; }
            set
            {
                Engine.AnisotropicFiltering = value;
                if (Image_MipMaps)
                {
                    MipMaps = false;
                    MipMaps = true;
                }
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(1.0f)]
        public float ClearBlueChannelValue
        {
            get { return Engine.ClearBlueChannelValue; }
            set { Engine.ClearBlueChannelValue = value; }
        }

        [Category("OpenGL")]
        [DefaultValue(false)]
        public bool InvertX
        {
            get { return Engine.InvertX; }
            set
            {
                Engine.InvertX = value;

                if (DesignMode || !ControlLoaded)
                    return;

                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(false)]
        public bool InvertY
        {
            get { return Engine.InvertY; }
            set
            {
                Engine.InvertY = value;

                if (DesignMode || !ControlLoaded)
                    return;

                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(null)]
        public Image Image
        {
            get
            {
                if (DesignMode || !ControlLoaded)
                    return ImageBMP;

                if (Engine.TextureID == 0)
                    return null;

                GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
                GL.PixelStore(PixelStoreParameter.PackAlignment, 4);

                int Texture_Width, Texture_Height, Texture_PixelInternalFormat;
                GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out Texture_Width);
                GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out Texture_Height);
                GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureInternalFormat, out Texture_PixelInternalFormat);
                System.Drawing.Imaging.PixelFormat PF = TexFormatConverter.GetPixelFormat((PixelInternalFormat)Texture_PixelInternalFormat);
                PixelFormat GL_PF = TexFormatConverter.GetGLPixelFormat(PF);

                Bitmap BMP = new Bitmap(Texture_Width, Texture_Height, PF);
                BitmapData BmpData = BMP.LockBits(new Rectangle(0, 0, Texture_Width, Texture_Height), ImageLockMode.WriteOnly, BMP.PixelFormat);
                GL.GetTexImage(TextureTarget.Texture2D, 0, GL_PF, PixelType.UnsignedByte, BmpData.Scan0);
                BMP.UnlockBits(BmpData);
                BmpData = null;
                BMP.RotateFlip(RotateFlipType.RotateNoneFlipY); // Flip Y
                return BMP;
            }
            set
            {
                if (DesignMode || !ControlLoaded)
                {
                    ImageBMP = (Bitmap)value;
                    ImageSize = (value == null ? Size.Empty : value.Size);
                }
                else
                {
                    GL.BindTexture(TextureTarget.Texture2D, 0);

                    // CleanUp
                    GL.DeleteTexture(Engine.TextureID);
                    GL.DeleteTexture(Engine.TextureMapGen_OrigID);
                    Engine.TextureID = Engine.TextureMapGen_OrigID = 0;

                    if (value != null)
                    {
                        if (value.PixelFormat == System.Drawing.Imaging.PixelFormat.Format1bppIndexed |
                            value.PixelFormat == System.Drawing.Imaging.PixelFormat.Format4bppIndexed |
                            value.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed |
                            value.PixelFormat == System.Drawing.Imaging.PixelFormat.Indexed)
                        {
                            Bitmap Image_Result = new Bitmap(value.Width, value.Height);
                            using (Graphics G = Graphics.FromImage(Image_Result))
                                G.DrawImageUnscaled(value, 0, 0);
                            value.Dispose();
                            value = Image_Result;
                        }

                        Engine.TextureID = GL.GenTexture();
                        GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
                        GL.PixelStore(PixelStoreParameter.UnpackAlignment, 4);

                        Bitmap BMP = (Bitmap)value.Clone();
                        BMP.RotateFlip(RotateFlipType.RotateNoneFlipY); // Flip Y
                        ImageSize = BMP.Size;
                        Zoom_CenterPoint = new PointF(BMP.Width * 0.5f, BMP.Height * 0.5f);
                        
                        PixelFormat GL_PF = TexFormatConverter.GetGLPixelFormat(BMP.PixelFormat);
                        PixelInternalFormat GL_PIF = TexFormatConverter.GetGLPixelInternalFormat(BMP.PixelFormat);

                        BitmapData BmpData = BMP.LockBits(new Rectangle(Point.Empty, BMP.Size), ImageLockMode.ReadOnly, BMP.PixelFormat);
                        GL.TexImage2D(TextureTarget.Texture2D, 0, GL_PIF, BMP.Width, BMP.Height, 0, GL_PF, PixelType.UnsignedByte, BmpData.Scan0);
                        BMP.UnlockBits(BmpData);
                        BmpData = null;
                        BMP.Dispose();
                        BMP = null;

                        MipMaps = Image_MipMaps; // Generate Mip Maps (if true)
                    }

                    ZoomUpdate(false);
                }
            }
        }

        [Category("OpenGL")]
        [DefaultValue(false)]
        public bool MipMaps
        {
            get { return Image_MipMaps; }
            set
            {
                Image_MipMaps = value;

                if (DesignMode || !ControlLoaded || Engine.TextureID == 0)
                    return;

                GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);

                if (value)
                {
                    GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
                    GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
                }
                else
                {
                    int Texture_Width, Texture_Height, Texture_PixelInternalFormat;
                    GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out Texture_Width);
                    GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out Texture_Height);
                    GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureInternalFormat, out Texture_PixelInternalFormat);

                    int TextureID_New = GL.GenTexture();
                    int FBO = GL.GenFramebuffer();
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO);
                    GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, Engine.TextureID, 0);
                    GL.BindTexture(TextureTarget.Texture2D, TextureID_New);
                    GL.CopyTexImage2D(TextureTarget.Texture2D, 0, (PixelInternalFormat)Texture_PixelInternalFormat, 0, 0, Texture_Width, Texture_Height, 0);

                    GL.DeleteTexture(Engine.TextureID);
                    Engine.TextureID = TextureID_New;
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                    GL.DeleteFramebuffer(FBO);
                    GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
                }

                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(TextureMagFilter.Nearest)]
        public TextureMagFilter MagFilter
        {
            get { return Engine.TextureMagFilter_Default; }
            set
            {
                Engine.TextureMagFilter_Default = value;

                if (DesignMode || !ControlLoaded || Engine.TextureID == 0)
                    return;

                GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)Engine.TextureMagFilter_Default);
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(TextureMinFilter.Nearest)]
        public TextureMinFilter MinFilter
        {
            get { return Engine.TextureMinFilter_Default; }
            set
            {
                Engine.TextureMinFilter_Default = value;

                if (DesignMode || !ControlLoaded || Engine.TextureID == 0)
                    return;

                GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)Engine.TextureMinFilter_Default);
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(TextureMagFilter.Nearest)]
        public TextureMagFilter BadPixels_MagFilter
        {
            get { return Engine.TextureMagFilter_BadPixels; }
            set
            {
                Engine.TextureMagFilter_BadPixels = value;
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(TextureMinFilter.Nearest)]
        public TextureMinFilter BadPixels_MinFilter
        {
            get { return Engine.TextureMinFilter_BadPixels; }
            set
            {
                Engine.TextureMinFilter_BadPixels = value;
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(0.02f)]
        public float BadPixels_Threshold
        {
            get { return Engine.BadPixel_Threshold; }
            set
            {
                Engine.BadPixel_Threshold = value;
                Invalidate();
            }
        }

        [Category("OpenGL")]
        public Color BadPixels_ColorBad
        {
            get { return Color.FromArgb((int)Engine.BadPixel_ColorBad.X * 255, (int)Engine.BadPixel_ColorBad.Y * 255, (int)Engine.BadPixel_ColorBad.Z * 255); }
            set
            {
                Engine.BadPixel_ColorBad.X = value.R / 255f;
                Engine.BadPixel_ColorBad.Y = value.G / 255f;
                Engine.BadPixel_ColorBad.Z = value.B / 255f;
                Invalidate();
            }
        }

        [Category("OpenGL")]
        public Color BadPixels_ColorGood
        {
            get { return Color.FromArgb((int)Engine.BadPixel_ColorGood.X * 255, (int)Engine.BadPixel_ColorGood.Y * 255, (int)Engine.BadPixel_ColorGood.Z * 255); }
            set
            {
                Engine.BadPixel_ColorGood.X = value.R / 255f;
                Engine.BadPixel_ColorGood.Y = value.G / 255f;
                Engine.BadPixel_ColorGood.Z = value.B / 255f;
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(DrawModes2D.RGB)]
        public DrawModes2D DrawMode2D
        {
            get { return Engine.DrawMode2D; }
            set
            {
                Engine.DrawMode2D = value;
                Invalidate();
            }
        }

        [Category("OpenGL")]
        [DefaultValue(true)]
        public bool Draw2D_UseAlpha
        {
            get { return Engine.Draw2D_UseAlpha; }
            set
            {
                Engine.Draw2D_UseAlpha = value;
                Invalidate();
            }
        }
        #endregion

        private void glControl_Resize(object sender, EventArgs e)
        {
            if (DesignMode || !ControlLoaded || ClientSize.Width <= 0 || ClientSize.Height <= 0)
                return;

            GL.Viewport(0, 0, ClientSize.Width, ClientSize.Height);

            if (is3DView)
            {
                FBO_3D.Free();
                FBO_3D.Init(ClientSize.Width, ClientSize.Height);
                Engine.MainCamera.SetProjectionMatrix(ProjectionTypes.Perspective, ClientSize.Width, ClientSize.Height, Engine.MainCamera.zNear, Engine.MainCamera.zFar, Engine.MainCamera.FOV);
            }
            else
            {
                Draw2D.ProjectionMatrix = Matrix4.CreateOrthographic(ClientSize.Width, ClientSize.Height, -1f, 1f);
                ZoomUpdate(true);
            }
        }

        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            if (DesignMode || !ControlLoaded)
                return;

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)Engine.TextureMagFilter_Default);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)Engine.TextureMinFilter_Default);

            if (Engine.AnisotropicFilteringMaxValue > 1f)
                GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt,
                    is3DView && Engine.AnisotropicFiltering ? Engine.AnisotropicFilteringMaxValue : 1f);

            if (is3DView)
            {
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
                Draw3D.Draw();
            }
            else
            {
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

                float Zoom = GetZoom;
                float X1 = -Zoom_CenterPoint.X * Zoom;
                float X2 = X1 + ImageSize.Width * Zoom;
                float Y2 = Zoom_CenterPoint.Y * Zoom;
                float Y1 = Y2 - ImageSize.Height * Zoom;
                Draw2D.Draw(X1, X2, Y1, Y2);
            }

            SwapBuffers();
        }
    }
}