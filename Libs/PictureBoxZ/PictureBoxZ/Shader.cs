﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace PictureBoxZ
{
    public static class Shaders
    {
        public static List<Shader> ShadersList = new List<Shader>();

        public static void LoadShaders()
        {
            try
            {
                XmlDocument XML = new XmlDocument();
                XmlNodeList xmlNodeList;

                XML.InnerXml = Properties.Resources.Shaders_xml;
                xmlNodeList = XML.DocumentElement.SelectNodes("Shader");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    Shader S = new Shader();
                    S.Name = xmlNode.SelectSingleNode("Name").InnerText;
                    S.VS_Code = Properties.Resources.ResourceManager.GetString(xmlNode.SelectSingleNode("Vertex").InnerText);
                    S.FS_Code = Properties.Resources.ResourceManager.GetString(xmlNode.SelectSingleNode("Fragment").InnerText);

                    if (xmlNode.SelectNodes("Geometry").Count > 0)
                        S.GS_Code = Properties.Resources.ResourceManager.GetString(xmlNode.SelectSingleNode("Geometry").InnerText);

                    if (xmlNode.SelectNodes("TessControl").Count > 0)
                        S.TCS_Code = Properties.Resources.ResourceManager.GetString(xmlNode.SelectSingleNode("TessControl").InnerText);

                    if (xmlNode.SelectNodes("TessEvaluation").Count > 0)
                        S.TES_Code = Properties.Resources.ResourceManager.GetString(xmlNode.SelectSingleNode("TessEvaluation").InnerText);

                    if (xmlNode.SelectNodes("Compute").Count > 0)
                        S.CS_Code = Properties.Resources.ResourceManager.GetString(xmlNode.SelectSingleNode("Compute").InnerText);

                    ShadersList.Add(S);
                    Load(S.Name);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Shaders.LoadShaders() Exception.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static Shader Load(string Name)
        {
            try
            {
                Shader S = GetShader(Name);

                if (S == null)
                {
                    foreach (var i in ShadersList)
                        if (i.Name.GetHashCode() == Name.GetHashCode())
                            S = i;
                    if (S == null)
                        return null;
                }

                S.LoadShader();

                // Проверка шейдера на ошибки
                string InfoLog = GL.GetProgramInfoLog(S.ProgramID);
                if (InfoLog.Trim() != String.Empty)
                    MessageBox.Show(InfoLog, "ShaderProgram \"" + S.Name + "\" InfoLog:", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return S;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Shaders.Load() Exception, Name: \"" + Name + "\"", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public static Shader GetShader(string Name)
        {
            foreach (var i in ShadersList)
                if (i.Name.GetHashCode() == Name.GetHashCode())
                    return i;
            return null;
        }
    }

    public class UniformInfo
    {
        public String name = "";
        public int address = -1;
        public int size = 0;
        public ActiveUniformType type;

        public UniformInfo() { }

        public UniformInfo(string Name, int Address)
        {
            name = Name;
            address = Address;
        }
    }

    public class AttributeInfo
    {
        public String name = "";
        public int address = -1;
        public int size = 0;
        public ActiveAttribType type;

        public AttributeInfo() { }

        public AttributeInfo(string Name, int Address)
        {
            name = Name;
            address = Address;
        }
    }

    public class Shader
    {
        public int
            ProgramID = 0, //Shader program ID
            VS_ID = 0, //Vertex Shader ID
            FS_ID = 0, //Fragment Shader ID
            GS_ID = 0, //Geometry Shader ID
            TCS_ID = 0, //Tesselation Control Shader ID
            TES_ID = 0, //Tesselation Evaluation Shader ID
            CS_ID = 0; //Compute Shader ID

        public int
            AttributeCount = 0,
            UniformCount = 0;

        public string Name = String.Empty,
            VS_Code = String.Empty,
            FS_Code = String.Empty,
            GS_Code = String.Empty,
            TCS_Code = String.Empty,
            TES_Code = String.Empty,
            CS_Code = String.Empty;

        public List<AttributeInfo> Attributes = new List<AttributeInfo>();
        public List<UniformInfo> Uniforms = new List<UniformInfo>();

        public void LoadShader()
        {
            Free();
            ProgramID = GL.CreateProgram();

            LoadShaderFromString(VS_Code, ShaderType.VertexShader);
            LoadShaderFromString(FS_Code, ShaderType.FragmentShader);

            if (GS_Code != String.Empty)
                LoadShaderFromString(GS_Code, ShaderType.GeometryShader);

            if (TCS_Code != String.Empty)
                LoadShaderFromString(TCS_Code, ShaderType.TessControlShader);

            if (TES_Code != String.Empty)
                LoadShaderFromString(TES_Code, ShaderType.TessEvaluationShader);

            if (CS_Code != String.Empty)
                LoadShaderFromString(CS_Code, ShaderType.ComputeShader);

            Link();
            TextureUnits();

            #region Check for Errors
            int LinkStatus;
            GL.GetProgram(ProgramID, GetProgramParameterName.LinkStatus, out LinkStatus);
            if (LinkStatus != 1)
            {
                string StrManyEquals = String.Empty;
                StrManyEquals += StrManyEquals.PadRight(50, '=');

                string InfoLog = GL.GetProgramInfoLog(ProgramID).Trim();
                MessageBox.Show("GetProgramInfoLog:\n" + InfoLog, "Shader Program Error: \"" + Name + "\"", MessageBoxButtons.OK, MessageBoxIcon.Error);

                InfoLog = GL.GetShaderInfoLog(VS_ID).Trim();
                if (InfoLog != String.Empty)
                {
                    Debug.WriteLine(StrManyEquals);
                    Debug.WriteLine("GetShaderInfoLog:\n" + InfoLog);
                    Debug.WriteLine("Vertex shader code:");
                    Debug.WriteLine(VS_Code);
                }

                InfoLog = GL.GetShaderInfoLog(FS_ID).Trim();
                if (InfoLog != String.Empty)
                {
                    Debug.WriteLine(StrManyEquals);
                    Debug.WriteLine("GetShaderInfoLog:\n" + InfoLog);
                    Debug.WriteLine("Fragment shader code:");
                    Debug.WriteLine(FS_Code);
                }

                if (GS_Code != String.Empty)
                {
                    InfoLog = GL.GetShaderInfoLog(GS_ID).Trim();
                    if (InfoLog != String.Empty)
                    {
                        Debug.WriteLine(StrManyEquals);
                        Debug.WriteLine("GetShaderInfoLog:\n" + InfoLog);
                        Debug.WriteLine("Geometry shader code:");
                        Debug.WriteLine(GS_Code);
                    }
                }

                if (TCS_Code != String.Empty)
                {
                    InfoLog = GL.GetShaderInfoLog(TCS_ID).Trim();
                    if (InfoLog != String.Empty)
                    {
                        Debug.WriteLine(StrManyEquals);
                        Debug.WriteLine("GetShaderInfoLog:\n" + InfoLog);
                        Debug.WriteLine("Tesselation Control shader code:");
                        Debug.WriteLine(TCS_Code);
                    }
                }

                if (TES_Code != String.Empty)
                {
                    InfoLog = GL.GetShaderInfoLog(TES_ID).Trim();
                    if (InfoLog != String.Empty)
                    {
                        Debug.WriteLine(StrManyEquals);
                        Debug.WriteLine("GetShaderInfoLog:\n" + InfoLog);
                        Debug.WriteLine("Tesselation Evaluation shader code:");
                        Debug.WriteLine(TES_Code);
                    }
                }

                if (CS_Code != String.Empty)
                {
                    InfoLog = GL.GetShaderInfoLog(CS_ID).Trim();
                    if (InfoLog != String.Empty)
                    {
                        Debug.WriteLine(StrManyEquals);
                        Debug.WriteLine("GetShaderInfoLog:\n" + InfoLog);
                        Debug.WriteLine("Compute shader code:");
                        Debug.WriteLine(CS_Code);
                    }
                }
            }
            else
            {
                VS_Code = null;
                FS_Code = null;
                GS_Code = null;
                TCS_Code = null;
                TES_Code = null;
                CS_Code = null;
            }
            #endregion
        }

        void loadShader(String code, ShaderType type, out int shader)
        {
            shader = GL.CreateShader(type);
            GL.ShaderSource(shader, code);
            GL.CompileShader(shader);
            GL.AttachShader(ProgramID, shader);
        }

        void LoadShaderFromString(String code, ShaderType type)
        {
            try
            {
                code = DeleteComments(code);

                switch (type)
                {
                    case ShaderType.VertexShader:
                        loadShader(code, type, out VS_ID);
                        break;
                    case ShaderType.FragmentShader:
                        loadShader(code, type, out FS_ID);
                        break;
                    case ShaderType.GeometryShader:
                        loadShader(code, type, out GS_ID);
                        break;
                    case ShaderType.TessControlShader:
                        loadShader(code, type, out TCS_ID);
                        break;
                    case ShaderType.TessEvaluationShader:
                        loadShader(code, type, out TES_ID);
                        break;
                    case ShaderType.ComputeShader:
                        loadShader(code, type, out CS_ID);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("See DebugOutput for details...", "LoadShaderFromString() Exception!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (code != String.Empty)
                    Debug.WriteLine(code);
            }
        }

        static string DeleteComments(string Str)
        {
            try
            {
                const string ConstCommentString = "//";
                const string ConstCommentEndLine = "\n";
                const string ConstCommentStart = "/*";
                const string ConstCommentStop = "*/";

                int Start, StartIndex, End, Len;
                do
                {
                    Start = Str.IndexOf(ConstCommentStart);
                    End = -1;
                    if (Start != -1)
                    {
                        StartIndex = Start + ConstCommentStart.Length;
                        if (StartIndex < Str.Length)
                            End = Str.IndexOf(ConstCommentStop, StartIndex);

                        Len = End - Start + ConstCommentStop.Length;
                        if (Start < End)
                            Str = Str.Remove(Start, Len);
                    }
                } while (Start != -1 && End != -1);

                do
                {
                    Start = Str.IndexOf(ConstCommentString);
                    End = Str.Length;
                    if (Start != -1)
                    {
                        StartIndex = Start + ConstCommentString.Length;
                        if (StartIndex < Str.Length)
                            End = Str.IndexOf(ConstCommentEndLine, StartIndex);
                        if (End == -1)
                            End = Str.Length;

                        Len = End - Start;
                        if (Start < End)
                            Str = Str.Remove(Start, Len);
                    }
                } while (Start != -1);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + Str, "Delete Comments Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return Str;
        }

        public void Link()
        {
            GL.LinkProgram(ProgramID);

            GL.GetProgram(ProgramID, GetProgramParameterName.ActiveAttributes, out AttributeCount);
            GL.GetProgram(ProgramID, GetProgramParameterName.ActiveUniforms, out UniformCount);

            for (int i = 0; i < AttributeCount; i++)
            {
                AttributeInfo info = new AttributeInfo();
                int length = 0;

                StringBuilder name = new StringBuilder();

                GL.GetActiveAttrib(ProgramID, i, 256, out length, out info.size, out info.type, name);

                info.name = name.ToString();
                info.address = GL.GetAttribLocation(ProgramID, info.name);
                Attributes.Add(info);
            }

            for (int i = 0; i < UniformCount; i++)
            {
                UniformInfo info = new UniformInfo();
                int length = 0;

                StringBuilder name = new StringBuilder();

                GL.GetActiveUniform(ProgramID, i, 256, out length, out info.size, out info.type, name);

                info.name = name.ToString();
                info.address = GL.GetUniformLocation(ProgramID, info.name);
                Uniforms.Add(info);
            }
        }

        public void TextureUnits()
        {
            GL.UseProgram(ProgramID);

            for (int i = 0; i < Engine.TextureImageUnits; i++)
            {
                int TextureUnitLocation = GetUniform("TextureUnit" + i.ToString());
                if (TextureUnitLocation != -1)
                    GL.Uniform1(TextureUnitLocation, i);
            }

            GL.UseProgram(0);
        }

        public void EnableVertexAttribArrays()
        {
            for (int i = 0; i < Attributes.Count; i++)
                GL.EnableVertexAttribArray(Attributes[i].address);
        }

        public void DisableVertexAttribArrays()
        {
            for (int i = 0; i < Attributes.Count; i++)
                GL.DisableVertexAttribArray(Attributes[i].address);
        }

        public int GetAttribute(string name)
        {
            foreach (var item in Attributes)
                if (item.name == name)
                    return item.address;
            return -1;
        }

        public int GetUniform(string name)
        {
            foreach (var item in Uniforms)
                if (item.name == name)
                    return item.address;
            return -1;
        }

        public void Free()
        {
            GL.DetachShader(ProgramID, VS_ID);
            GL.DetachShader(ProgramID, FS_ID);
            GL.DetachShader(ProgramID, GS_ID);
            GL.DetachShader(ProgramID, TCS_ID);
            GL.DetachShader(ProgramID, TES_ID);
            GL.DetachShader(ProgramID, CS_ID);

            GL.DeleteShader(VS_ID);
            GL.DeleteShader(FS_ID);
            GL.DeleteShader(GS_ID);
            GL.DeleteShader(TCS_ID);
            GL.DeleteShader(TES_ID);
            GL.DeleteShader(CS_ID);

            GL.DeleteProgram(ProgramID);

            Attributes.Clear();
            Uniforms.Clear();

            AttributeCount = 0;
            UniformCount = 0;

            VS_ID = 0;
            FS_ID = 0;
            GS_ID = 0;
            TCS_ID = 0;
            TES_ID = 0;
            CS_ID = 0;
            ProgramID = 0;
        }
    }
}
