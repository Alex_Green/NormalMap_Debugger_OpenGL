﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace PictureBoxZ
{
    public enum DrawModes2D
    {
        RGB = 0,
        Red,
        Green,
        Blue,
        Alpha,
        BadPixels
    }

    public static class Draw2D
    {
        public static Shader shader;
        public static Matrix4 ProjectionMatrix = Matrix4.Identity;

        static int VertexBufferID, UVBufferID;
        static Vector2[] VBO_Vertexes = new Vector2[4];
        static Vector2[] VBO_UVs = new Vector2[] { new Vector2(0f, 0f), new Vector2(0f, 1f), new Vector2(1f, 1f), new Vector2(1f, 0f) };

        public static void Init()
        {
            shader = Shaders.GetShader("View2D");

            VertexBufferID = GL.GenBuffer();
            UVBufferID = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferID);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(VBO_Vertexes.Length * Vector2.SizeInBytes), VBO_Vertexes, BufferUsageHint.DynamicDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, UVBufferID);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(VBO_UVs.Length * Vector2.SizeInBytes), VBO_UVs, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public static void Draw(float X1, float X2, float Y1, float Y2)
        {
            #region Update Vertexes
            VBO_Vertexes[0] = new Vector2(X1, Y1);
            VBO_Vertexes[1] = new Vector2(X1, Y2);
            VBO_Vertexes[2] = new Vector2(X2, Y2);
            VBO_Vertexes[3] = new Vector2(X2, Y1);

            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferID);
            GL.BufferSubData(BufferTarget.ArrayBuffer, IntPtr.Zero, (IntPtr)(VBO_Vertexes.Length * Vector2.SizeInBytes), VBO_Vertexes);
            #endregion

            GL.Disable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.ClearColor(Engine.GLClearColor);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.UseProgram(shader.ProgramID);

            if (Engine.TextureID != 0)
            {
                GL.UniformMatrix4(shader.GetUniform("MVP"), false, ref ProjectionMatrix); // Model and View matrices are Identity.

                GL.Uniform1(shader.GetUniform("DrawMode"), (int)Engine.DrawMode2D);
                if (Engine.DrawMode2D == DrawModes2D.BadPixels)
                {
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)Engine.TextureMagFilter_BadPixels);
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)Engine.TextureMinFilter_BadPixels);

                    GL.Uniform1(shader.GetUniform("BadPixThreshold"), Engine.BadPixel_Threshold);
                    GL.Uniform3(shader.GetUniform("BadPixColorBad"), Engine.BadPixel_ColorBad);
                    GL.Uniform3(shader.GetUniform("BadPixColorGood"), Engine.BadPixel_ColorGood);
                }

                GL.Uniform1(shader.GetUniform("UseAlpha"), Convert.ToInt32(Engine.Draw2D_UseAlpha));

                shader.EnableVertexAttribArrays();

                GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferID);
                GL.VertexAttribPointer(shader.GetAttribute("v_Position"), 2, VertexAttribPointerType.Float, false, 0, 0);
                GL.BindBuffer(BufferTarget.ArrayBuffer, UVBufferID);
                GL.VertexAttribPointer(shader.GetAttribute("v_UV"), 2, VertexAttribPointerType.Float, false, 0, 0);

                GL.DrawArrays(PrimitiveType.Quads, 0, 4);
            }
            shader.DisableVertexAttribArrays();
        }

        public static void Free()
        {
            shader = null;

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(VertexBufferID);
            GL.DeleteBuffer(UVBufferID);
            VertexBufferID = UVBufferID = 0;
        }
    }

    public static class Draw3D
    {
        public static void Draw()
        {
            // G-Buffer
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBO_3D.FBO_PP);
            GL.Enable(EnableCap.DepthTest); // Включаем тест глубины
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.CullFace);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.ClearColor(Engine.GLClearColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Engine.MainMesh.CalculateMatrices(Engine.MainCamera);
            Shader shader = Engine.MainMaterial.Shader;
            GL.UseProgram(shader.ProgramID);

            for (int i = 0; i < Engine.MainMesh.Parts.Count; i++)
            {
                int TempLocation;
                #region Камера и матрицы
                // Передаем шейдеру матрицу ModelMatrix, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("ModelMatrix");
                if (TempLocation != -1)
                    GL.UniformMatrix4(TempLocation, false, ref Engine.MainMesh.ModelMatrix);

                // Передаем шейдеру матрицу ViewMatrix, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("ViewMatrix");
                if (TempLocation != -1)
                {
                    Matrix4 V = Engine.MainCamera.GetViewMatrix();
                    GL.UniformMatrix4(TempLocation, false, ref V);
                }

                // Передаем шейдеру матрицу ProjectionMatrix, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("ProjectionMatrix");
                if (TempLocation != -1)
                {
                    Matrix4 P = Engine.MainCamera.GetProjectionMatrix();
                    GL.UniformMatrix4(TempLocation, false, ref P);
                }

                // Передаем шейдеру матрицу ModelView, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("ModelView");
                if (TempLocation != -1)
                    GL.UniformMatrix4(TempLocation, false, ref Engine.MainMesh.ModelViewMatrix);

                // Передаем шейдеру матрицу NormalMatrix, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("NormalMatrix");
                if (TempLocation != -1)
                {
                    Matrix3 NormalMatrix = new Matrix3(Engine.MainMesh.ModelMatrix);
                    NormalMatrix.Invert();
                    NormalMatrix.Transpose();
                    GL.UniformMatrix3(TempLocation, false, ref NormalMatrix);
                }

                // Передаем шейдеру матрицу ModelViewProjection, если шейдер поддерживает это (должна быть 100% поддержка).
                TempLocation = shader.GetUniform("MVP");
                if (TempLocation != -1)
                    GL.UniformMatrix4(TempLocation, false, ref Engine.MainMesh.ModelViewProjectionMatrix);

                // Передаем шейдеру позицию камеры, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("CameraPos");
                if (TempLocation != -1)
                    GL.Uniform3(TempLocation, Engine.MainCamera.Position);
                #endregion

                #region Передача различных параметров шейдерам
                GL.Uniform4(shader.GetUniform("Material_Kd"), Engine.MainMaterial.Kd);
                GL.Uniform3(shader.GetUniform("Material_Ks"), Engine.MainMaterial.Ks);
                GL.Uniform1(shader.GetUniform("Material_S"), Engine.MainMaterial.Shininess);

                // Lights
                GL.Uniform1(shader.GetUniform("UseLight"), Convert.ToInt32(Engine.UseLight));
                GL.Uniform3(shader.GetUniform("Light_Dir"), Light.Direction);
                GL.Uniform3(shader.GetUniform("Light_Ld"), Light.Diffuse);
                GL.Uniform3(shader.GetUniform("Light_Ls"), Light.Specular);

                TempLocation = shader.GetUniform("ShowTexture");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, Convert.ToInt32(Engine.ShowTexture));

                TempLocation = shader.GetUniform("TextureAsNMap");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, Convert.ToInt32(Engine.UseTextureAsNormalMap));

                TempLocation = shader.GetUniform("InvertX");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, Convert.ToUInt32(Engine.InvertX));

                TempLocation = shader.GetUniform("InvertY");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, Convert.ToUInt32(Engine.InvertY));
                #endregion

                shader.EnableVertexAttribArrays();

                #region Передаем шейдеру VertexPosition, VertexNormal, VertexUV, VertexTangents
                // Передаем шейдеру буфер позицый вертексов, если шейдер поддерживает это (должна быть 100% поддержка).
                TempLocation = shader.GetAttribute("v_Position");
                if (TempLocation != -1)
                {
                    GL.BindBuffer(BufferTarget.ArrayBuffer, Engine.MainMesh.Parts[i].VertexBufferID);
                    GL.VertexAttribPointer(TempLocation, 3, VertexAttribPointerType.Float, false, 0, 0);
                }

                // Передаем шейдеру буфер нормалей, если шейдер поддерживает это.
                TempLocation = shader.GetAttribute("v_Normal");
                if (TempLocation != -1)
                {
                    GL.BindBuffer(BufferTarget.ArrayBuffer, Engine.MainMesh.Parts[i].NormalBufferID);
                    GL.VertexAttribPointer(TempLocation, 3, VertexAttribPointerType.Float, false, 0, 0);
                }

                // Передаем шейдеру буфер текстурных координат, если шейдер поддерживает это.
                TempLocation = shader.GetAttribute("v_UV");
                if (TempLocation != -1)
                {
                    GL.BindBuffer(BufferTarget.ArrayBuffer, Engine.MainMesh.Parts[i].UVBufferID);
                    GL.VertexAttribPointer(TempLocation, 2, VertexAttribPointerType.Float, false, 0, 0);
                }

                // Передаем шейдеру буфер тангенсов, если шейдер поддерживает это.
                TempLocation = shader.GetAttribute("v_Tan");
                if (TempLocation != -1)
                {
                    GL.BindBuffer(BufferTarget.ArrayBuffer, Engine.MainMesh.Parts[i].TangentBufferID);
                    GL.VertexAttribPointer(TempLocation, 3, VertexAttribPointerType.Float, false, 0, 0);
                }
                #endregion

                GL.DrawArrays(PrimitiveType.Triangles, 0, Engine.MainMesh.Parts[i].Vertexes.Length);

                shader.DisableVertexAttribArrays();
            }

            FBO_3D.Draw_PostPocess(); // Draw PostProcessed Result to Screen (FBO = 0)
        }
    }
}