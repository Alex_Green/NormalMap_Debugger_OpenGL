﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PictureBoxZ;

namespace PictureBoxZ_Test
{
    public partial class Form1 : Form
    {
        Bitmap bmp;

        public Form1()
        {
            InitializeComponent();
            pictureBoxZ1.Image = bmp = new Bitmap("Test.png");

            TextureMinFilter MinFilter = pictureBoxZ1.MinFilter;
            comboBoxMinFilter.DataSource = (TextureMinFilter[])Enum.GetValues(typeof(TextureMinFilter));
            comboBoxMinFilter.SelectedItem = MinFilter;

            TextureMagFilter MagFilter = pictureBoxZ1.MagFilter;
            comboBoxMagFilter.DataSource = (TextureMagFilter[])Enum.GetValues(typeof(TextureMagFilter));
            comboBoxMagFilter.SelectedItem = MagFilter;

            checkBoxMipMaps.Checked = pictureBoxZ1.MipMaps;

            DrawModes2D DrawMode2D = pictureBoxZ1.DrawMode2D;
            comboBoxDrawMode2D.DataSource = (DrawModes2D[])Enum.GetValues(typeof(DrawModes2D));
            comboBoxDrawMode2D.SelectedItem = DrawMode2D;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
            else if (e.KeyCode == Keys.F1)
                pictureBoxZ1.Image = (pictureBoxZ1.Image == null ? bmp : null);
            else if (e.KeyCode == Keys.F2)
            {
                BackgroundImage = (pictureBoxZ1.Visible ? pictureBoxZ1.Image : null);
                pictureBoxZ1.Visible = !pictureBoxZ1.Visible;
            }
            else if (e.KeyCode == Keys.F3)
                panelSettings.Visible = !panelSettings.Visible;
            else if (e.KeyCode == Keys.F4)
            {
                EditingModes EditingMode = EditingModes.NormalMapGen; //TODO

                int NewTexture = FBO_2D.Edit2D(EditingMode, pictureBoxZ1, false, false);
                GL.DeleteTexture(Engine.TextureID);
                Engine.TextureID = NewTexture;
                if (pictureBoxZ1.MipMaps) //Regenerate MipMaps
                {
                    pictureBoxZ1.MipMaps = false;
                    pictureBoxZ1.MipMaps = true;
                }

                if (EditingMode == EditingModes.Rotate270 || EditingMode == EditingModes.Rotate90)
                    pictureBoxZ1.ZoomUpdate(false);

                pictureBoxZ1.Invalidate();
            }
        }

        private void pictureBoxZ1_MouseClick_Move(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.X >= 0 && e.Y >= 0 &&
                e.X < pictureBoxZ1.ClientRectangle.Width && e.Y < pictureBoxZ1.ClientRectangle.Height)
            {
                Color NMapColor = Color.FromArgb(pictureBoxZ1.GetColorAtPoint(e.Location).ToArgb());
                Text = String.Format("RGBA: [{0}; {1}; {2}; {3}]", NMapColor.R, NMapColor.G, NMapColor.B, NMapColor.A);
            }
        }

        private void comboBoxMinFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.MinFilter = (TextureMinFilter)comboBoxMinFilter.SelectedItem;
        }

        private void comboBoxMagFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.MagFilter = (TextureMagFilter)comboBoxMagFilter.SelectedItem;
        }

        private void checkBoxMipMaps_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.MipMaps = checkBoxMipMaps.Checked;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.Is3DView = checkBox3D.Checked;
        }

        private void comboBoxDrawMode2D_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.DrawMode2D = (DrawModes2D)comboBoxDrawMode2D.SelectedItem;
        }
    }
}
