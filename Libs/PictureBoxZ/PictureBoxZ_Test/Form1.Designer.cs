﻿namespace PictureBoxZ_Test
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelSettings = new System.Windows.Forms.Panel();
            this.labelDrawMode2D = new System.Windows.Forms.Label();
            this.comboBoxDrawMode2D = new System.Windows.Forms.ComboBox();
            this.checkBox3D = new System.Windows.Forms.CheckBox();
            this.checkBoxMipMaps = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxMagFilter = new System.Windows.Forms.ComboBox();
            this.comboBoxMinFilter = new System.Windows.Forms.ComboBox();
            this.pictureBoxZ1 = new PictureBoxZ.PictureBoxZ();
            this.panelSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSettings
            // 
            this.panelSettings.BackColor = System.Drawing.SystemColors.Control;
            this.panelSettings.Controls.Add(this.labelDrawMode2D);
            this.panelSettings.Controls.Add(this.comboBoxDrawMode2D);
            this.panelSettings.Controls.Add(this.checkBox3D);
            this.panelSettings.Controls.Add(this.checkBoxMipMaps);
            this.panelSettings.Controls.Add(this.label2);
            this.panelSettings.Controls.Add(this.label1);
            this.panelSettings.Controls.Add(this.comboBoxMagFilter);
            this.panelSettings.Controls.Add(this.comboBoxMinFilter);
            this.panelSettings.Location = new System.Drawing.Point(0, 0);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(246, 102);
            this.panelSettings.TabIndex = 1;
            this.panelSettings.Visible = false;
            // 
            // labelDrawMode2D
            // 
            this.labelDrawMode2D.AutoSize = true;
            this.labelDrawMode2D.Location = new System.Drawing.Point(5, 81);
            this.labelDrawMode2D.Name = "labelDrawMode2D";
            this.labelDrawMode2D.Size = new System.Drawing.Size(76, 13);
            this.labelDrawMode2D.TabIndex = 7;
            this.labelDrawMode2D.Text = "DrawMode2D:";
            // 
            // comboBoxDrawMode2D
            // 
            this.comboBoxDrawMode2D.FormattingEnabled = true;
            this.comboBoxDrawMode2D.Location = new System.Drawing.Point(87, 78);
            this.comboBoxDrawMode2D.Name = "comboBoxDrawMode2D";
            this.comboBoxDrawMode2D.Size = new System.Drawing.Size(153, 21);
            this.comboBoxDrawMode2D.TabIndex = 6;
            this.comboBoxDrawMode2D.SelectedIndexChanged += new System.EventHandler(this.comboBoxDrawMode2D_SelectedIndexChanged);
            // 
            // checkBox3D
            // 
            this.checkBox3D.AutoSize = true;
            this.checkBox3D.Location = new System.Drawing.Point(103, 56);
            this.checkBox3D.Name = "checkBox3D";
            this.checkBox3D.Size = new System.Drawing.Size(110, 17);
            this.checkBox3D.TabIndex = 5;
            this.checkBox3D.Text = "2D/3D (checked)";
            this.checkBox3D.UseVisualStyleBackColor = true;
            this.checkBox3D.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBoxMipMaps
            // 
            this.checkBoxMipMaps.AutoSize = true;
            this.checkBoxMipMaps.Location = new System.Drawing.Point(6, 56);
            this.checkBoxMipMaps.Name = "checkBoxMipMaps";
            this.checkBoxMipMaps.Size = new System.Drawing.Size(91, 17);
            this.checkBoxMipMaps.TabIndex = 4;
            this.checkBoxMipMaps.Text = "Use MipMaps";
            this.checkBoxMipMaps.UseVisualStyleBackColor = true;
            this.checkBoxMipMaps.CheckedChanged += new System.EventHandler(this.checkBoxMipMaps_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "MagFilter:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "MinFilter:";
            // 
            // comboBoxMagFilter
            // 
            this.comboBoxMagFilter.FormattingEnabled = true;
            this.comboBoxMagFilter.Location = new System.Drawing.Point(58, 30);
            this.comboBoxMagFilter.Name = "comboBoxMagFilter";
            this.comboBoxMagFilter.Size = new System.Drawing.Size(182, 21);
            this.comboBoxMagFilter.TabIndex = 1;
            this.comboBoxMagFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxMagFilter_SelectedIndexChanged);
            // 
            // comboBoxMinFilter
            // 
            this.comboBoxMinFilter.FormattingEnabled = true;
            this.comboBoxMinFilter.Location = new System.Drawing.Point(58, 3);
            this.comboBoxMinFilter.Name = "comboBoxMinFilter";
            this.comboBoxMinFilter.Size = new System.Drawing.Size(182, 21);
            this.comboBoxMinFilter.TabIndex = 0;
            this.comboBoxMinFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxMinFilter_SelectedIndexChanged);
            // 
            // pictureBoxZ1
            // 
            this.pictureBoxZ1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.pictureBoxZ1.BadPixels_ColorBad = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBoxZ1.BadPixels_ColorGood = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBoxZ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxZ1.InvertY = true;
            this.pictureBoxZ1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxZ1.MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear;
            this.pictureBoxZ1.Name = "pictureBoxZ1";
            this.pictureBoxZ1.Size = new System.Drawing.Size(284, 261);
            this.pictureBoxZ1.TabIndex = 0;
            this.pictureBoxZ1.VSync = true;
            this.pictureBoxZ1.ZoomCenterPoint = ((System.Drawing.PointF)(resources.GetObject("pictureBoxZ1.ZoomCenterPoint")));
            this.pictureBoxZ1.ZoomList = ((System.Collections.Generic.List<float>)(resources.GetObject("pictureBoxZ1.ZoomList")));
            this.pictureBoxZ1.ZoomListIndex = -1;
            this.pictureBoxZ1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClick_Move);
            this.pictureBoxZ1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClick_Move);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.panelSettings);
            this.Controls.Add(this.pictureBoxZ1);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PictureBoxZ Test (F1-F3)";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.panelSettings.ResumeLayout(false);
            this.panelSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBoxZ.PictureBoxZ pictureBoxZ1;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxMagFilter;
        private System.Windows.Forms.ComboBox comboBoxMinFilter;
        private System.Windows.Forms.CheckBox checkBoxMipMaps;
        private System.Windows.Forms.CheckBox checkBox3D;
        private System.Windows.Forms.Label labelDrawMode2D;
        private System.Windows.Forms.ComboBox comboBoxDrawMode2D;
    }
}

