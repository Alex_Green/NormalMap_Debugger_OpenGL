System.Drawing.PSD
==================

PSD loader for .NET written entirely in managed C#

Link: https://github.com/bizzehdee/System.Drawing.PSD

License: BSD