﻿namespace System.Drawing.PSD
{
    internal class Parallel
    {
        public delegate void DelegateFor(int i);
        public delegate void DelegateProcess();

        /// <summary>
        /// Parallel for loop. Invokes given action, passing arguments
        /// fromInclusive - toExclusive on multiple threads.
        /// Returns when loop finished.
        /// </summary>
        public static void For(int from, int to, DelegateFor delFor)
        {
            // chunkSize = 1 makes items to be processed in order.
            // Bigger chunk size should reduce lock waiting time and thus increase paralelism.
            int Step = 4;
            int Count = from - Step;
            int ThreadCount = Environment.ProcessorCount;

            //Now let's take the next chunk 
            DelegateProcess process = delegate ()
            {
                while (true)
                {
                    int iter = 0;
                    lock (typeof(Parallel))
                    {
                        Count += Step;
                        iter = Count;
                    }
                    for (int i = iter; i < iter + Step; i++)
                    {
                        if (i >= to)
                            return;
                        delFor(i);
                    }
                }
            };

            //IAsyncResult array to launch Thread(s)
            IAsyncResult[] asyncResults = new IAsyncResult[ThreadCount];
            for (int i = 0; i < ThreadCount; ++i)
            {
                asyncResults[i] = process.BeginInvoke(null, null);
            }
            //EndInvoke to wait for all threads to be completed
            for (int i = 0; i < ThreadCount; ++i)
            {
                process.EndInvoke(asyncResults[i]);
            }
        }
    }
}
