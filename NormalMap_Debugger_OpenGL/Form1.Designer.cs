﻿namespace NormalMap_Debugger
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox_ShowTexIn3D = new System.Windows.Forms.CheckBox();
            this.checkBox_UseLightIn3D = new System.Windows.Forms.CheckBox();
            this.checkBox_UseTexAsNMap = new System.Windows.Forms.CheckBox();
            this.checkBox_2D_3D = new System.Windows.Forms.CheckBox();
            this.radioButtonBadPixels = new System.Windows.Forms.RadioButton();
            this.label_3DModel = new System.Windows.Forms.Label();
            this.comboBox_Mashes = new System.Windows.Forms.ComboBox();
            this.radioButtonA = new System.Windows.Forms.RadioButton();
            this.radioButtonRGB = new System.Windows.Forms.RadioButton();
            this.radioButtonB = new System.Windows.Forms.RadioButton();
            this.radioButtonG = new System.Windows.Forms.RadioButton();
            this.radioButtonR = new System.Windows.Forms.RadioButton();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_ToNMap = new System.Windows.Forms.TabPage();
            this.button_NMap_Cancel = new System.Windows.Forms.Button();
            this.label_GenRGB2Gray = new System.Windows.Forms.Label();
            this.comboBox_GenRGB2Gray = new System.Windows.Forms.ComboBox();
            this.controlsListKernels = new ControlsList.ControlsList();
            this.button_NMap_Apply = new System.Windows.Forms.Button();
            this.button_NMap_Gen = new System.Windows.Forms.Button();
            this.checkBox_GenNormalize = new System.Windows.Forms.CheckBox();
            this.checkBox_GenInvY = new System.Windows.Forms.CheckBox();
            this.checkBox_GenInvX = new System.Windows.Forms.CheckBox();
            this.tabPage_ToHeight = new System.Windows.Forms.TabPage();
            this.checkBox_HMap_CPU_Precompute = new System.Windows.Forms.CheckBox();
            this.checkBox_HMap_Invert = new System.Windows.Forms.CheckBox();
            this.button_HMap_Cancel = new System.Windows.Forms.Button();
            this.button_HMap_Apply = new System.Windows.Forms.Button();
            this.button_HMap_Gen = new System.Windows.Forms.Button();
            this.sliderTrackbar_HMap_Repeat = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_HMap_Step = new System.Windows.Forms.SliderTrackbar();
            this.tabPage_Settings = new System.Windows.Forms.TabPage();
            this.checkBox_Draw2D_UseAlpha = new System.Windows.Forms.CheckBox();
            this.checkBox_VSync = new System.Windows.Forms.CheckBox();
            this.checkBox_AnisotropicFiltering = new System.Windows.Forms.CheckBox();
            this.checkBox_FXAA = new System.Windows.Forms.CheckBox();
            this.checkBox_MipMaps = new System.Windows.Forms.CheckBox();
            this.comboBox_SettingsBadPixelsMinFilter = new System.Windows.Forms.ComboBox();
            this.label_SettingsBadPixelsMinFilter = new System.Windows.Forms.Label();
            this.label_SettingsBadPixelsMagFilter = new System.Windows.Forms.Label();
            this.comboBox_SettingsBadPixelsMagFilter = new System.Windows.Forms.ComboBox();
            this.comboBox_SettingsViewMinFilter = new System.Windows.Forms.ComboBox();
            this.label_SettingsViewMinFilter = new System.Windows.Forms.Label();
            this.buttonSaveLocalizationFile = new System.Windows.Forms.Button();
            this.label_SettingsViewMagFilter = new System.Windows.Forms.Label();
            this.comboBox_SettingsViewMagFilter = new System.Windows.Forms.ComboBox();
            this.buttonSettingsSave = new System.Windows.Forms.Button();
            this.radioButtonSettingsPosG = new System.Windows.Forms.RadioButton();
            this.radioButtonSettingsNegG = new System.Windows.Forms.RadioButton();
            this.groupBoxInfo = new System.Windows.Forms.GroupBox();
            this.checkBox_IgnoreB = new System.Windows.Forms.CheckBox();
            this.label_RGB = new System.Windows.Forms.Label();
            this.label_X = new System.Windows.Forms.Label();
            this.label_Length = new System.Windows.Forms.Label();
            this.label_Y = new System.Windows.Forms.Label();
            this.label_Z = new System.Windows.Forms.Label();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDlg = new System.Windows.Forms.SaveFileDialog();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveAs = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonInvR = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInvG = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInvB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInvA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNormalize = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRestoreB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonClearB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonFlipRG = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipGB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipBA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRotate270 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRotate90 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRotate180 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipH = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipV = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripIconMenu = new System.Windows.Forms.ToolStrip();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBoxZ = new PictureBoxZ.PictureBoxZ();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage_ToNMap.SuspendLayout();
            this.tabPage_ToHeight.SuspendLayout();
            this.tabPage_Settings.SuspendLayout();
            this.groupBoxInfo.SuspendLayout();
            this.toolStripIconMenu.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxInfo, 1, 0);
            this.tableLayoutPanel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(285, 530);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.checkBox_ShowTexIn3D);
            this.panel1.Controls.Add(this.checkBox_UseLightIn3D);
            this.panel1.Controls.Add(this.checkBox_UseTexAsNMap);
            this.panel1.Controls.Add(this.checkBox_2D_3D);
            this.panel1.Controls.Add(this.radioButtonBadPixels);
            this.panel1.Controls.Add(this.label_3DModel);
            this.panel1.Controls.Add(this.comboBox_Mashes);
            this.panel1.Controls.Add(this.radioButtonA);
            this.panel1.Controls.Add(this.radioButtonRGB);
            this.panel1.Controls.Add(this.radioButtonB);
            this.panel1.Controls.Add(this.radioButtonG);
            this.panel1.Controls.Add(this.radioButtonR);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 131);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(279, 92);
            this.panel1.TabIndex = 3;
            // 
            // checkBox_ShowTexIn3D
            // 
            this.checkBox_ShowTexIn3D.AutoSize = true;
            this.checkBox_ShowTexIn3D.Location = new System.Drawing.Point(128, 72);
            this.checkBox_ShowTexIn3D.Name = "checkBox_ShowTexIn3D";
            this.checkBox_ShowTexIn3D.Size = new System.Drawing.Size(126, 17);
            this.checkBox_ShowTexIn3D.TabIndex = 30;
            this.checkBox_ShowTexIn3D.Text = "Show Texture in 3D?";
            this.checkBox_ShowTexIn3D.UseVisualStyleBackColor = true;
            this.checkBox_ShowTexIn3D.CheckedChanged += new System.EventHandler(this.checkBoxes_3D_CheckedChanged);
            // 
            // checkBox_UseLightIn3D
            // 
            this.checkBox_UseLightIn3D.AutoSize = true;
            this.checkBox_UseLightIn3D.Checked = true;
            this.checkBox_UseLightIn3D.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_UseLightIn3D.Location = new System.Drawing.Point(5, 72);
            this.checkBox_UseLightIn3D.Name = "checkBox_UseLightIn3D";
            this.checkBox_UseLightIn3D.Size = new System.Drawing.Size(105, 17);
            this.checkBox_UseLightIn3D.TabIndex = 29;
            this.checkBox_UseLightIn3D.Text = "Use Light in 3D?";
            this.checkBox_UseLightIn3D.UseVisualStyleBackColor = true;
            this.checkBox_UseLightIn3D.CheckedChanged += new System.EventHandler(this.checkBoxes_3D_CheckedChanged);
            // 
            // checkBox_UseTexAsNMap
            // 
            this.checkBox_UseTexAsNMap.AutoSize = true;
            this.checkBox_UseTexAsNMap.Checked = true;
            this.checkBox_UseTexAsNMap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_UseTexAsNMap.Location = new System.Drawing.Point(76, 49);
            this.checkBox_UseTexAsNMap.Name = "checkBox_UseTexAsNMap";
            this.checkBox_UseTexAsNMap.Size = new System.Drawing.Size(194, 17);
            this.checkBox_UseTexAsNMap.TabIndex = 28;
            this.checkBox_UseTexAsNMap.Text = "Use texture as NormalMap (3D ony)";
            this.checkBox_UseTexAsNMap.UseVisualStyleBackColor = true;
            this.checkBox_UseTexAsNMap.CheckedChanged += new System.EventHandler(this.checkBoxes_3D_CheckedChanged);
            // 
            // checkBox_2D_3D
            // 
            this.checkBox_2D_3D.AutoSize = true;
            this.checkBox_2D_3D.Location = new System.Drawing.Point(5, 49);
            this.checkBox_2D_3D.Name = "checkBox_2D_3D";
            this.checkBox_2D_3D.Size = new System.Drawing.Size(65, 17);
            this.checkBox_2D_3D.TabIndex = 27;
            this.checkBox_2D_3D.Text = "2D / 3D";
            this.checkBox_2D_3D.UseVisualStyleBackColor = true;
            this.checkBox_2D_3D.CheckedChanged += new System.EventHandler(this.checkBoxes_3D_CheckedChanged);
            // 
            // radioButtonBadPixels
            // 
            this.radioButtonBadPixels.AutoSize = true;
            this.radioButtonBadPixels.Location = new System.Drawing.Point(5, 26);
            this.radioButtonBadPixels.Name = "radioButtonBadPixels";
            this.radioButtonBadPixels.Size = new System.Drawing.Size(74, 17);
            this.radioButtonBadPixels.TabIndex = 5;
            this.radioButtonBadPixels.Text = "Bad Pixels";
            this.radioButtonBadPixels.UseVisualStyleBackColor = true;
            this.radioButtonBadPixels.CheckedChanged += new System.EventHandler(this.radioButtonBadPixels_CheckedChanged);
            // 
            // label_3DModel
            // 
            this.label_3DModel.Location = new System.Drawing.Point(95, 27);
            this.label_3DModel.Name = "label_3DModel";
            this.label_3DModel.Size = new System.Drawing.Size(61, 17);
            this.label_3DModel.TabIndex = 22;
            this.label_3DModel.Text = "3D Model:";
            this.label_3DModel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox_Mashes
            // 
            this.comboBox_Mashes.FormattingEnabled = true;
            this.comboBox_Mashes.Items.AddRange(new object[] {
            "Box",
            "Chamfer Box",
            "Cylinder",
            "Sphere"});
            this.comboBox_Mashes.Location = new System.Drawing.Point(161, 26);
            this.comboBox_Mashes.Name = "comboBox_Mashes";
            this.comboBox_Mashes.Size = new System.Drawing.Size(108, 21);
            this.comboBox_Mashes.TabIndex = 22;
            this.comboBox_Mashes.Text = "Box";
            this.comboBox_Mashes.SelectedIndexChanged += new System.EventHandler(this.comboBox_Mashes_SelectedIndexChanged);
            // 
            // radioButtonA
            // 
            this.radioButtonA.AutoSize = true;
            this.radioButtonA.Location = new System.Drawing.Point(216, 3);
            this.radioButtonA.Name = "radioButtonA";
            this.radioButtonA.Size = new System.Drawing.Size(32, 17);
            this.radioButtonA.TabIndex = 4;
            this.radioButtonA.Text = "A";
            this.radioButtonA.UseVisualStyleBackColor = true;
            this.radioButtonA.CheckedChanged += new System.EventHandler(this.radioButtonA_CheckedChanged);
            // 
            // radioButtonRGB
            // 
            this.radioButtonRGB.AutoSize = true;
            this.radioButtonRGB.Checked = true;
            this.radioButtonRGB.Location = new System.Drawing.Point(5, 3);
            this.radioButtonRGB.Name = "radioButtonRGB";
            this.radioButtonRGB.Size = new System.Drawing.Size(89, 17);
            this.radioButtonRGB.TabIndex = 0;
            this.radioButtonRGB.TabStop = true;
            this.radioButtonRGB.Text = "RGB / RGBA";
            this.radioButtonRGB.UseVisualStyleBackColor = true;
            this.radioButtonRGB.CheckedChanged += new System.EventHandler(this.radioButtonRGB_CheckedChanged);
            // 
            // radioButtonB
            // 
            this.radioButtonB.AutoSize = true;
            this.radioButtonB.Location = new System.Drawing.Point(178, 3);
            this.radioButtonB.Name = "radioButtonB";
            this.radioButtonB.Size = new System.Drawing.Size(32, 17);
            this.radioButtonB.TabIndex = 3;
            this.radioButtonB.Text = "B";
            this.radioButtonB.UseVisualStyleBackColor = true;
            this.radioButtonB.CheckedChanged += new System.EventHandler(this.radioButtonB_CheckedChanged);
            // 
            // radioButtonG
            // 
            this.radioButtonG.AutoSize = true;
            this.radioButtonG.Location = new System.Drawing.Point(139, 3);
            this.radioButtonG.Name = "radioButtonG";
            this.radioButtonG.Size = new System.Drawing.Size(33, 17);
            this.radioButtonG.TabIndex = 2;
            this.radioButtonG.Text = "G";
            this.radioButtonG.UseVisualStyleBackColor = true;
            this.radioButtonG.CheckedChanged += new System.EventHandler(this.radioButtonG_CheckedChanged);
            // 
            // radioButtonR
            // 
            this.radioButtonR.AutoSize = true;
            this.radioButtonR.Location = new System.Drawing.Point(100, 3);
            this.radioButtonR.Name = "radioButtonR";
            this.radioButtonR.Size = new System.Drawing.Size(33, 17);
            this.radioButtonR.TabIndex = 1;
            this.radioButtonR.Text = "R";
            this.radioButtonR.UseVisualStyleBackColor = true;
            this.radioButtonR.CheckedChanged += new System.EventHandler(this.radioButtonR_CheckedChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(3, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(122, 122);
            this.pictureBox.TabIndex = 14;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            // 
            // tabControl1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tabControl1, 2);
            this.tabControl1.Controls.Add(this.tabPage_ToNMap);
            this.tabControl1.Controls.Add(this.tabPage_ToHeight);
            this.tabControl1.Controls.Add(this.tabPage_Settings);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 229);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(279, 298);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage_ToNMap
            // 
            this.tabPage_ToNMap.BackColor = System.Drawing.Color.White;
            this.tabPage_ToNMap.Controls.Add(this.button_NMap_Cancel);
            this.tabPage_ToNMap.Controls.Add(this.label_GenRGB2Gray);
            this.tabPage_ToNMap.Controls.Add(this.comboBox_GenRGB2Gray);
            this.tabPage_ToNMap.Controls.Add(this.controlsListKernels);
            this.tabPage_ToNMap.Controls.Add(this.button_NMap_Apply);
            this.tabPage_ToNMap.Controls.Add(this.button_NMap_Gen);
            this.tabPage_ToNMap.Controls.Add(this.checkBox_GenNormalize);
            this.tabPage_ToNMap.Controls.Add(this.checkBox_GenInvY);
            this.tabPage_ToNMap.Controls.Add(this.checkBox_GenInvX);
            this.tabPage_ToNMap.Location = new System.Drawing.Point(4, 22);
            this.tabPage_ToNMap.Name = "tabPage_ToNMap";
            this.tabPage_ToNMap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_ToNMap.Size = new System.Drawing.Size(271, 272);
            this.tabPage_ToNMap.TabIndex = 2;
            this.tabPage_ToNMap.Text = "To NormalMap";
            this.tabPage_ToNMap.Resize += new System.EventHandler(this.tabPageGenNMap_Resize);
            // 
            // button_NMap_Cancel
            // 
            this.button_NMap_Cancel.Enabled = false;
            this.button_NMap_Cancel.Location = new System.Drawing.Point(177, 221);
            this.button_NMap_Cancel.Name = "button_NMap_Cancel";
            this.button_NMap_Cancel.Size = new System.Drawing.Size(88, 45);
            this.button_NMap_Cancel.TabIndex = 22;
            this.button_NMap_Cancel.Text = "Cancel";
            this.button_NMap_Cancel.UseVisualStyleBackColor = true;
            this.button_NMap_Cancel.Click += new System.EventHandler(this.button_H_N_Maps_Cancel_Click);
            // 
            // label_GenRGB2Gray
            // 
            this.label_GenRGB2Gray.Location = new System.Drawing.Point(4, 189);
            this.label_GenRGB2Gray.Name = "label_GenRGB2Gray";
            this.label_GenRGB2Gray.Size = new System.Drawing.Size(102, 21);
            this.label_GenRGB2Gray.TabIndex = 21;
            this.label_GenRGB2Gray.Text = "RGB -> Gray:";
            this.label_GenRGB2Gray.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox_GenRGB2Gray
            // 
            this.comboBox_GenRGB2Gray.FormattingEnabled = true;
            this.comboBox_GenRGB2Gray.Location = new System.Drawing.Point(112, 189);
            this.comboBox_GenRGB2Gray.Name = "comboBox_GenRGB2Gray";
            this.comboBox_GenRGB2Gray.Size = new System.Drawing.Size(153, 21);
            this.comboBox_GenRGB2Gray.TabIndex = 20;
            // 
            // controlsListKernels
            // 
            this.controlsListKernels.AutoScroll = true;
            this.controlsListKernels.AutoSize = true;
            this.controlsListKernels.BackColor = System.Drawing.SystemColors.Control;
            this.controlsListKernels.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.controlsListKernels.ButtonsAddVisible = true;
            this.controlsListKernels.ButtonsDeleteVisible = true;
            this.controlsListKernels.ButtonsUpDownVisible = true;
            this.controlsListKernels.ColorIcons = true;
            this.controlsListKernels.DefaultRowsEnabled = true;
            this.controlsListKernels.Elements = new System.Windows.Forms.Control[0];
            this.controlsListKernels.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.controlsListKernels.Location = new System.Drawing.Point(1, 4);
            this.controlsListKernels.Margin = new System.Windows.Forms.Padding(1);
            this.controlsListKernels.MaxElementsCount = 20;
            this.controlsListKernels.MinElementsCount = 1;
            this.controlsListKernels.MinimumSize = new System.Drawing.Size(32, 32);
            this.controlsListKernels.Name = "controlsListKernels";
            this.controlsListKernels.Size = new System.Drawing.Size(266, 158);
            this.controlsListKernels.TabIndex = 15;
            this.controlsListKernels.WrapContents = false;
            // 
            // button_NMap_Apply
            // 
            this.button_NMap_Apply.Enabled = false;
            this.button_NMap_Apply.Location = new System.Drawing.Point(92, 221);
            this.button_NMap_Apply.Name = "button_NMap_Apply";
            this.button_NMap_Apply.Size = new System.Drawing.Size(79, 45);
            this.button_NMap_Apply.TabIndex = 13;
            this.button_NMap_Apply.Text = "Apply";
            this.button_NMap_Apply.UseVisualStyleBackColor = true;
            this.button_NMap_Apply.Click += new System.EventHandler(this.button_H_N_Maps_Apply_Click);
            // 
            // button_NMap_Gen
            // 
            this.button_NMap_Gen.Location = new System.Drawing.Point(7, 221);
            this.button_NMap_Gen.Name = "button_NMap_Gen";
            this.button_NMap_Gen.Size = new System.Drawing.Size(79, 45);
            this.button_NMap_Gen.TabIndex = 12;
            this.button_NMap_Gen.Text = "Generate";
            this.button_NMap_Gen.UseVisualStyleBackColor = true;
            this.button_NMap_Gen.Click += new System.EventHandler(this.button_NMap_Gen_Click);
            // 
            // checkBox_GenNormalize
            // 
            this.checkBox_GenNormalize.AutoSize = true;
            this.checkBox_GenNormalize.Checked = true;
            this.checkBox_GenNormalize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_GenNormalize.Location = new System.Drawing.Point(159, 166);
            this.checkBox_GenNormalize.Name = "checkBox_GenNormalize";
            this.checkBox_GenNormalize.Size = new System.Drawing.Size(72, 17);
            this.checkBox_GenNormalize.TabIndex = 7;
            this.checkBox_GenNormalize.Text = "Normalize";
            this.checkBox_GenNormalize.UseVisualStyleBackColor = true;
            // 
            // checkBox_GenInvY
            // 
            this.checkBox_GenInvY.AutoSize = true;
            this.checkBox_GenInvY.Location = new System.Drawing.Point(80, 166);
            this.checkBox_GenInvY.Name = "checkBox_GenInvY";
            this.checkBox_GenInvY.Size = new System.Drawing.Size(63, 17);
            this.checkBox_GenInvY.TabIndex = 4;
            this.checkBox_GenInvY.Text = "Invert Y";
            this.checkBox_GenInvY.UseVisualStyleBackColor = true;
            // 
            // checkBox_GenInvX
            // 
            this.checkBox_GenInvX.AutoSize = true;
            this.checkBox_GenInvX.Location = new System.Drawing.Point(6, 166);
            this.checkBox_GenInvX.Name = "checkBox_GenInvX";
            this.checkBox_GenInvX.Size = new System.Drawing.Size(63, 17);
            this.checkBox_GenInvX.TabIndex = 2;
            this.checkBox_GenInvX.Text = "Invert X";
            this.checkBox_GenInvX.UseVisualStyleBackColor = true;
            // 
            // tabPage_ToHeight
            // 
            this.tabPage_ToHeight.Controls.Add(this.checkBox_HMap_CPU_Precompute);
            this.tabPage_ToHeight.Controls.Add(this.checkBox_HMap_Invert);
            this.tabPage_ToHeight.Controls.Add(this.button_HMap_Cancel);
            this.tabPage_ToHeight.Controls.Add(this.button_HMap_Apply);
            this.tabPage_ToHeight.Controls.Add(this.button_HMap_Gen);
            this.tabPage_ToHeight.Controls.Add(this.sliderTrackbar_HMap_Repeat);
            this.tabPage_ToHeight.Controls.Add(this.sliderTrackbar_HMap_Step);
            this.tabPage_ToHeight.Location = new System.Drawing.Point(4, 22);
            this.tabPage_ToHeight.Name = "tabPage_ToHeight";
            this.tabPage_ToHeight.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_ToHeight.Size = new System.Drawing.Size(271, 272);
            this.tabPage_ToHeight.TabIndex = 3;
            this.tabPage_ToHeight.Text = "To Height";
            this.tabPage_ToHeight.UseVisualStyleBackColor = true;
            // 
            // checkBox_HMap_CPU_Precompute
            // 
            this.checkBox_HMap_CPU_Precompute.AutoSize = true;
            this.checkBox_HMap_CPU_Precompute.Checked = true;
            this.checkBox_HMap_CPU_Precompute.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_HMap_CPU_Precompute.Location = new System.Drawing.Point(147, 9);
            this.checkBox_HMap_CPU_Precompute.Name = "checkBox_HMap_CPU_Precompute";
            this.checkBox_HMap_CPU_Precompute.Size = new System.Drawing.Size(116, 17);
            this.checkBox_HMap_CPU_Precompute.TabIndex = 27;
            this.checkBox_HMap_CPU_Precompute.Text = "CPU Precomputing";
            this.checkBox_HMap_CPU_Precompute.UseVisualStyleBackColor = true;
            // 
            // checkBox_HMap_Invert
            // 
            this.checkBox_HMap_Invert.AutoSize = true;
            this.checkBox_HMap_Invert.Location = new System.Drawing.Point(147, 35);
            this.checkBox_HMap_Invert.Name = "checkBox_HMap_Invert";
            this.checkBox_HMap_Invert.Size = new System.Drawing.Size(53, 17);
            this.checkBox_HMap_Invert.TabIndex = 26;
            this.checkBox_HMap_Invert.Text = "Invert";
            this.checkBox_HMap_Invert.UseVisualStyleBackColor = true;
            // 
            // button_HMap_Cancel
            // 
            this.button_HMap_Cancel.Enabled = false;
            this.button_HMap_Cancel.Location = new System.Drawing.Point(176, 58);
            this.button_HMap_Cancel.Name = "button_HMap_Cancel";
            this.button_HMap_Cancel.Size = new System.Drawing.Size(88, 45);
            this.button_HMap_Cancel.TabIndex = 25;
            this.button_HMap_Cancel.Text = "Cancel";
            this.button_HMap_Cancel.UseVisualStyleBackColor = true;
            this.button_HMap_Cancel.Click += new System.EventHandler(this.button_H_N_Maps_Cancel_Click);
            // 
            // button_HMap_Apply
            // 
            this.button_HMap_Apply.Enabled = false;
            this.button_HMap_Apply.Location = new System.Drawing.Point(91, 58);
            this.button_HMap_Apply.Name = "button_HMap_Apply";
            this.button_HMap_Apply.Size = new System.Drawing.Size(79, 45);
            this.button_HMap_Apply.TabIndex = 24;
            this.button_HMap_Apply.Text = "Apply";
            this.button_HMap_Apply.UseVisualStyleBackColor = true;
            this.button_HMap_Apply.Click += new System.EventHandler(this.button_H_N_Maps_Apply_Click);
            // 
            // button_HMap_Gen
            // 
            this.button_HMap_Gen.Location = new System.Drawing.Point(6, 58);
            this.button_HMap_Gen.Name = "button_HMap_Gen";
            this.button_HMap_Gen.Size = new System.Drawing.Size(79, 45);
            this.button_HMap_Gen.TabIndex = 23;
            this.button_HMap_Gen.Text = "Generate";
            this.button_HMap_Gen.UseVisualStyleBackColor = true;
            this.button_HMap_Gen.Click += new System.EventHandler(this.button_HMap_Gen_Click);
            // 
            // sliderTrackbar_HMap_Repeat
            // 
            this.sliderTrackbar_HMap_Repeat.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Repeat.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_HMap_Repeat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_HMap_Repeat.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Repeat.BarBlend_FactorPos")));
            this.sliderTrackbar_HMap_Repeat.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Repeat.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_HMap_Repeat.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Repeat.BgBlend_FactorPos")));
            this.sliderTrackbar_HMap_Repeat.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Repeat.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_HMap_Repeat.Location = new System.Drawing.Point(6, 32);
            this.sliderTrackbar_HMap_Repeat.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.sliderTrackbar_HMap_Repeat.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_HMap_Repeat.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_HMap_Repeat.Name = "sliderTrackbar_HMap_Repeat";
            this.sliderTrackbar_HMap_Repeat.SelectedItem = null;
            this.sliderTrackbar_HMap_Repeat.Size = new System.Drawing.Size(135, 20);
            this.sliderTrackbar_HMap_Repeat.SliderMaximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.sliderTrackbar_HMap_Repeat.SliderMinimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_HMap_Repeat.TabIndex = 10;
            this.sliderTrackbar_HMap_Repeat.TextInputStringFormat = "0";
            this.sliderTrackbar_HMap_Repeat.TextStringFormat = "Loops/Step: {0:0}";
            this.sliderTrackbar_HMap_Repeat.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.sliderTrackbar_HMap_Repeat.ValueDefault = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // sliderTrackbar_HMap_Step
            // 
            this.sliderTrackbar_HMap_Step.ArrayOfValues = new string[] {
        "0",
        "1",
        "2",
        "4",
        "8",
        "16",
        "32",
        "64",
        "128",
        "256",
        "512"};
            this.sliderTrackbar_HMap_Step.ArrayOfValuesEnabled = true;
            this.sliderTrackbar_HMap_Step.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Step.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_HMap_Step.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_HMap_Step.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Step.BarBlend_FactorPos")));
            this.sliderTrackbar_HMap_Step.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Step.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_HMap_Step.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Step.BgBlend_FactorPos")));
            this.sliderTrackbar_HMap_Step.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_HMap_Step.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_HMap_Step.DefaultIndex = 6;
            this.sliderTrackbar_HMap_Step.Location = new System.Drawing.Point(6, 6);
            this.sliderTrackbar_HMap_Step.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_HMap_Step.Name = "sliderTrackbar_HMap_Step";
            this.sliderTrackbar_HMap_Step.SelectedIndex = 6;
            this.sliderTrackbar_HMap_Step.SelectedItem = "32";
            this.sliderTrackbar_HMap_Step.Size = new System.Drawing.Size(135, 20);
            this.sliderTrackbar_HMap_Step.TabIndex = 9;
            this.sliderTrackbar_HMap_Step.TextInputStringFormat = "0";
            this.sliderTrackbar_HMap_Step.TextStringFormat = "Step: {0}";
            // 
            // tabPage_Settings
            // 
            this.tabPage_Settings.Controls.Add(this.checkBox_Draw2D_UseAlpha);
            this.tabPage_Settings.Controls.Add(this.checkBox_VSync);
            this.tabPage_Settings.Controls.Add(this.checkBox_AnisotropicFiltering);
            this.tabPage_Settings.Controls.Add(this.checkBox_FXAA);
            this.tabPage_Settings.Controls.Add(this.checkBox_MipMaps);
            this.tabPage_Settings.Controls.Add(this.comboBox_SettingsBadPixelsMinFilter);
            this.tabPage_Settings.Controls.Add(this.label_SettingsBadPixelsMinFilter);
            this.tabPage_Settings.Controls.Add(this.label_SettingsBadPixelsMagFilter);
            this.tabPage_Settings.Controls.Add(this.comboBox_SettingsBadPixelsMagFilter);
            this.tabPage_Settings.Controls.Add(this.comboBox_SettingsViewMinFilter);
            this.tabPage_Settings.Controls.Add(this.label_SettingsViewMinFilter);
            this.tabPage_Settings.Controls.Add(this.buttonSaveLocalizationFile);
            this.tabPage_Settings.Controls.Add(this.label_SettingsViewMagFilter);
            this.tabPage_Settings.Controls.Add(this.comboBox_SettingsViewMagFilter);
            this.tabPage_Settings.Controls.Add(this.buttonSettingsSave);
            this.tabPage_Settings.Controls.Add(this.radioButtonSettingsPosG);
            this.tabPage_Settings.Controls.Add(this.radioButtonSettingsNegG);
            this.tabPage_Settings.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Settings.Name = "tabPage_Settings";
            this.tabPage_Settings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Settings.Size = new System.Drawing.Size(271, 272);
            this.tabPage_Settings.TabIndex = 1;
            this.tabPage_Settings.Text = "Settings";
            this.tabPage_Settings.UseVisualStyleBackColor = true;
            // 
            // checkBox_Draw2D_UseAlpha
            // 
            this.checkBox_Draw2D_UseAlpha.AutoSize = true;
            this.checkBox_Draw2D_UseAlpha.Checked = true;
            this.checkBox_Draw2D_UseAlpha.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_Draw2D_UseAlpha.Location = new System.Drawing.Point(129, 192);
            this.checkBox_Draw2D_UseAlpha.Name = "checkBox_Draw2D_UseAlpha";
            this.checkBox_Draw2D_UseAlpha.Size = new System.Drawing.Size(124, 17);
            this.checkBox_Draw2D_UseAlpha.TabIndex = 14;
            this.checkBox_Draw2D_UseAlpha.Text = "UseAlpha (Draw2d)?";
            this.checkBox_Draw2D_UseAlpha.UseVisualStyleBackColor = true;
            this.checkBox_Draw2D_UseAlpha.CheckedChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // checkBox_VSync
            // 
            this.checkBox_VSync.AutoSize = true;
            this.checkBox_VSync.Checked = true;
            this.checkBox_VSync.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_VSync.Location = new System.Drawing.Point(81, 215);
            this.checkBox_VSync.Name = "checkBox_VSync";
            this.checkBox_VSync.Size = new System.Drawing.Size(57, 17);
            this.checkBox_VSync.TabIndex = 13;
            this.checkBox_VSync.Text = "VSync";
            this.checkBox_VSync.UseVisualStyleBackColor = true;
            this.checkBox_VSync.CheckedChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // checkBox_AnisotropicFiltering
            // 
            this.checkBox_AnisotropicFiltering.AutoSize = true;
            this.checkBox_AnisotropicFiltering.Checked = true;
            this.checkBox_AnisotropicFiltering.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_AnisotropicFiltering.Location = new System.Drawing.Point(6, 192);
            this.checkBox_AnisotropicFiltering.Name = "checkBox_AnisotropicFiltering";
            this.checkBox_AnisotropicFiltering.Size = new System.Drawing.Size(117, 17);
            this.checkBox_AnisotropicFiltering.TabIndex = 10;
            this.checkBox_AnisotropicFiltering.Text = "Anisotropic Filtering";
            this.checkBox_AnisotropicFiltering.UseVisualStyleBackColor = true;
            this.checkBox_AnisotropicFiltering.CheckedChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // checkBox_FXAA
            // 
            this.checkBox_FXAA.AutoSize = true;
            this.checkBox_FXAA.Checked = true;
            this.checkBox_FXAA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_FXAA.Location = new System.Drawing.Point(144, 215);
            this.checkBox_FXAA.Name = "checkBox_FXAA";
            this.checkBox_FXAA.Size = new System.Drawing.Size(53, 17);
            this.checkBox_FXAA.TabIndex = 11;
            this.checkBox_FXAA.Text = "FXAA";
            this.checkBox_FXAA.UseVisualStyleBackColor = true;
            this.checkBox_FXAA.CheckedChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // checkBox_MipMaps
            // 
            this.checkBox_MipMaps.AutoSize = true;
            this.checkBox_MipMaps.Location = new System.Drawing.Point(6, 215);
            this.checkBox_MipMaps.Name = "checkBox_MipMaps";
            this.checkBox_MipMaps.Size = new System.Drawing.Size(69, 17);
            this.checkBox_MipMaps.TabIndex = 12;
            this.checkBox_MipMaps.Text = "MipMaps";
            this.checkBox_MipMaps.UseVisualStyleBackColor = true;
            this.checkBox_MipMaps.CheckedChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // comboBox_SettingsBadPixelsMinFilter
            // 
            this.comboBox_SettingsBadPixelsMinFilter.FormattingEnabled = true;
            this.comboBox_SettingsBadPixelsMinFilter.Location = new System.Drawing.Point(124, 165);
            this.comboBox_SettingsBadPixelsMinFilter.Name = "comboBox_SettingsBadPixelsMinFilter";
            this.comboBox_SettingsBadPixelsMinFilter.Size = new System.Drawing.Size(124, 21);
            this.comboBox_SettingsBadPixelsMinFilter.TabIndex = 9;
            this.comboBox_SettingsBadPixelsMinFilter.SelectedIndexChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // label_SettingsBadPixelsMinFilter
            // 
            this.label_SettingsBadPixelsMinFilter.Location = new System.Drawing.Point(6, 165);
            this.label_SettingsBadPixelsMinFilter.Name = "label_SettingsBadPixelsMinFilter";
            this.label_SettingsBadPixelsMinFilter.Size = new System.Drawing.Size(112, 21);
            this.label_SettingsBadPixelsMinFilter.TabIndex = 8;
            this.label_SettingsBadPixelsMinFilter.Text = "BadPixels Min Filter:";
            this.label_SettingsBadPixelsMinFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_SettingsBadPixelsMagFilter
            // 
            this.label_SettingsBadPixelsMagFilter.Location = new System.Drawing.Point(6, 137);
            this.label_SettingsBadPixelsMagFilter.Name = "label_SettingsBadPixelsMagFilter";
            this.label_SettingsBadPixelsMagFilter.Size = new System.Drawing.Size(112, 21);
            this.label_SettingsBadPixelsMagFilter.TabIndex = 6;
            this.label_SettingsBadPixelsMagFilter.Text = "BadPixels Mag Filter:";
            this.label_SettingsBadPixelsMagFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox_SettingsBadPixelsMagFilter
            // 
            this.comboBox_SettingsBadPixelsMagFilter.FormattingEnabled = true;
            this.comboBox_SettingsBadPixelsMagFilter.Location = new System.Drawing.Point(124, 138);
            this.comboBox_SettingsBadPixelsMagFilter.Name = "comboBox_SettingsBadPixelsMagFilter";
            this.comboBox_SettingsBadPixelsMagFilter.Size = new System.Drawing.Size(124, 21);
            this.comboBox_SettingsBadPixelsMagFilter.TabIndex = 7;
            this.comboBox_SettingsBadPixelsMagFilter.SelectedIndexChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // comboBox_SettingsViewMinFilter
            // 
            this.comboBox_SettingsViewMinFilter.FormattingEnabled = true;
            this.comboBox_SettingsViewMinFilter.Location = new System.Drawing.Point(124, 111);
            this.comboBox_SettingsViewMinFilter.Name = "comboBox_SettingsViewMinFilter";
            this.comboBox_SettingsViewMinFilter.Size = new System.Drawing.Size(124, 21);
            this.comboBox_SettingsViewMinFilter.TabIndex = 5;
            this.comboBox_SettingsViewMinFilter.SelectedIndexChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // label_SettingsViewMinFilter
            // 
            this.label_SettingsViewMinFilter.Location = new System.Drawing.Point(6, 111);
            this.label_SettingsViewMinFilter.Name = "label_SettingsViewMinFilter";
            this.label_SettingsViewMinFilter.Size = new System.Drawing.Size(112, 21);
            this.label_SettingsViewMinFilter.TabIndex = 4;
            this.label_SettingsViewMinFilter.Text = "View Min Filter:";
            this.label_SettingsViewMinFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonSaveLocalizationFile
            // 
            this.buttonSaveLocalizationFile.Location = new System.Drawing.Point(124, 238);
            this.buttonSaveLocalizationFile.Name = "buttonSaveLocalizationFile";
            this.buttonSaveLocalizationFile.Size = new System.Drawing.Size(141, 25);
            this.buttonSaveLocalizationFile.TabIndex = 16;
            this.buttonSaveLocalizationFile.Text = "Save Localization File";
            this.buttonSaveLocalizationFile.UseVisualStyleBackColor = true;
            this.buttonSaveLocalizationFile.Visible = false;
            this.buttonSaveLocalizationFile.Click += new System.EventHandler(this.buttonSaveLocalizationFile_Click);
            // 
            // label_SettingsViewMagFilter
            // 
            this.label_SettingsViewMagFilter.Location = new System.Drawing.Point(6, 83);
            this.label_SettingsViewMagFilter.Name = "label_SettingsViewMagFilter";
            this.label_SettingsViewMagFilter.Size = new System.Drawing.Size(112, 21);
            this.label_SettingsViewMagFilter.TabIndex = 2;
            this.label_SettingsViewMagFilter.Text = "View Mag Filter:";
            this.label_SettingsViewMagFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox_SettingsViewMagFilter
            // 
            this.comboBox_SettingsViewMagFilter.FormattingEnabled = true;
            this.comboBox_SettingsViewMagFilter.Location = new System.Drawing.Point(124, 84);
            this.comboBox_SettingsViewMagFilter.Name = "comboBox_SettingsViewMagFilter";
            this.comboBox_SettingsViewMagFilter.Size = new System.Drawing.Size(124, 21);
            this.comboBox_SettingsViewMagFilter.TabIndex = 3;
            this.comboBox_SettingsViewMagFilter.SelectedIndexChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // buttonSettingsSave
            // 
            this.buttonSettingsSave.Location = new System.Drawing.Point(6, 238);
            this.buttonSettingsSave.Name = "buttonSettingsSave";
            this.buttonSettingsSave.Size = new System.Drawing.Size(112, 25);
            this.buttonSettingsSave.TabIndex = 15;
            this.buttonSettingsSave.Text = "Save";
            this.buttonSettingsSave.UseVisualStyleBackColor = true;
            this.buttonSettingsSave.Click += new System.EventHandler(this.buttonSettingsSave_Click);
            // 
            // radioButtonSettingsPosG
            // 
            this.radioButtonSettingsPosG.Location = new System.Drawing.Point(6, 50);
            this.radioButtonSettingsPosG.Name = "radioButtonSettingsPosG";
            this.radioButtonSettingsPosG.Size = new System.Drawing.Size(259, 30);
            this.radioButtonSettingsPosG.TabIndex = 1;
            this.radioButtonSettingsPosG.Text = "Unity, Blender, Maya, Modo, Toolbag...\r\n[R+, G+, B+]";
            this.radioButtonSettingsPosG.UseVisualStyleBackColor = true;
            this.radioButtonSettingsPosG.CheckedChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // radioButtonSettingsNegG
            // 
            this.radioButtonSettingsNegG.Checked = true;
            this.radioButtonSettingsNegG.Location = new System.Drawing.Point(6, 6);
            this.radioButtonSettingsNegG.Name = "radioButtonSettingsNegG";
            this.radioButtonSettingsNegG.Size = new System.Drawing.Size(259, 37);
            this.radioButtonSettingsNegG.TabIndex = 0;
            this.radioButtonSettingsNegG.TabStop = true;
            this.radioButtonSettingsNegG.Text = "Unreal Engine, CryENGINE,\r\nSource Engine, 3ds Max... [R+, G-, B+]";
            this.radioButtonSettingsNegG.UseVisualStyleBackColor = true;
            this.radioButtonSettingsNegG.CheckedChanged += new System.EventHandler(this.SettingsTab_SettingsChanged);
            // 
            // groupBoxInfo
            // 
            this.groupBoxInfo.Controls.Add(this.checkBox_IgnoreB);
            this.groupBoxInfo.Controls.Add(this.label_RGB);
            this.groupBoxInfo.Controls.Add(this.label_X);
            this.groupBoxInfo.Controls.Add(this.label_Length);
            this.groupBoxInfo.Controls.Add(this.label_Y);
            this.groupBoxInfo.Controls.Add(this.label_Z);
            this.groupBoxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxInfo.Location = new System.Drawing.Point(131, 3);
            this.groupBoxInfo.Name = "groupBoxInfo";
            this.groupBoxInfo.Size = new System.Drawing.Size(151, 122);
            this.groupBoxInfo.TabIndex = 16;
            this.groupBoxInfo.TabStop = false;
            this.groupBoxInfo.Text = "Info";
            // 
            // checkBox_IgnoreB
            // 
            this.checkBox_IgnoreB.AutoSize = true;
            this.checkBox_IgnoreB.Location = new System.Drawing.Point(6, 96);
            this.checkBox_IgnoreB.Name = "checkBox_IgnoreB";
            this.checkBox_IgnoreB.Size = new System.Drawing.Size(108, 17);
            this.checkBox_IgnoreB.TabIndex = 7;
            this.checkBox_IgnoreB.Text = "Ignore B-Channel";
            this.checkBox_IgnoreB.UseVisualStyleBackColor = true;
            // 
            // label_RGB
            // 
            this.label_RGB.AutoSize = true;
            this.label_RGB.Location = new System.Drawing.Point(6, 19);
            this.label_RGB.Name = "label_RGB";
            this.label_RGB.Size = new System.Drawing.Size(33, 13);
            this.label_RGB.TabIndex = 5;
            this.label_RGB.Text = "RGB:";
            // 
            // label_X
            // 
            this.label_X.AutoSize = true;
            this.label_X.Location = new System.Drawing.Point(6, 32);
            this.label_X.Name = "label_X";
            this.label_X.Size = new System.Drawing.Size(17, 13);
            this.label_X.TabIndex = 6;
            this.label_X.Text = "X:";
            // 
            // label_Length
            // 
            this.label_Length.AutoSize = true;
            this.label_Length.Location = new System.Drawing.Point(6, 71);
            this.label_Length.Name = "label_Length";
            this.label_Length.Size = new System.Drawing.Size(43, 13);
            this.label_Length.TabIndex = 8;
            this.label_Length.Text = "Length:";
            // 
            // label_Y
            // 
            this.label_Y.AutoSize = true;
            this.label_Y.Location = new System.Drawing.Point(6, 45);
            this.label_Y.Name = "label_Y";
            this.label_Y.Size = new System.Drawing.Size(17, 13);
            this.label_Y.TabIndex = 9;
            this.label_Y.Text = "Y:";
            // 
            // label_Z
            // 
            this.label_Z.AutoSize = true;
            this.label_Z.Location = new System.Drawing.Point(6, 58);
            this.label_Z.Name = "label_Z";
            this.label_Z.Size = new System.Drawing.Size(17, 13);
            this.label_Z.TabIndex = 10;
            this.label_Z.Text = "Z:";
            // 
            // openFileDlg
            // 
            this.openFileDlg.Filter = resources.GetString("openFileDlg.Filter");
            this.openFileDlg.FilterIndex = 9;
            this.openFileDlg.SupportMultiDottedExtensions = true;
            // 
            // saveFileDlg
            // 
            this.saveFileDlg.Filter = resources.GetString("saveFileDlg.Filter");
            this.saveFileDlg.FilterIndex = 6;
            this.saveFileDlg.SupportMultiDottedExtensions = true;
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpen.Image")));
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonOpen.Text = " (Ctrl + O)";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripButtonSaveAs
            // 
            this.toolStripButtonSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSaveAs.Image")));
            this.toolStripButtonSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveAs.Name = "toolStripButtonSaveAs";
            this.toolStripButtonSaveAs.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSaveAs.Text = " (Ctrl + S)";
            this.toolStripButtonSaveAs.Click += new System.EventHandler(this.toolStripButtonSaveAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonUndo
            // 
            this.toolStripButtonUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUndo.Enabled = false;
            this.toolStripButtonUndo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUndo.Image")));
            this.toolStripButtonUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUndo.Name = "toolStripButtonUndo";
            this.toolStripButtonUndo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUndo.Text = " (Ctrl + Z)";
            this.toolStripButtonUndo.Click += new System.EventHandler(this.toolStripButtonUndo_Click);
            // 
            // toolStripButtonRedo
            // 
            this.toolStripButtonRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRedo.Enabled = false;
            this.toolStripButtonRedo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRedo.Image")));
            this.toolStripButtonRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRedo.Name = "toolStripButtonRedo";
            this.toolStripButtonRedo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRedo.Text = " (Ctrl + Y)";
            this.toolStripButtonRedo.Click += new System.EventHandler(this.toolStripButtonRedo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonCopy
            // 
            this.toolStripButtonCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCopy.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCopy.Image")));
            this.toolStripButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCopy.Name = "toolStripButtonCopy";
            this.toolStripButtonCopy.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCopy.Text = " (Ctrl + C)";
            this.toolStripButtonCopy.Click += new System.EventHandler(this.toolStripButtonCopy_Click);
            // 
            // toolStripButtonPaste
            // 
            this.toolStripButtonPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPaste.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPaste.Image")));
            this.toolStripButtonPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaste.Name = "toolStripButtonPaste";
            this.toolStripButtonPaste.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonPaste.Text = " (Ctrl + V)";
            this.toolStripButtonPaste.Click += new System.EventHandler(this.toolStripButtonPaste_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonInvR
            // 
            this.toolStripButtonInvR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInvR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvR.Image")));
            this.toolStripButtonInvR.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvR.Name = "toolStripButtonInvR";
            this.toolStripButtonInvR.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInvR.Text = " (Ctrl + R)";
            this.toolStripButtonInvR.Click += new System.EventHandler(this.toolStripButtonInvR_Click);
            // 
            // toolStripButtonInvG
            // 
            this.toolStripButtonInvG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInvG.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvG.Image")));
            this.toolStripButtonInvG.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvG.Name = "toolStripButtonInvG";
            this.toolStripButtonInvG.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInvG.Text = " (Ctrl + G)";
            this.toolStripButtonInvG.Click += new System.EventHandler(this.toolStripButtonInvG_Click);
            // 
            // toolStripButtonInvB
            // 
            this.toolStripButtonInvB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInvB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvB.Image")));
            this.toolStripButtonInvB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvB.Name = "toolStripButtonInvB";
            this.toolStripButtonInvB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInvB.Text = " (Ctrl + B)";
            this.toolStripButtonInvB.Click += new System.EventHandler(this.toolStripButtonInvB_Click);
            // 
            // toolStripButtonInvA
            // 
            this.toolStripButtonInvA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInvA.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvA.Image")));
            this.toolStripButtonInvA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvA.Name = "toolStripButtonInvA";
            this.toolStripButtonInvA.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInvA.Text = " (Ctrl + A)";
            this.toolStripButtonInvA.Click += new System.EventHandler(this.toolStripButtonInvA_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonNormalize
            // 
            this.toolStripButtonNormalize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNormalize.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNormalize.Image")));
            this.toolStripButtonNormalize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNormalize.Name = "toolStripButtonNormalize";
            this.toolStripButtonNormalize.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonNormalize.Text = " (Ctrl + N)";
            this.toolStripButtonNormalize.Click += new System.EventHandler(this.toolStripButtonNormalize_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonRestoreB
            // 
            this.toolStripButtonRestoreB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRestoreB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRestoreB.Image")));
            this.toolStripButtonRestoreB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRestoreB.Name = "toolStripButtonRestoreB";
            this.toolStripButtonRestoreB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRestoreB.Text = " (Shift + B)";
            this.toolStripButtonRestoreB.Click += new System.EventHandler(this.toolStripButtonRestoreB_Click);
            // 
            // toolStripButtonClearB
            // 
            this.toolStripButtonClearB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClearB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClearB.Image")));
            this.toolStripButtonClearB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClearB.Name = "toolStripButtonClearB";
            this.toolStripButtonClearB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonClearB.Text = " (Alt + B)";
            this.toolStripButtonClearB.Click += new System.EventHandler(this.toolStripButtonClearB_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonFlipRG
            // 
            this.toolStripButtonFlipRG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipRG.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipRG.Image")));
            this.toolStripButtonFlipRG.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipRG.Name = "toolStripButtonFlipRG";
            this.toolStripButtonFlipRG.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipRG.Click += new System.EventHandler(this.toolStripButtonFlipRG_Click);
            // 
            // toolStripButtonFlipGB
            // 
            this.toolStripButtonFlipGB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipGB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipGB.Image")));
            this.toolStripButtonFlipGB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipGB.Name = "toolStripButtonFlipGB";
            this.toolStripButtonFlipGB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipGB.Click += new System.EventHandler(this.toolStripButtonFlipGB_Click);
            // 
            // toolStripButtonFlipBA
            // 
            this.toolStripButtonFlipBA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipBA.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipBA.Image")));
            this.toolStripButtonFlipBA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipBA.Name = "toolStripButtonFlipBA";
            this.toolStripButtonFlipBA.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipBA.Click += new System.EventHandler(this.toolStripButtonFlipBA_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonRotate270
            // 
            this.toolStripButtonRotate270.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotate270.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotate270.Image")));
            this.toolStripButtonRotate270.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRotate270.Name = "toolStripButtonRotate270";
            this.toolStripButtonRotate270.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRotate270.Click += new System.EventHandler(this.toolStripButtonRotate270_Click);
            // 
            // toolStripButtonRotate90
            // 
            this.toolStripButtonRotate90.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotate90.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotate90.Image")));
            this.toolStripButtonRotate90.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRotate90.Name = "toolStripButtonRotate90";
            this.toolStripButtonRotate90.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRotate90.Click += new System.EventHandler(this.toolStripButtonRotate90_Click);
            // 
            // toolStripButtonRotate180
            // 
            this.toolStripButtonRotate180.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotate180.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotate180.Image")));
            this.toolStripButtonRotate180.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRotate180.Name = "toolStripButtonRotate180";
            this.toolStripButtonRotate180.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRotate180.Click += new System.EventHandler(this.toolStripButtonRotate180_Click);
            // 
            // toolStripButtonFlipH
            // 
            this.toolStripButtonFlipH.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipH.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipH.Image")));
            this.toolStripButtonFlipH.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipH.Name = "toolStripButtonFlipH";
            this.toolStripButtonFlipH.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipH.Click += new System.EventHandler(this.toolStripButtonFlipH_Click);
            // 
            // toolStripButtonFlipV
            // 
            this.toolStripButtonFlipV.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipV.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipV.Image")));
            this.toolStripButtonFlipV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipV.Name = "toolStripButtonFlipV";
            this.toolStripButtonFlipV.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipV.Click += new System.EventHandler(this.toolStripButtonFlipV_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonHelp.Text = " (F1)";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // toolStripIconMenu
            // 
            this.toolStripIconMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripIconMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOpen,
            this.toolStripButtonSaveAs,
            this.toolStripSeparator1,
            this.toolStripButtonUndo,
            this.toolStripButtonRedo,
            this.toolStripSeparator2,
            this.toolStripButtonCopy,
            this.toolStripButtonPaste,
            this.toolStripSeparator3,
            this.toolStripButtonInvR,
            this.toolStripButtonInvG,
            this.toolStripButtonInvB,
            this.toolStripButtonInvA,
            this.toolStripSeparator4,
            this.toolStripButtonNormalize,
            this.toolStripSeparator5,
            this.toolStripButtonRestoreB,
            this.toolStripButtonClearB,
            this.toolStripSeparator6,
            this.toolStripButtonFlipRG,
            this.toolStripButtonFlipGB,
            this.toolStripButtonFlipBA,
            this.toolStripSeparator7,
            this.toolStripButtonRotate270,
            this.toolStripButtonRotate90,
            this.toolStripButtonRotate180,
            this.toolStripButtonFlipH,
            this.toolStripButtonFlipV,
            this.toolStripSeparator8,
            this.toolStripButtonHelp});
            this.toolStripIconMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripIconMenu.Name = "toolStripIconMenu";
            this.toolStripIconMenu.Size = new System.Drawing.Size(784, 25);
            this.toolStripIconMenu.TabIndex = 13;
            this.toolStripIconMenu.Text = "toolStrip1";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.toolStripIconMenu, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.splitContainer1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel2.TabIndex = 18;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pictureBoxZ);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(778, 530);
            this.splitContainer1.SplitterDistance = 489;
            this.splitContainer1.TabIndex = 14;
            // 
            // pictureBoxZ
            // 
            this.pictureBoxZ.AllowDrop = true;
            this.pictureBoxZ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.pictureBoxZ.BadPixels_ColorBad = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBoxZ.BadPixels_ColorGood = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBoxZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxZ.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxZ.MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear;
            this.pictureBoxZ.Name = "pictureBoxZ";
            this.pictureBoxZ.Size = new System.Drawing.Size(489, 530);
            this.pictureBoxZ.TabIndex = 17;
            this.pictureBoxZ.VSync = true;
            this.pictureBoxZ.ZoomCenterPoint = ((System.Drawing.PointF)(resources.GetObject("pictureBoxZ.ZoomCenterPoint")));
            this.pictureBoxZ.ZoomList = ((System.Collections.Generic.List<float>)(resources.GetObject("pictureBoxZ.ZoomList")));
            this.pictureBoxZ.ZoomListIndex = -1;
            this.pictureBoxZ.DragDrop += new System.Windows.Forms.DragEventHandler(this.pictureBoxZ_DragDrop);
            this.pictureBoxZ.DragEnter += new System.Windows.Forms.DragEventHandler(this.pictureBoxZ_DragEnter);
            this.pictureBoxZ.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ_MouseClick_Move);
            this.pictureBoxZ.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ_MouseClick_Move);
            this.pictureBoxZ.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ_MouseClick_Move);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel2);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(430, 430);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NormalMap Debugger";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage_ToNMap.ResumeLayout(false);
            this.tabPage_ToNMap.PerformLayout();
            this.tabPage_ToHeight.ResumeLayout(false);
            this.tabPage_ToHeight.PerformLayout();
            this.tabPage_Settings.ResumeLayout(false);
            this.tabPage_Settings.PerformLayout();
            this.groupBoxInfo.ResumeLayout(false);
            this.groupBoxInfo.PerformLayout();
            this.toolStripIconMenu.ResumeLayout(false);
            this.toolStripIconMenu.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonBadPixels;
        private System.Windows.Forms.RadioButton radioButtonRGB;
        private System.Windows.Forms.RadioButton radioButtonB;
        private System.Windows.Forms.RadioButton radioButtonG;
        private System.Windows.Forms.RadioButton radioButtonR;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.SaveFileDialog saveFileDlg;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_ToNMap;
        private System.Windows.Forms.CheckBox checkBox_GenNormalize;
        private System.Windows.Forms.CheckBox checkBox_GenInvY;
        private System.Windows.Forms.CheckBox checkBox_GenInvX;
        private System.Windows.Forms.TabPage tabPage_Settings;
        public System.Windows.Forms.RadioButton radioButtonSettingsPosG;
        public System.Windows.Forms.RadioButton radioButtonSettingsNegG;
        private System.Windows.Forms.GroupBox groupBoxInfo;
        private System.Windows.Forms.CheckBox checkBox_IgnoreB;
        private System.Windows.Forms.Label label_RGB;
        private System.Windows.Forms.Label label_X;
        private System.Windows.Forms.Label label_Length;
        private System.Windows.Forms.Label label_Y;
        private System.Windows.Forms.Label label_Z;
        private System.Windows.Forms.Button buttonSettingsSave;
        private System.Windows.Forms.Label label_SettingsViewMagFilter;
        private System.Windows.Forms.ComboBox comboBox_SettingsViewMagFilter;
        private System.Windows.Forms.Button button_NMap_Gen;
        private System.Windows.Forms.Button button_NMap_Apply;
        private System.Windows.Forms.Button buttonSaveLocalizationFile;
        private ControlsList.ControlsList controlsListKernels;
        private PictureBoxZ.PictureBoxZ pictureBoxZ;
        private System.Windows.Forms.RadioButton radioButtonA;
        private System.Windows.Forms.ComboBox comboBox_SettingsViewMinFilter;
        private System.Windows.Forms.Label label_SettingsViewMinFilter;
        private System.Windows.Forms.ComboBox comboBox_SettingsBadPixelsMinFilter;
        private System.Windows.Forms.Label label_SettingsBadPixelsMinFilter;
        private System.Windows.Forms.Label label_SettingsBadPixelsMagFilter;
        private System.Windows.Forms.ComboBox comboBox_SettingsBadPixelsMagFilter;
        private System.Windows.Forms.CheckBox checkBox_MipMaps;
        private System.Windows.Forms.CheckBox checkBox_FXAA;
        private System.Windows.Forms.CheckBox checkBox_AnisotropicFiltering;
        private System.Windows.Forms.CheckBox checkBox_Draw2D_UseAlpha;
        private System.Windows.Forms.CheckBox checkBox_VSync;
        private System.Windows.Forms.Label label_GenRGB2Gray;
        private System.Windows.Forms.ComboBox comboBox_GenRGB2Gray;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveAs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonUndo;
        private System.Windows.Forms.ToolStripButton toolStripButtonRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonCopy;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvR;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvG;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvB;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButtonNormalize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButtonRestoreB;
        private System.Windows.Forms.ToolStripButton toolStripButtonClearB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipRG;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipGB;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipBA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripButtonRotate270;
        private System.Windows.Forms.ToolStripButton toolStripButtonRotate90;
        private System.Windows.Forms.ToolStripButton toolStripButtonRotate180;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipH;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipV;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ToolStrip toolStripIconMenu;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox comboBox_Mashes;
        private System.Windows.Forms.Label label_3DModel;
        private System.Windows.Forms.Button button_NMap_Cancel;
        private System.Windows.Forms.CheckBox checkBox_2D_3D;
        private System.Windows.Forms.CheckBox checkBox_UseTexAsNMap;
        private System.Windows.Forms.CheckBox checkBox_UseLightIn3D;
        private System.Windows.Forms.CheckBox checkBox_ShowTexIn3D;
        private System.Windows.Forms.TabPage tabPage_ToHeight;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_HMap_Repeat;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_HMap_Step;
        private System.Windows.Forms.Button button_HMap_Cancel;
        private System.Windows.Forms.Button button_HMap_Apply;
        private System.Windows.Forms.Button button_HMap_Gen;
        private System.Windows.Forms.CheckBox checkBox_HMap_Invert;
        private System.Windows.Forms.CheckBox checkBox_HMap_CPU_Precompute;
    }
}

