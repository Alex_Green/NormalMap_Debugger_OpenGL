﻿using System;

namespace NormalMap_Debugger
{
    internal class Parallel
    {
        public delegate void DelegateFor(int i);
        public delegate void DelegateProcess();

        /// <summary>
        /// Parallel for loop. Invokes given action, passing arguments fromInclusive - toExclusive on multiple threads. Returns when loop finished.
        /// </summary>
        /// <param name="From">Inclusive value.</param>
        /// <param name="To">Exclusive value.</param>
        /// <param name="delFor"></param>
        public static void For(int From, int To, DelegateFor delFor)
        {
            int Step = 4;
            int Count = From - Step;
            int ThreadCount = Environment.ProcessorCount;

            //Now let's take the next chunk 
            DelegateProcess process = delegate ()
            {
                while (true)
                {
                    int iter = 0;
                    lock (typeof(Parallel))
                    {
                        Count += Step;
                        iter = Count;
                    }
                    for (int i = iter; i < iter + Step; i++)
                    {
                        if (i >= To)
                            return;
                        delFor(i);
                    }
                }
            };

            //IAsyncResult array to launch Thread(s)
            IAsyncResult[] asyncResults = new IAsyncResult[ThreadCount];
            for (int i = 0; i < ThreadCount; i++)
                asyncResults[i] = process.BeginInvoke(null, null);

            //EndInvoke to wait for all threads to be completed
            for (int i = 0; i < ThreadCount; i++)
                process.EndInvoke(asyncResults[i]);
        }
    }
}
