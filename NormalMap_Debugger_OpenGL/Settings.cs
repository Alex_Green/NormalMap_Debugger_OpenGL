﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using PictureBoxZ;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace NormalMap_Debugger
{
    [XmlRoot(ElementName = "Config")]
    public class SettingsXML
    {
        public const string XmlFile = "Settings.xml";

        public static bool Save(SettingsXML SettingsXml, string FullPath)
        {
            try
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(FullPath, new XmlWriterSettings() { Indent = true, IndentChars = "\t", OmitXmlDeclaration = true }))
                {
                    #region Remove 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ...'
                    XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);
                    #endregion

                    XmlSerializer serializer = new XmlSerializer(typeof(SettingsXML));
                    serializer.Serialize(xmlWriter, SettingsXml, namespaces);
                    xmlWriter.Close();
                    return File.Exists(XmlFile);
                }
            }
            catch
            {
                return false;
            }
        }

        public static SettingsXML Load(string XmlFile)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsXML));
            if (File.Exists(XmlFile)) //If file exist
            {
                try
                {
                    using (FileStream SettingsFile = new FileStream(XmlFile, FileMode.Open))
                    {
                        SettingsXML S = (SettingsXML)serializer.Deserialize(SettingsFile);
                        SettingsFile.Close();
                        return S;
                    }
                }
                catch
                {
                    return null;
                }
            }
            else //File not exist
            {
                if (Save(new SettingsXML(), XmlFile)) //Save new SettingsFile
                {
                    using (FileStream SettingsFile = new FileStream(XmlFile, FileMode.Open))
                    {
                        SettingsXML S = (SettingsXML)serializer.Deserialize(SettingsFile);
                        SettingsFile.Close();
                        return S;
                    }
                }
                else
                    return null;
            }
        }

        #region Settings

        [XmlIgnore]
        public bool IsLoaded = false;

        public FormWindowState WindowState = FormWindowState.Normal;
        //Need a lot of RAM: UndoLevels * Width * Height * PixelDepth. [10 * 4K textures => 640Mb!]
        public uint UndoLevels = 10;
        public bool NegativeGreenChannel = true;
        public bool ShowGenerationTime = false;
        public float ClearBlueChannelValue = 1f;
        public string ToStringParameter = "0.00000";
        public RgbToGrayModes RgbToGrayMode = RgbToGrayModes.Desaturate;

        public _BadPixels BadPixels = new _BadPixels();
        public class _BadPixels
        {
            public TextureMagFilter MagFilter = TextureMagFilter.Nearest;
            public TextureMinFilter MinFilter = TextureMinFilter.Nearest;
            public ColorRGBA ColorBad = new ColorRGBA(1f, 0f, 0f);
            public ColorRGBA ColorGood = new ColorRGBA(0f, 0f, 0f);
            public float Threshold = 0.02f;
        }

        public _Graphics Graphics = new _Graphics();
        public class _Graphics
        {
            public bool AnisotropicFiltering = true;
            public bool FXAA = true;
            public bool MipMaps = false;
            public bool VSync = true;
            public bool Draw2D_UseAlpha = true;
            public TextureMagFilter View_MagFilter = TextureMagFilter.Nearest;
            public TextureMinFilter View_MinFilter = TextureMinFilter.Linear;
            public ColorRGBA GLClearColor = new ColorRGBA(0.5f, 0.5f, 0.5f, 1f);

            public _Light Light = new _Light();
            public class _Light
            {
                public ColorRGBA Diffuse = new ColorRGBA(0.8f);
                public ColorRGBA Specular = new ColorRGBA(1.0f);
            }

            public _NormalVector NormalVector = new _NormalVector();
            public class _NormalVector
            {
                public CompositingQuality CompositingQuality = CompositingQuality.HighQuality;
                public PixelOffsetMode PixelOffsetMode = PixelOffsetMode.HighQuality;
                public SmoothingMode SmoothingMode = SmoothingMode.HighQuality;
                public float LineWidth = 2.0f;
                public ColorRGBA LineColor1 = new ColorRGBA(1f, 0f, 0f);
                public ColorRGBA LineColor2 = new ColorRGBA(1f, 1f, 0f);
                public ColorRGBA BackgroundColor = new ColorRGBA(0f, 0f, 0f);
            }
        }
        #endregion
    }
}
