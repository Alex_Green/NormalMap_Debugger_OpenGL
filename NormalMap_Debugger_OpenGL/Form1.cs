﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using PictureBoxZ;
using TGASharpLib;
using System.Drawing.PSD;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace NormalMap_Debugger
{
    public partial class Form1 : Form
    {
        string StartupPath;
        SettingsXML Settings = new SettingsXML();
        LocalizationXML Localization = new LocalizationXML();
        string Form1TextDefault = "NormalMap Debugger v{0}.{1}";
        Color NMapColor = Color.FromArgb(128, 128, 255);
        Stopwatch PerfomanceTimer = new Stopwatch();
        WayBack<Image> Wayback;

        public Form1()
        {
            InitializeComponent();
            Assembly Exe = Assembly.GetExecutingAssembly();
            StartupPath = Path.GetDirectoryName(Exe.Location);

            Form1TextDefault = String.Format(Form1TextDefault, Exe.GetName().Version.Major, Exe.GetName().Version.Minor);
            base.Text = Form1TextDefault;

            comboBox_GenRGB2Gray.DataSource = (RgbToGrayModes[])Enum.GetValues(typeof(RgbToGrayModes));
            comboBox_SettingsBadPixelsMagFilter.DataSource = Array.FindAll((TextureMagFilter[])Enum.GetValues(typeof(TextureMagFilter)),
                (TextureMagFilter M) => { return !(M.ToString().EndsWith("Sgis") || M.ToString().EndsWith("Sgix")); });
            comboBox_SettingsViewMagFilter.DataSource = Array.FindAll((TextureMagFilter[])Enum.GetValues(typeof(TextureMagFilter)),
                (TextureMagFilter M) => { return !(M.ToString().EndsWith("Sgis") || M.ToString().EndsWith("Sgix")); });
            comboBox_SettingsBadPixelsMinFilter.DataSource = Array.FindAll((TextureMinFilter[])Enum.GetValues(typeof(TextureMinFilter)),
                (TextureMinFilter M) => { return !(M.ToString().EndsWith("Sgis") || M.ToString().EndsWith("Sgix")); });
            comboBox_SettingsViewMinFilter.DataSource = Array.FindAll((TextureMinFilter[])Enum.GetValues(typeof(TextureMinFilter)),
                (TextureMinFilter M) => { return !(M.ToString().EndsWith("Sgis") || M.ToString().EndsWith("Sgix")); });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBoxZ.Image = Properties.Resources.TestNormalMap;
            Text = Form1TextDefault + " - " + GL.GetString(StringName.Renderer);

            #region Load and Set Settings
            Settings = SettingsXML.Load(Path.Combine(StartupPath, SettingsXML.XmlFile));
            if (Settings == null)
            {
                Settings = new SettingsXML();
                MessageBox.Show("Error loading settings!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            WindowState = Settings.WindowState;

            Engine.GLClearColor = Settings.Graphics.GLClearColor;
            Light.Diffuse = Settings.Graphics.Light.Diffuse;
            Light.Specular = Settings.Graphics.Light.Specular;

            radioButtonSettingsNegG.Checked = Settings.NegativeGreenChannel;
            pictureBoxZ.InvertY = radioButtonSettingsNegG.Checked;
            radioButtonSettingsPosG.Checked = !radioButtonSettingsNegG.Checked;

            comboBox_SettingsViewMagFilter.SelectedItem = Settings.Graphics.View_MagFilter;
            comboBox_SettingsViewMinFilter.SelectedItem = Settings.Graphics.View_MinFilter;
            comboBox_SettingsBadPixelsMagFilter.SelectedItem = Settings.BadPixels.MagFilter;
            comboBox_SettingsBadPixelsMinFilter.SelectedItem = Settings.BadPixels.MinFilter;
            pictureBoxZ.BadPixels_ColorBad = Settings.BadPixels.ColorBad;
            pictureBoxZ.BadPixels_ColorGood = Settings.BadPixels.ColorGood;
            pictureBoxZ.BadPixels_Threshold = Settings.BadPixels.Threshold;

            checkBox_AnisotropicFiltering.Checked = pictureBoxZ.AnisotropicFiltering = Settings.Graphics.AnisotropicFiltering;
            checkBox_FXAA.Checked = pictureBoxZ.FXAA = Settings.Graphics.FXAA;
            checkBox_MipMaps.Checked = pictureBoxZ.MipMaps = Settings.Graphics.MipMaps;
            checkBox_VSync.Checked = pictureBoxZ.VSync = Settings.Graphics.VSync;
            checkBox_Draw2D_UseAlpha.Checked = pictureBoxZ.Draw2D_UseAlpha = Settings.Graphics.Draw2D_UseAlpha;

            Wayback = new WayBack<Image>(Settings.UndoLevels, toolStripButtonUndo, toolStripButtonRedo);

            Settings.IsLoaded = true;
            SettingsTab_SettingsChanged(sender, e);
            #endregion

            #region Localization
            Localization = LocalizationXML.Load(Path.Combine(StartupPath, LocalizationXML.XmlFile));
            if (Localization == null)
            {
                buttonSaveLocalizationFile.Visible = true;
                Localization = new LocalizationXML();
            }

            toolStripButtonOpen.Text = Localization.MenuButtons.Open + toolStripButtonOpen.Text;
            toolStripButtonSaveAs.Text = Localization.MenuButtons.SaveAs + toolStripButtonSaveAs.Text;
            toolStripButtonUndo.Text = Localization.MenuButtons.Undo + toolStripButtonUndo.Text;
            toolStripButtonRedo.Text = Localization.MenuButtons.Redo + toolStripButtonRedo.Text;
            toolStripButtonCopy.Text = Localization.MenuButtons.Copy + toolStripButtonCopy.Text;
            toolStripButtonPaste.Text = Localization.MenuButtons.Paste + toolStripButtonPaste.Text;
            toolStripButtonInvR.Text = Localization.MenuButtons.InvertR + toolStripButtonInvR.Text;
            toolStripButtonInvG.Text = Localization.MenuButtons.InvertG + toolStripButtonInvG.Text;
            toolStripButtonInvB.Text = Localization.MenuButtons.InvertB + toolStripButtonInvB.Text;
            toolStripButtonNormalize.Text = Localization.MenuButtons.Normalize + toolStripButtonNormalize.Text;
            toolStripButtonRestoreB.Text = Localization.MenuButtons.RestoreB + toolStripButtonRestoreB.Text;
            toolStripButtonClearB.Text = Localization.MenuButtons.ClearB + toolStripButtonClearB.Text;
            toolStripButtonFlipRG.Text = Localization.MenuButtons.FlipRG + toolStripButtonFlipRG.Text;
            toolStripButtonFlipGB.Text = Localization.MenuButtons.FlipGB + toolStripButtonFlipGB.Text;
            toolStripButtonFlipBA.Text = Localization.MenuButtons.FlipBA + toolStripButtonFlipBA.Text;
            toolStripButtonRotate270.Text = Localization.MenuButtons.Rotate270 + toolStripButtonRotate270.Text;
            toolStripButtonRotate90.Text = Localization.MenuButtons.Rotate90 + toolStripButtonRotate90.Text;
            toolStripButtonRotate180.Text = Localization.MenuButtons.Rotate180 + toolStripButtonRotate180.Text;
            toolStripButtonFlipH.Text = Localization.MenuButtons.FlipHorizontally + toolStripButtonFlipH.Text;
            toolStripButtonFlipV.Text = Localization.MenuButtons.FlipVertically + toolStripButtonFlipV.Text;
            toolStripButtonHelp.Text = Localization.MenuButtons.Help + toolStripButtonHelp.Text;

            checkBox_IgnoreB.Text = Localization.Info.CheckBox_IgnoreBlueChannel;
            groupBoxInfo.Text = Localization.Info.Label_Info;
            radioButtonBadPixels.Text = Localization.Info.RadioButton_BadPixels;
            label_3DModel.Text = Localization.Info.Label_3DModel;

            tabPage_ToNMap.Text = Localization.NormalMapGeneration.TabPage_GenerateNormalMap;
            checkBox_GenInvX.Text = Localization.NormalMapGeneration.CheckBox_GenInvX;
            checkBox_GenInvY.Text = Localization.NormalMapGeneration.CheckBox_GenInvY;
            checkBox_GenNormalize.Text = Localization.NormalMapGeneration.CheckBox_GenNormalize;
            button_NMap_Gen.Text = Localization.NormalMapGeneration.Button_Generate;

            tabPage_Settings.Text = Localization.Settings.TabPage_Settings;
            label_SettingsViewMagFilter.Text = Localization.Settings.Label_SettingsViewMagFilter;
            label_SettingsViewMinFilter.Text = Localization.Settings.Label_SettingsViewMinFilter;
            label_SettingsBadPixelsMagFilter.Text = Localization.Settings.Label_SettingsBadPixelsMagFilter;
            label_SettingsBadPixelsMinFilter.Text = Localization.Settings.Label_SettingsBadPixelsMinFilter;
            checkBox_AnisotropicFiltering.Text = Localization.Settings.checkBox_AnisotropicFiltering;
            checkBox_FXAA.Text = Localization.Settings.checkBox_UseFXAA;
            checkBox_MipMaps.Text = Localization.Settings.checkBox_UseMipMaps;
            checkBox_VSync.Text = Localization.Settings.checkBox_VSync;
            checkBox_Draw2D_UseAlpha.Text = Localization.Settings.checkBox_Draw2D_UseAlpha;
            buttonSettingsSave.Text = Localization.Settings.Button_SettingsSave;
            #endregion

            #region SliderTrackbar List
            controlsListKernels.SetButtonAddFunc(() =>
            {
                SliderTrackbar SliderTrackbar1 = new SliderTrackbar();
                SliderTrackbar SliderTrackbar2 = new SliderTrackbar();
                SliderTrackbar1.Name = SliderTrackbar1.Text = nameof(SliderTrackbar1);
                SliderTrackbar2.Name = SliderTrackbar2.Text = nameof(SliderTrackbar2);
                SliderTrackbar1.Dock = SliderTrackbar2.Dock = DockStyle.Fill;
                SliderTrackbar1.Margin = SliderTrackbar2.Margin = Padding.Empty;

                SliderTrackbar1.Minimum = SliderTrackbar1.SliderMinimum = SliderTrackbar1.ValueDefault = 3;
                SliderTrackbar1.Maximum = 499;
                SliderTrackbar1.SliderMaximum = 199;
                SliderTrackbar1.Step = 2m;
                SliderTrackbar1.KeyboardInputStep = true;
                SliderTrackbar1.TextStringFormat = Localization.NormalMapGeneration.Slider_Kernel + ": {0:0}";
                SliderTrackbar1.TextInputStringFormat = "0";

                SliderTrackbar2.Minimum = SliderTrackbar2.SliderMinimum = 0m;
                SliderTrackbar2.Maximum = 100m;
                SliderTrackbar2.SliderMaximum = 5m;
                SliderTrackbar2.ValueDefault = 1m;
                SliderTrackbar2.Step = 0.1m;
                SliderTrackbar2.KeyboardInputStep = false;
                SliderTrackbar2.TextStringFormat = Localization.NormalMapGeneration.Slider_Scale + ": {0:0.##}";
                SliderTrackbar2.TextInputStringFormat = "0.##";

                TableLayoutPanel TLP = new TableLayoutPanel();
                TLP.Height = Math.Max(SliderTrackbar1.Height, SliderTrackbar2.Height);
                TLP.ColumnCount = 2;
                TLP.Margin = Padding.Empty;
                TLP.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
                TLP.Controls.Add(SliderTrackbar1, 0, 0);
                TLP.Controls.Add(SliderTrackbar2, 1, 0);
                return TLP;
            });

            int[] Kernels = new int[] { 1, 7, 15, 35 };
            double[] Scales = new double[] { 1.0, 1.1, 2.0, 2.0 };
            for (int i = 0; i < Math.Min(Kernels.Length, Scales.Length); i++)
            {
                controlsListKernels.AddControl();
                SliderTrackbar ST1 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[0];
                SliderTrackbar ST2 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[1];
                ST1.Value = ST1.ValueDefault = Kernels[i];
                ST2.Value = ST2.ValueDefault = (decimal)Scales[i];
            }
            #endregion

            OpenFirstImage(Program.Args);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Close();
                    break;
                case Keys.O:
                    if (e.Control && toolStripButtonOpen.Enabled)
                        toolStripButtonOpen_Click(sender, e);
                    break;
                case Keys.S:
                    if (e.Control && toolStripButtonSaveAs.Enabled)
                        toolStripButtonSaveAs_Click(sender, e);
                    break;
                case Keys.Z:
                    if (e.Control && toolStripButtonUndo.Enabled)
                        toolStripButtonUndo_Click(sender, e);
                    break;
                case Keys.Y:
                    if (e.Control && toolStripButtonRedo.Enabled)
                        toolStripButtonRedo_Click(sender, e);
                    break;
                case Keys.C:
                    if (e.Control && toolStripButtonCopy.Enabled)
                        toolStripButtonCopy_Click(sender, e);
                    break;
                case Keys.V:
                    if (e.Control && toolStripButtonPaste.Enabled)
                        toolStripButtonPaste_Click(sender, e);
                    break;
                case Keys.R:
                    if (e.Control && toolStripButtonInvR.Enabled)
                        toolStripButtonInvR_Click(sender, e);
                    break;
                case Keys.G:
                    if (e.Control && toolStripButtonInvG.Enabled)
                        toolStripButtonInvG_Click(sender, e);
                    break;
                case Keys.B:
                    if (e.Control && toolStripButtonInvB.Enabled)
                        toolStripButtonInvB_Click(sender, e);
                    if (e.Shift && toolStripButtonRestoreB.Enabled)
                        toolStripButtonRestoreB_Click(sender, e);
                    if (e.Alt && toolStripButtonClearB.Enabled)
                        toolStripButtonClearB_Click(sender, e);
                    break;
                case Keys.A:
                    if (e.Control && toolStripButtonInvA.Enabled)
                        toolStripButtonInvA_Click(sender, e);
                    break;
                case Keys.N:
                    if (e.Control && toolStripButtonNormalize.Enabled)
                        toolStripButtonNormalize_Click(sender, e);
                    break;
                case Keys.F1:
                    if (toolStripButtonHelp.Enabled)
                        toolStripButtonHelp_Click(sender, e);
                    break;
            }
        }

        void OpenFirstImage(string[] Args)
        {
            foreach (string Arg in Args)
                if (OpenImage(Arg))
                    break;
        }

        bool OpenImage(string ImageFile)
        {
            if (!File.Exists(ImageFile))
                return false;

            try
            {
                Text = Path.GetFileName(ImageFile);
                string FileExt = Path.GetExtension(ImageFile).TrimStart(new char[] { '.' }).ToLowerInvariant();
                switch (FileExt)
                {
                    default:
                    case null:
                    case "":
                        pictureBoxZ.Image = new Bitmap(ImageFile);
                        return true;

                    case "tga":
                    case "targa":
                        pictureBoxZ.Image = new TGA(ImageFile).ToBitmap();
                        return true;

                    case "psd":
                        PsdFile psdFile = new PsdFile();
                        psdFile.Load(ImageFile);
                        pictureBoxZ.Image = ImageDecoder.DecodeImage(psdFile);
                        psdFile = null;
                        return true;

                    case "dds":
                        pictureBoxZ.Image = DDSReaderSharp.ToBitmap(File.ReadAllBytes(ImageFile));
                        return true;
                }
            }
            catch
            {
                return false;
            }
        }

        #region pictureBoxZ and pictureBox
        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics Gr = e.Graphics;
            Gr.CompositingQuality = Settings.Graphics.NormalVector.CompositingQuality;
            Gr.PixelOffsetMode = Settings.Graphics.NormalVector.PixelOffsetMode;
            Gr.SmoothingMode = Settings.Graphics.NormalVector.SmoothingMode;

            RectangleF Re = pictureBox.ClientRectangle;
            Gr.FillRectangle(new SolidBrush(NMapColor), Re);
            Gr.FillEllipse(new SolidBrush(Settings.Graphics.NormalVector.BackgroundColor), Re);

            Vector3 V;
            if (radioButtonSettingsNegG.Checked)
                V = new Vector3(NMapColor.R, NMapColor.G, NMapColor.B);
            else
                V = new Vector3(NMapColor.R, 255f - NMapColor.G, NMapColor.B);

            V = V / 127.5f - Vector3.One; // [-1; 1]

            if (checkBox_IgnoreB.Checked)
            {
                double ZZ = 1.0f - V.X * V.X - V.Y * V.Y;
                if (ZZ >= 0)
                {
                    V.Z = (float)Math.Sqrt(ZZ);
                    label_Z.Text = "Z: " + V.Z.ToString(Settings.ToStringParameter);
                }
                else
                    label_Z.Text = "Z: Z < 0 !!!";

                label_Length.Text = Localization.Info.Label_Length + " ≡ 1.0";
            }
            else
            {
                label_Length.Text = Localization.Info.Label_Length + ": " + V.Length.ToString(Settings.ToStringParameter);
                label_Z.Text = "Z: " + V.Z.ToString(Settings.ToStringParameter);
            }

            label_X.Text = "X: " + V.X.ToString(Settings.ToStringParameter);
            label_Y.Text = "Y: " + V.Y.ToString(Settings.ToStringParameter);

            V.Normalize(); //Normalize vector

            // Get Yaw, Pitch
            float Yaw = (float)Math.Atan2(V.X, V.Z);
            float Pitch = (float)Math.Asin(V.Y);

            // Normalize Yaw, Pitch [-1, 1]
            // Must be "(float)Math.PI / 2f", but PI/4 most usable.
            Yaw /= (float)Math.PI / 4f;
            Pitch /= (float)Math.PI / 4f;

            float HalfW = Re.Width / 2f;
            float HalfH = Re.Height / 2f;
            PointF P1 = new PointF(HalfW, HalfH);
            PointF P2 = new PointF(HalfW + HalfW * Yaw, HalfH + HalfH * Pitch);

            LinearGradientBrush b = new LinearGradientBrush(P1, P2, Settings.Graphics.NormalVector.LineColor1, Settings.Graphics.NormalVector.LineColor2);
            b.GammaCorrection = true;
            Pen p = new Pen(b, Settings.Graphics.NormalVector.LineWidth);
            p.SetLineCap(LineCap.RoundAnchor, LineCap.ArrowAnchor, DashCap.Flat);
            Gr.DrawLine(p, P1, P2);
        }

        private void pictureBoxZ_MouseClick_Move(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.X >= 0 && e.Y >= 0 &&
                e.X < pictureBoxZ.ClientRectangle.Width && e.Y < pictureBoxZ.ClientRectangle.Height)
            {
                NMapColor = Color.FromArgb(pictureBoxZ.GetColorAtPoint(e.Location).ToArgb());
                label_RGB.Text = String.Format("RGB: [{0}; {1}; {2}]", NMapColor.R, NMapColor.G, NMapColor.B);
                pictureBox.Invalidate();
            }
        }

        private void pictureBoxZ_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && (e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
                e.Effect = DragDropEffects.Move;
        }

        private void pictureBoxZ_DragDrop(object sender, DragEventArgs e)
        {
            button_H_N_Maps_Cancel_Click(sender, e);
            Wayback.ClearStates();

            if (e.Data.GetDataPresent(DataFormats.FileDrop) && e.Effect == DragDropEffects.Move)
                OpenFirstImage((string[])e.Data.GetData(DataFormats.FileDrop));
        }
        #endregion

        #region RGB, R, G, B, A, BadPixels
        private void radioButtonRGB_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ.DrawMode2D = DrawModes2D.RGB;
        }

        private void radioButtonR_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ.DrawMode2D = DrawModes2D.Red;
        }

        private void radioButtonG_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ.DrawMode2D = DrawModes2D.Green;
        }

        private void radioButtonB_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ.DrawMode2D = DrawModes2D.Blue;
        }

        private void radioButtonA_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ.DrawMode2D = DrawModes2D.Alpha;
        }

        private void radioButtonBadPixels_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ.DrawMode2D = DrawModes2D.BadPixels;
        }
        #endregion

        void EditTexture(EditingModes EditingMode)
        {
            if (EditingMode != EditingModes.NormalMapGen)
            {
                if (Engine.TextureMapGen_OrigID != 0)
                {
                    int GeneratedResult = Engine.TextureID;
                    Engine.TextureID = Engine.TextureMapGen_OrigID;
                    Wayback.AddState(pictureBoxZ.Image);
                    Engine.TextureID = GeneratedResult;
                }
                else
                    Wayback.AddState(pictureBoxZ.Image);
            }

            if (Engine.TextureMapGen_OrigID != 0)
            {
                GL.DeleteTexture(Engine.TextureID);
                Engine.TextureID = Engine.TextureMapGen_OrigID;
                Engine.TextureMapGen_OrigID = 0;
            }

            int NewTexture = FBO_2D.Edit2D(EditingMode, pictureBoxZ, checkBox_GenInvX.Checked, checkBox_GenInvY.Checked);

            if (EditingMode == EditingModes.NormalMapGen)
                Engine.TextureMapGen_OrigID = Engine.TextureID;
            else
            {
                GL.DeleteTexture(Engine.TextureID);
                GL.DeleteTexture(Engine.TextureMapGen_OrigID);
                Engine.TextureMapGen_OrigID = 0;
                button_NMap_Apply.Enabled = button_NMap_Cancel.Enabled = false;
                button_HMap_Apply.Enabled = button_HMap_Cancel.Enabled = false;
            }

            Engine.TextureID = NewTexture;

            if (pictureBoxZ.MipMaps) // Regenerate MipMaps
            {
                pictureBoxZ.MipMaps = false;
                pictureBoxZ.MipMaps = true;
            }

            if (EditingMode == EditingModes.Rotate270 || EditingMode == EditingModes.Rotate90)
                pictureBoxZ.ZoomUpdate(false);

            pictureBoxZ.Invalidate();
        }

        #region Normal, Height Maps
        float NormalMap_EqualizeKernelCoef(int KernelSide)
        {
            return new float[] {
                0.125f,
                0.0217391304347826f,
                0.0072463768115942f,
                0.00324675324675325f,
                0.00172413793103448f,
                0.00102249488752556f,
                0.000655307994757536f,
                0.000444839857651246f,
                0.000315656565656566f,
                0.000232018561484919f,
                0.000175500175500175f,
                0.000135943447525829f,
                0.000107434464976364f,
                8.63707030575229E-05f,
                7.04721634954193E-05f,
                5.82479030754893E-05f,
                4.86949746786132E-05f,
                4.11218027798339E-05f,
                3.50409979676221E-05f,
                3.01023479831427E-05f,
                2.60498072314265E-05f,
                2.26932328779558E-05f,
                1.9889414853415E-05f,
                1.75290983031833E-05f,
                1.5527950310559E-05f,
                1.38201719229387E-05f,
                1.23539149556494E-05f,
                1.10879496163569E-05f,
                9.98921165141647E-06f,
                9.03097624853247E-06f,
                8.19148413309523E-06f,
                7.45289768662056E-06f,
                6.80050051683804E-06f,
                6.22207842307644E-06f,
                5.70743679013755E-06f,
                5.24802149589605E-06f,
                4.83661900984736E-06f,
                4.46711754773115E-06f,
                4.134315646731E-06f,
                3.8337678270204E-06f,
                3.56165944837018E-06f,
                3.3147046929589E-06f,
                3.09006297548344E-06f,
                2.88527011898854E-06f,
                2.69818142571907E-06f,
                2.52692437926103E-06f,
                2.36985918296735E-06f,
                2.22554570380657E-06f,
                2.09271567527749E-06f,
                1.97024923652842E-06f,
                1.85715506130469E-06f,
                1.75255347040638E-06f,
                1.65566203302052E-06f,
                1.56578325175603E-06f,
                1.4822939981916E-06f,
                1.40463642390804E-06f,
                1.33231011916182E-06f,
                1.26486532978834E-06f,
                1.20189707434214E-06f,
                1.14304002926182E-06f,
                1.08796407107452E-06f,
                1.03637038219267E-06f,
                9.87988041392747E-07f,
                9.42571032152983E-07f,
                8.99895612108995E-07f,
                8.59757995319477E-07f,
                8.21972306109063E-07f,
                7.86368769207057E-07f,
                7.52792105920861E-07f,
                7.21100110328317E-07f,
                6.91162383072604E-07f,
                6.62859203402324E-07f,
                6.3608052270553E-07f,
                6.10725065011683E-07f,
                5.8669952183989E-07f,
                5.63917830405096E-07f,
                5.42300525597669E-07f,
                5.21774157360825E-07f,
                5.0227076613369E-07f,
                4.83727409929956E-07f,
                4.66085737403567E-07f,
                4.49291601931235E-07f,
                4.33294712331308E-07f,
                4.18048316352211E-07f,
                4.03508913511899E-07f,
                3.89635994261441E-07f,
                3.76391802788762E-07f,
                3.63741121079234E-07f,
                3.51651072113789E-07f,
                3.40090940317441E-07f,
                3.29032007575633E-07f,
                3.18447403316184E-07f,
                3.08311967313998E-07f,
                2.98602124016629E-07f,
                2.89295767313628E-07f,
                2.80372154783373E-07f,
                2.71811810549343E-07f,
                2.63596435965309E-07f,
                2.55708827426715E-07f,
            }[KernelSide / 2 - 1];
        }

        private void button_NMap_Gen_Click(object sender, EventArgs e)
        {
            button_NMap_Gen.Enabled = button_NMap_Apply.Enabled = button_NMap_Cancel.Enabled = false;

            if (Settings.ShowGenerationTime)
            {
                PerfomanceTimer.Reset();
                PerfomanceTimer.Start();
            }

            // Get Kernels, Scales
            List<int> Kernels = new List<int>();
            List<float> Scales = new List<float>();
            for (int i = 0; i < controlsListKernels.Controls.Count; i++)
            {
                SliderTrackbar ST1 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[0];
                SliderTrackbar ST2 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[1];

                if (ST1.Enabled && ST2.Enabled)
                {
                    int Kernel = (int)ST1.Value;
                    Kernels.Add(Kernel);
                    Scales.Add((float)ST2.Value * NormalMap_EqualizeKernelCoef(Kernel));
                }
            }

            Engine.NormalMapNormalize = checkBox_GenNormalize.Checked;
            Engine.NormalMapKernels = Kernels.ToArray();
            Engine.NormalMapScales = Scales.ToArray();
            Engine.RgbToGrayMode = Settings.RgbToGrayMode = (RgbToGrayModes)comboBox_GenRGB2Gray.SelectedValue;
            EditTexture(EditingModes.NormalMapGen);

            if (Settings.ShowGenerationTime)
            {
                PerfomanceTimer.Stop();
                ShowCalculationTime();
                PerfomanceTimer.Reset();
            }

            button_NMap_Gen.Enabled = button_NMap_Apply.Enabled = button_NMap_Cancel.Enabled = true;
        }

        private void button_HMap_Gen_Click(object sender, EventArgs e)
        {
            button_HMap_Gen.Enabled = button_HMap_Apply.Enabled = button_HMap_Cancel.Enabled = false;

            if (Settings.ShowGenerationTime)
            {
                PerfomanceTimer.Reset();
                PerfomanceTimer.Start();
            }

            if (Engine.TextureMapGen_OrigID != 0)
            {
                GL.DeleteTexture(Engine.TextureID);
                Engine.TextureID = Engine.TextureMapGen_OrigID;
                Engine.TextureMapGen_OrigID = 0;
            }

            int SwapTextures = FBO_2D.Edit2D(EditingModes.NoOperation, pictureBoxZ); // Create Texture BackUp
            Engine.TextureMapGen_OrigID = Engine.TextureID;
            Engine.TextureID = SwapTextures;

            int FloatTexture = FBO_2D.Edit2D(EditingModes.NoOperation, pictureBoxZ); // UByteTexture -> FloatTexture
            GL.DeleteTexture(Engine.TextureID);
            Engine.TextureID = FloatTexture;

            int Tex_Width, Tex_Height;
            GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
            GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out Tex_Width);
            GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out Tex_Height);

            Vector4[] V_1D = new Vector4[Tex_Width * Tex_Height]; // R, G, B<->Height, Alpha
            Vector2[,] V = new Vector2[Tex_Width, Tex_Height];
            GL.PixelStore(PixelStoreParameter.PackAlignment, 1);
            GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.Rgba, PixelType.Float, V_1D);

            int TexHeightMinOne = Tex_Height - 1;
            Parallel.For(0, Tex_Height, (int j) =>
             {
                 long HIndex = (TexHeightMinOne - j) * Tex_Width;
                 for (int i = 0; i < Tex_Width; i++)
                 {
                     long Index = HIndex + i;
                     V_1D[Index].Xy = V_1D[Index].Xy * 2f - Vector2.One;
                     V_1D[Index].X *= Engine.InvertX ? -1f : 1f;
                     V_1D[Index].Y *= Engine.InvertY ? -1f : 1f;
                     V[i, j] = V_1D[Index].Xy;
                 }
             });

            float[,] H = new float[Tex_Width, Tex_Height];
            if (checkBox_HMap_CPU_Precompute.Checked)
                H = CalcHeight(V); // Calc Height on CPU

            Parallel.For(0, Tex_Height, (int j) =>
            {
                long HIndex = (TexHeightMinOne - j) * Tex_Width;
                for (int i = 0; i < Tex_Width; i++)
                    V_1D[HIndex + i].Z = H[i, j];
            });
            V = null;
            H = null;

            GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
            GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
            GL.TexSubImage2D(TextureTarget.Texture2D, 0, 0, 0, Tex_Width, Tex_Height, PixelFormat.Rgba, PixelType.Float, V_1D);

            int HMap_Step = int.Parse(sliderTrackbar_HMap_Step.SelectedItem);
            if (HMap_Step > 0)
            {
                // Calc Height on GPU
                for (int j = HMap_Step; j > 0; j >>= 1) //32, 16, 8, 4, 2, 1
                {
                    Engine.HeightMap_GenStepValue = j;
                    for (int i = 0; i < (int)sliderTrackbar_HMap_Repeat.Value; i++) // Repeat i times
                    {
                        int NewTexture = FBO_2D.Edit2D(EditingModes.HeightMapGen, pictureBoxZ);
                        GL.DeleteTexture(Engine.TextureID);
                        Engine.TextureID = NewTexture;
                    }
                }

                GL.BindTexture(TextureTarget.Texture2D, Engine.TextureID);
                GL.PixelStore(PixelStoreParameter.PackAlignment, 1);
                GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.Rgba, PixelType.Float, V_1D);
            }

            Engine.HeightMap_MinValue = float.MaxValue;
            Engine.HeightMap_MaxValue = float.MinValue;
            Parallel.For(0, V_1D.Length, (int i) =>
             {
                 Engine.HeightMap_MinValue = Math.Min(V_1D[i].Z, Engine.HeightMap_MinValue);
                 Engine.HeightMap_MaxValue = Math.Max(V_1D[i].Z, Engine.HeightMap_MaxValue);
             });
            V_1D = null;

            int Normalized = FBO_2D.Edit2D(EditingModes.HeightMapNormalize, pictureBoxZ); // Normalize Texture on GPU
            GL.DeleteTexture(Engine.TextureID);
            Engine.TextureID = Normalized;

            if (checkBox_HMap_Invert.Checked) //Invert RGB
            {
                int Inverted = FBO_2D.Edit2D(EditingModes.InvertRGB, pictureBoxZ);
                GL.DeleteTexture(Engine.TextureID);
                Engine.TextureID = Inverted;
            }

            if (pictureBoxZ.MipMaps) // Regenerate MipMaps
            {
                pictureBoxZ.MipMaps = false;
                pictureBoxZ.MipMaps = true;
            }

            if (Settings.ShowGenerationTime)
            {
                PerfomanceTimer.Stop();
                ShowCalculationTime();
                PerfomanceTimer.Reset();
            }

            button_HMap_Gen.Enabled = button_HMap_Apply.Enabled = button_HMap_Cancel.Enabled = true;
            pictureBoxZ.Invalidate();
        }

        private void ShowCalculationTime()
        {
            Text = Form1TextDefault + " - " + Localization.GenerationTime + ": " + PerfomanceTimer.Elapsed.ToString();
        }

        private float[,] CalcHeight(Vector2[,] V, int Step = 1)
        {
            int Width = V.GetLength(0);
            int Height = V.GetLength(1);

            float[,] LT = new float[Width, Height]; //Start at Left, Top
            float[,] RB = new float[Width, Height]; //Start at Right, Bottom
            float[,] LB = new float[Width, Height]; //Start at Left, Bottom
            float[,] RT = new float[Width, Height]; //Start at Right, Top

            int WidthMinusOne = Width - 1;
            int HeightMinusOne = Height - 1;
            int WidthMinusStep = Width - Step;
            int HeightMinusStep = Height - Step;

            const float HalfConst = 0.5f;

            for (int j = 0; j < Height; j++)
            {
                int y = Math.Max(Math.Min(j, HeightMinusStep), Step); // y
                int Y = HeightMinusOne - y; // inverted y
                int J = HeightMinusOne - j; // inverted j

                LT[0, j] = -V[0, j].X + V[0, j].Y;
                RB[WidthMinusOne, J] = V[WidthMinusOne, J].X - V[WidthMinusOne, J].Y;
                LB[0, J] = -V[0, J].X - V[0, J].Y;
                RT[WidthMinusOne, j] = V[WidthMinusOne, j].X + V[WidthMinusOne, j].Y;

                for (int i = 0; i < Width; i++)
                {
                    int x = Math.Max(Math.Min(i, WidthMinusStep), Step); // x
                    int X = WidthMinusOne - x; // inverted x
                    int I = WidthMinusOne - i; // inverted i

                    
                    //LT[i, j] = (LT[x - Step, y] + LT[x, y - Step]) * HalfConst - V[i, j].X + V[i, j].Y;
                    //RB[I, J] = (RB[X + Step, Y] + RB[X, Y + Step]) * HalfConst + V[I, J].X - V[I, J].Y;
                    //LB[i, J] = (LB[x - Step, Y] + LB[x, Y + Step]) * HalfConst - V[i, J].X - V[i, J].Y;
                    //RT[I, j] = (RT[X + Step, y] + RT[X, y - Step]) * HalfConst + V[I, j].X + V[I, j].Y;

                    LT[i, j] = (LT[x - Step, y] + LT[x, y - Step]) * HalfConst - V[i, j].X + V[i, j].Y - V[x - Step, y].X + V[x, y - Step].Y;
                    RB[I, J] = (RB[X + Step, Y] + RB[X, Y + Step]) * HalfConst + V[I, J].X - V[I, J].Y + V[X + Step, Y].X - V[X, Y + Step].Y;
                    LB[i, J] = (LB[x - Step, Y] + LB[x, Y + Step]) * HalfConst - V[i, J].X - V[i, J].Y - V[x - Step, Y].X - V[x, Y + Step].Y;
                    RT[I, j] = (RT[X + Step, y] + RT[X, y - Step]) * HalfConst + V[I, j].X + V[I, j].Y + V[X + Step, y].X + V[X, y - Step].Y;
                }
            }

            float AvgStepValue = Step * 0.25f * 0.25f; //Step * 0.25f * 0.5f;
            if (Height >= Width)
                Parallel.For(0, Height, (int j) =>
                {
                    for (int i = 0; i < Width; i++)
                        LT[i, j] = (LT[i, j] + RB[i, j] + LB[i, j] + RT[i, j]) * AvgStepValue;
                });
            else
                Parallel.For(0, Width, (int i) =>
                {
                    for (int j = 0; j < Height; j++)
                        LT[i, j] = (LT[i, j] + RB[i, j] + LB[i, j] + RT[i, j]) * AvgStepValue;
                });

            return LT;
        }

        private void button_H_N_Maps_Apply_Click(object sender, EventArgs e)
        {
            int GeneratedResult = Engine.TextureID;
            Engine.TextureID = Engine.TextureMapGen_OrigID;
            Wayback.AddState(pictureBoxZ.Image);
            Engine.TextureID = GeneratedResult;
            GL.DeleteTexture(Engine.TextureMapGen_OrigID);
            Engine.TextureMapGen_OrigID = 0;

            button_NMap_Apply.Enabled = button_NMap_Cancel.Enabled = false;
            button_HMap_Apply.Enabled = button_HMap_Cancel.Enabled = false;
        }

        private void button_H_N_Maps_Cancel_Click(object sender, EventArgs e)
        {
            button_NMap_Apply.Enabled = button_NMap_Cancel.Enabled = false;
            button_HMap_Apply.Enabled = button_HMap_Cancel.Enabled = false;
            if (Engine.TextureMapGen_OrigID != 0)
            {
                GL.DeleteTexture(Engine.TextureID);
                Engine.TextureID = Engine.TextureMapGen_OrigID;
                Engine.TextureMapGen_OrigID = 0;
            }
            pictureBoxZ.Invalidate();
        }
        #endregion

        private void comboBox_Mashes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Engine.MainMesh = Meshes.MeshesList[comboBox_Mashes.SelectedIndex];
            Engine.MainMesh.Rotation = Vector3.Zero;
            pictureBoxZ.Invalidate();
        }

        private void checkBoxes_3D_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ.Is3DView = checkBox_2D_3D.Checked;
            pictureBoxZ.UseTextureAsNormalMap = checkBox_UseTexAsNMap.Checked;
            pictureBoxZ.UseLight = checkBox_UseLightIn3D.Checked;
            pictureBoxZ.ShowTexture = checkBox_ShowTexIn3D.Checked;
        }

        private void buttonSettingsSave_Click(object sender, EventArgs e)
        {
            SettingsXML.Save(Settings, Path.Combine(StartupPath, SettingsXML.XmlFile));
        }

        private void buttonSaveLocalizationFile_Click(object sender, EventArgs e)
        {
            string XmlFile = Path.Combine(StartupPath, LocalizationXML.XmlFile);
            if (LocalizationXML.Save(Localization, XmlFile))
            {
                MessageBox.Show(String.Format("Done, saved to: \"{0}\"", XmlFile), "Done",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                buttonSaveLocalizationFile.Visible = false;
            }
            else
                MessageBox.Show("Localization file saving Error!!!", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SettingsTab_SettingsChanged(object sender, EventArgs e)
        {
            if (!Settings.IsLoaded)
                return;

            pictureBoxZ.InvertY = Settings.NegativeGreenChannel = radioButtonSettingsNegG.Checked;

            pictureBoxZ.MagFilter = Settings.Graphics.View_MagFilter = (TextureMagFilter)comboBox_SettingsViewMagFilter.SelectedValue;
            pictureBoxZ.MinFilter = Settings.Graphics.View_MinFilter = (TextureMinFilter)comboBox_SettingsViewMinFilter.SelectedValue;
            pictureBoxZ.BadPixels_MagFilter = Settings.BadPixels.MagFilter = (TextureMagFilter)comboBox_SettingsBadPixelsMagFilter.SelectedValue;
            pictureBoxZ.BadPixels_MinFilter = Settings.BadPixels.MinFilter = (TextureMinFilter)comboBox_SettingsBadPixelsMinFilter.SelectedValue;

            pictureBoxZ.AnisotropicFiltering = Settings.Graphics.AnisotropicFiltering = checkBox_AnisotropicFiltering.Checked;
            pictureBoxZ.FXAA = Settings.Graphics.FXAA = checkBox_FXAA.Checked;
            pictureBoxZ.MipMaps = Settings.Graphics.MipMaps = checkBox_MipMaps.Checked;
            pictureBoxZ.VSync = Settings.Graphics.VSync = checkBox_VSync.Checked;
            pictureBoxZ.Draw2D_UseAlpha = Settings.Graphics.Draw2D_UseAlpha = checkBox_Draw2D_UseAlpha.Checked;
        }

        #region toolStripButton (Menu)
        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            button_H_N_Maps_Cancel_Click(sender, e);

            openFileDlg.FileName = Path.GetFileName(openFileDlg.FileName);
            if (openFileDlg.ShowDialog() == DialogResult.OK && File.Exists(openFileDlg.FileName))
            {
                if (OpenImage(openFileDlg.FileName))
                {
                    Wayback.ClearStates();
                    saveFileDlg.FileName = openFileDlg.FileName;
                }
                else
                    MessageBox.Show("Maybe \"" + Path.GetFileName(openFileDlg.FileName) + "\" is not supported image!",
                        "File opening error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButtonSaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDlg.FileName = Path.GetFileNameWithoutExtension(saveFileDlg.FileName);
                if (saveFileDlg.ShowDialog() == DialogResult.OK)
                {
                    switch (saveFileDlg.FilterIndex)
                    {
                        default:
                        case 1:
                            pictureBoxZ.Image.Save(saveFileDlg.FileName, ImageFormat.Bmp);
                            break;

                        case 2:
                            ImageCodecInfo JgpEncoder = null;
                            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
                            foreach (ImageCodecInfo codec in codecs)
                                if (codec.FormatID == ImageFormat.Jpeg.Guid)
                                {
                                    JgpEncoder = codec;
                                    break;
                                }

                            if (JgpEncoder == null)
                                break;

                            EncoderParameters JpgEncParams = new EncoderParameters(1);
                            JpgEncParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
                            pictureBoxZ.Image.Save(saveFileDlg.FileName, JgpEncoder, JpgEncParams);
                            break;

                        case 3:
                            pictureBoxZ.Image.Save(saveFileDlg.FileName, ImageFormat.Png);
                            break;

                        case 4:
                            pictureBoxZ.Image.Save(saveFileDlg.FileName, ImageFormat.Gif);
                            break;

                        case 5:
                            pictureBoxZ.Image.Save(saveFileDlg.FileName, ImageFormat.Tiff);
                            break;

                        case 6:
                            TGA.FromBitmap((Bitmap)pictureBoxZ.Image, false, false).Save(saveFileDlg.FileName);
                            break;
                    }
                }
            }
            catch (ArgumentNullException) // TEMPLATE EXCEPTION
            {
                MessageBox.Show("Argument in Null!", "File saving error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Don't can save file: \"" + Path.GetFileName(saveFileDlg.FileName) + "\".",
                    "File saving error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButtonUndo_Click(object sender, EventArgs e)
        {
            button_H_N_Maps_Cancel_Click(sender, e);
            pictureBoxZ.Image = Wayback.Undo(pictureBoxZ.Image);
        }

        private void toolStripButtonRedo_Click(object sender, EventArgs e)
        {
            pictureBoxZ.Image = Wayback.Redo();
        }

        private void toolStripButtonCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetImage(pictureBoxZ.Image);
        }

        private void toolStripButtonPaste_Click(object sender, EventArgs e)
        {
            button_H_N_Maps_Cancel_Click(sender, e);

            if (Clipboard.ContainsImage())
            {
                Wayback.AddState(pictureBoxZ.Image);
                pictureBoxZ.Image = Clipboard.GetImage();
            }
        }

        private void toolStripButtonInvR_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.InvertR);
        }

        private void toolStripButtonInvG_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.InvertG);
        }

        private void toolStripButtonInvB_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.InvertB);
        }

        private void toolStripButtonInvA_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.InvertA);
        }

        private void toolStripButtonNormalize_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.Normalize);
        }

        private void toolStripButtonRestoreB_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.RestoreB);
        }

        private void toolStripButtonClearB_Click(object sender, EventArgs e)
        {
            pictureBoxZ.ClearBlueChannelValue = Settings.ClearBlueChannelValue;
            EditTexture(EditingModes.ClearB);
        }

        private void toolStripButtonFlipRG_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.FlipRG);
        }

        private void toolStripButtonFlipGB_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.FlipGB);
        }

        private void toolStripButtonFlipBA_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.FlipBA);
        }

        private void toolStripButtonRotate270_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.Rotate270);
        }

        private void toolStripButtonRotate90_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.Rotate90);
        }

        private void toolStripButtonRotate180_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.Rotate180);

        }

        private void toolStripButtonFlipH_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.FlipHorizontally);
        }

        private void toolStripButtonFlipV_Click(object sender, EventArgs e)
        {
            EditTexture(EditingModes.FlipVertically);

        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Form1TextDefault + "\n\n" +
                "Link: https://gitlab.com/Alex_Green/NormalMap_Debugger_OpenGL\n" +
                "License: GNU GPLv3\nFor any questions write me: \"alex.green.zaa.93@gmail.com\"",
                "About...", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        #endregion

        private void tabPageGenNMap_Resize(object sender, EventArgs e)
        {
            controlsListKernels.Width = tabPage_ToNMap.Width - controlsListKernels.Left * 2;
        }
    }
}
